import 'package:async/async.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

// import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:toast/toast.dart';
import 'package:waiter/restauran_otp_screen.dart';
import 'package:waiter/session/userRepository.dart';
import 'package:waiter/utils/DialogClass.dart';
import 'package:waiter/utils/all_constans.dart';
import 'package:waiter/utils/sizeconfig.dart';

import 'apis/WaiterGenerateOtpApi.dart';

class RestaurantLoginCode extends StatefulWidget {
  @override
  _RestaurantLoginCodeState createState() => _RestaurantLoginCodeState();
}

class _RestaurantLoginCodeState extends State<RestaurantLoginCode> {
  CancelableOperation cancelableOperation;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  String deviceToken = "";
  TextEditingController restaurant_code_Controller = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  double logomargin_height;

  @override
  void initState() {
    getDeviceToken();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    print("Height" + SizeConfig.screenHeight.toString());
    if (SizeConfig.screenHeight >= 580 && SizeConfig.screenHeight < 800) {
      logomargin_height = 120;
    }
    if (SizeConfig.screenHeight >= 800) {
      logomargin_height = 220;
    }
    return Scaffold(
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(image: DecorationImage(image: ExactAssetImage("images/splash_bg.png"), fit: BoxFit.cover)),
        child: SingleChildScrollView(
            child: Column(
          // shrinkWrap: true,
          children: [
            Container(
                margin: EdgeInsets.fromLTRB(40, logomargin_height, 0, 0),
                alignment: Alignment.centerLeft,
                //height: MediaQuery.of(context).size.height * 1.0,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image(
                      image: AssetImage('images/logo_dashboard.png'),
                      //fit: BoxFit.fitWidth,
                      width: 150,
                      height: 110,
                    ),
                    Padding(
                        padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                        child: Text(
                          'The Waiter App',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: SizeConfig.blockSizeHorizontal * 4,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w400,
                          ),
                        ))
                  ],
                )),
            Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                      margin: EdgeInsets.fromLTRB(0, 50, 0, 0),
                      alignment: Alignment.center,
                      //height: MediaQuery.of(context).size.height * 1.0,
                      child: Column(
                        children: [
                          Container(
                              margin: EdgeInsets.fromLTRB(35, 0, 35, 0),
                              alignment: Alignment.topLeft,
                              child: Text(
                                'Restaurant code',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: SizeConfig.blockSizeHorizontal * 3.2,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w400,
                                ),
                              )),
                          Container(
                            margin: EdgeInsets.fromLTRB(35, 5, 35, 0),
                            child: TextFormField(
                              validator: (val) {
                                if (val.isEmpty) return 'Enter Restaurant Code';
                                return null;
                              },
                              controller: restaurant_code_Controller,
                              obscureText: false,
                              keyboardType: TextInputType.text,
                              textCapitalization: TextCapitalization.characters,
                              decoration: InputDecoration(
                                  filled: true,
                                  fillColor: Colors.white,
                                  hintText: "Enter Restaurant Code",
                                  hintStyle: TextStyle(color: Colors.grey),
                                  contentPadding: EdgeInsets.only(
                                    bottom: 30 / 2,
                                    left: 50 / 2, // HERE THE IMPORTANT PART
                                    // HERE THE IMPORTANT PART
                                  ),
                                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0))),
                            ),
                          )
                        ],
                      )),
                  Container(
                      margin: EdgeInsets.fromLTRB(35, 20, 35, 0),
                      width: double.infinity,
                      decoration: BoxDecoration(color: login_passcode_bg1, borderRadius: BorderRadius.circular(0)),
                      child: TextButton(
                          child: Text(submit.toUpperCase(),
                              style: TextStyle(fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w800, color: Colors.white)),
                          onPressed: () {
                            FocusScope.of(context).unfocus();
                            /*check().then((intenet) {
                              if (intenet != null && intenet) {
                                if (_formKey.currentState.validate()) {
                                  _formKey.currentState.save();
                                  SignDialogs.showLoadingDialog(context, "Signing in", _keyLoader);
                                  cancelableOperation?.cancel();
                                  CancelableOperation.fromFuture(Future.delayed(Duration(seconds: 1), () {
                                    WaiterGenerateOtpRepository().generateotp(restaurant_code_Controller.text.trim().toString()).then((value) {
                                      print(value);
                                      if (value.responseStatus == 0) {
                                        Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
                                        Toast.show(value.result, context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                                      } else {
                                        print("GENERATECODERESPONSE " + value.oTP + "-----" + value.restaurantId + "-----" + value.result);
                                        //UserRepository.save_otp(value.oTP, value.restaurantId);
                                        setState(() {
                                          UserRepository.save_restaurant_code(restaurant_code_Controller.text.trim().toString());
                                          //UserRepository.save_restaurantid(value.restaurantId, "true");
                                          Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(builder: (context) => RestauranLoginPaascode(value.oTP, value.restaurantId)),
                                          );
                                        });
                                      }
                                    });
                                  }));
                                }
                              } else {
                                Toast.show("No Internet Connection", context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                              }
                              // No-Internet Case
                            });*/
                            if (_formKey.currentState.validate()) {
                              _formKey.currentState.save();
                              SignDialogs.showLoadingDialog(context, "Signing in", _keyLoader);
                              cancelableOperation?.cancel();
                              CancelableOperation.fromFuture(Future.delayed(Duration(seconds: 1), () {
                                WaiterGenerateOtpRepository().generateotp(restaurant_code_Controller.text.trim().toString()).then((value) {
                                  print(value);
                                  Navigator.of(context).pop();
                                  if (value.responseStatus == 0) {
                                    Toast.show(value.result, context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                                  } else {
                                    print("GENERATECODERESPONSE " + value.oTP + "-----" + value.restaurantId + "-----" + value.result);
                                    setState(() {
                                      UserRepository.save_restaurant_code(restaurant_code_Controller.text.trim().toString());
                                      UserRepository.saveDeviceToken(deviceToken);
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(builder: (context) => RestaurantLoginPasscode(value.oTP, value.restaurantId)),
                                      );
                                    });
                                  }
                                });
                              }));
                            }
                          }))
                ],
              ),
            )
          ],
        )),
      ),
    );
  }

  Future<void> getDeviceToken() async {
    try {
      await FirebaseMessaging().getToken().then((value) {
        deviceToken = value;
        print("DEVICETOKEN" + deviceToken);
      });
    } on PlatformException {
      print("DEVICETOKEN platform exception");
    }
  }
}

class CancelDialogs {
  static Future<void> showLoadingDialog(BuildContext context, GlobalKey key) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new WillPopScope(
              onWillPop: () async => false,
              child: SimpleDialog(key: key, backgroundColor: Colors.black54, children: <Widget>[
                Center(
                  child: Column(children: [
                    CircularProgressIndicator(),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Cancelling....",
                      style: TextStyle(color: Colors.lightBlueAccent),
                    )
                  ]),
                )
              ]));
        });
  }
}

/*Future<bool> check() async {
  var connectivityResult = await (Connectivity().checkConnectivity());
  if (connectivityResult == ConnectivityResult.mobile) {
    return true;
  } else if (connectivityResult == ConnectivityResult.wifi) {
    return true;
  }
  return false;
}*/

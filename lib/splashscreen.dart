import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:waiter/restauran_login_screen.dart';
import 'package:waiter/session/userRepository.dart';
import 'package:waiter/utils/sizeconfig.dart';
import 'package:waiter/webview_screen.dart';

import 'dashboard.dart';
import 'login.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  String is_Login_session = "";
  String is_Login_session_waiter = "";
  double logomargin_height;

  @override
  void initState() {
    super.initState();

    UserRepository().getRestaurant_id().then((getIsLogin) {
      setState(() {
        UserRepository().getuserdetails().then((getIsLoginWaiter) {
          setState(() {
            is_Login_session = getIsLogin[1];
            is_Login_session_waiter = getIsLoginWaiter[3];
            print("ISLOGIN=====" + is_Login_session.toString() + "------" + is_Login_session_waiter);
            if (is_Login_session == "true") {
              if (is_Login_session_waiter == "true") {
                Timer(Duration(seconds: 2),
                    () => Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => HomePage()), (route) => false));
              } else if (is_Login_session_waiter == "") {
                Timer(Duration(seconds: 2),
                    () => Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => Login()), (route) => false));
              } else {
                Timer(Duration(seconds: 2),
                    () => Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => Login()), (route) => false));
              }
            } else if (is_Login_session == "") {
              print("ISFALSE=====" + is_Login_session.toString());
              Timer(
                  Duration(seconds: 2),
                  () => Navigator.of(context)
                      .pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => RestaurantLoginCode()), (route) => false));
            } else {
              Timer(
                  Duration(seconds: 2),
                  () => Navigator.of(context)
                      .pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => RestaurantLoginCode()), (route) => false));
            }
          });
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    print("Height" + SizeConfig.screenHeight.toString());
    if (SizeConfig.screenHeight >= 580 && SizeConfig.screenHeight < 800) {
      logomargin_height = 120;
    }
    if (SizeConfig.screenHeight >= 800) {
      logomargin_height = 220;
    }
    return Scaffold(
      backgroundColor: Colors.black,
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(image: DecorationImage(image: ExactAssetImage("images/splash_bg.png"), fit: BoxFit.cover)),
        child: SingleChildScrollView(
            child: Column(
          children: [
            Container(
                margin: EdgeInsets.fromLTRB(40, logomargin_height, 0, 0),
                alignment: Alignment.centerLeft,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image(
                      image: AssetImage('images/logo_dashboard.png'),
                      width: 150,
                      height: 110,
                    ),
                    Padding(
                        padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                        child: Text(
                          'The Waiter App',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: SizeConfig.blockSizeHorizontal * 4,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w400,
                          ),
                        ))
                  ],
                )),
          ],
        )),
      ),
    );
  }
}

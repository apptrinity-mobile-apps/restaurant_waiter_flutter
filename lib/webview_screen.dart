import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';
import 'package:waiter/payment_success.dart';
import 'package:webview_flutter/webview_flutter.dart';

import 'apis/PayOrderApi.dart';
import 'model/paymentcarddetailsresponse.dart';

class WebViewExample extends StatefulWidget {
  final String orderId;
  final String grandtotal;
  final double amount_tendered;
  final String employee_id;
  final String res_id;
  final String table_number;
  final String no_of_guest;
  final String selected_tip;
  final bool use_credits;
  final double used_credits;

  const WebViewExample(this.orderId, this.grandtotal, this.amount_tendered, this.employee_id, this.res_id, this.table_number, this.no_of_guest,
      this.selected_tip, this.use_credits, this.used_credits,
      {Key key})
      : super(key: key);

  @override
  _WebViewExampleState createState() => _WebViewExampleState();
}

class _WebViewExampleState extends State<WebViewExample> {
  // URL to load
  // var URL = "http://3.14.187.24/api/pos/card_payment_redirection";
  var URL = "http://18.190.55.150/api/pos/card_payment_redirection";

  //var URL = "https://i1gov.com/";
  // Webview progress
  double progress = 0;

  // InAppWebViewController _webViewController;
  String url = "";
  final Completer<WebViewController> _controller = Completer<WebViewController>();
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  WebViewController _webViewController;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Payment Screen"),
      ),
      body: Container(
          child: Column(
              children: <Widget>[
        (progress != 1.0)
            ? LinearProgressIndicator(
                value: progress, backgroundColor: Colors.grey[200], valueColor: AlwaysStoppedAnimation<Color>(Colors.lightBlueAccent))
            : null, // Should be removed while showing
        Expanded(
          child: Container(
            child: WebView(
              initialUrl: URL,
              javascriptMode: JavascriptMode.unrestricted,
              onWebViewCreated: (WebViewController webViewController) {
                _webViewController = webViewController;
                _controller.complete(webViewController);
              },
              javascriptChannels: <JavascriptChannel>{
                _toasterJavascriptChannel(context),
              },
              navigationDelegate: (NavigationRequest request) {
                print('allowing navigation to $request');
                print(request.url);
                if (request.url.contains("card_payment_redirection")) {
                  return NavigationDecision.prevent;
                }
                return NavigationDecision.navigate;
              },
              onPageStarted: (String url) {
                print('Page started loading: $url');
                // if (url == "http://3.14.187.24/api/pos/generate_card_token") {
                if (url == "http://18.190.55.150/api/pos/generate_card_token") {
                  print("SAMEURL===generate_card_token");
                }
              },
              onPageFinished: (String url) {
                print('Page finished loading: ${url}');
                if (url.contains("generate_card_token")) {
                  readJS();
                }
              },
              gestureNavigationEnabled: true,
              gestureRecognizers: Set()..add(Factory<TapGestureRecognizer>(() => TapGestureRecognizer()..onTap = () => print("clicked"))),
            ),
          ),
        )
      ].where((Object o) => o != null).toList())),
    );
  }

  /*_bookmarkButton2() {
    print("BOOKMARKBUTTON22222222222");
    FutureBuilder<WebViewController>(
      future: _controller.future,
      builder: (BuildContext context, AsyncSnapshot<WebViewController> controller) {
        print("BOOKMARKBUTTON22222222222");
        Future.delayed(Duration(seconds: 0), () async {
          setState(() async {
            var url = await controller.data.evaluateJavascript("document.documentElement.innerHTML").then((value) => value);
            String fixed = url.replaceAll(r"\'", "'");
            print("FIXED" + json.decode(fixed));
            removeAllHtmlTags(json.decode(fixed));
            paymentcarddetailsresponse carddetails = paymentcarddetailsresponse.fromJson(jsonDecode(removeAllHtmlTags(json.decode(fixed))));
            print("JSONREADING====" +
                carddetails.cardDetails.cardLast4.toString() +
                "=========" +
                carddetails.cardDetails.cardToken.toString() +
                "======" +
                carddetails.cardDetails.expiry.toString());

            PayOrderRepository()
                .payOrder(
                    widget.orderId,
                    "",
                    double.parse(widget.grandtotal),
                    widget.amount_tendered,
                    0,
                    1,
                    widget.employee_id,
                    widget.res_id,
                    int.parse(widget.table_number),
                    int.parse(widget.no_of_guest),
                    double.parse(widget.selected_tip),
                    "",
                    0,
                    carddetails.cardDetails.cardToken.toString(),
                    carddetails.cardDetails.expiry.toString(),
                    carddetails.cardDetails.cardLast4.toString())
                .then((value) {
              print("PAYMENTSTSTAUS" + value.toString());
              if (value.responseStatus == 0) {
                print("PAYMENTSTATUS" + value.result);
                Toast.show(value.result, context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
              } else {
                setState(() {
                  print("PAYMENTSTATUS" + value.result);
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => PaymentSuccess(widget.orderId, double.parse(widget.grandtotal), value.receiptNumber)),
                  );
                });
              }
            });
          });
        });
        return Container();
      },
    );
  }*/

  /*_bookmarkButton() {
    print("BOOKMARKBUTTON");
    return FutureBuilder<WebViewController>(
      future: _controller.future,
      builder: (BuildContext context, AsyncSnapshot<WebViewController> controller) {
        if (controller.hasData) {
          return Container(
            margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
            alignment: Alignment.bottomRight,
            child: SizedBox(
                height: 50,
                width: 100,
                child: FloatingActionButton(
                  shape: BeveledRectangleBorder(borderRadius: BorderRadius.circular(2)),
                  elevation: 4,
                  onPressed: () async {
                    var url = await controller.data.evaluateJavascript("document.documentElement.innerHTML").then((value) => value);
                    String fixed = url.replaceAll(r"\'", "'");
                    print("FIXED" + json.decode(fixed));

                    removeAllHtmlTags(json.decode(fixed));

                    paymentcarddetailsresponse carddetails = paymentcarddetailsresponse.fromJson(jsonDecode(removeAllHtmlTags(json.decode(fixed))));
                    print("JSONREADING====" +
                        carddetails.cardDetails.cardLast4.toString() +
                        "=========" +
                        carddetails.cardDetails.cardToken.toString() +
                        "======" +
                        carddetails.cardDetails.expiry.toString());

                    PayOrderRepository()
                        .payOrder(
                            widget.orderId,
                            "",
                            double.parse(widget.grandtotal),
                            widget.amount_tendered,
                            0,
                            1,
                            widget.employee_id,
                            widget.res_id,
                            int.parse(widget.table_number),
                            int.parse(widget.no_of_guest),
                            double.parse(widget.selected_tip),
                            "",
                            0,
                            carddetails.cardDetails.cardToken.toString(),
                            carddetails.cardDetails.expiry.toString(),
                            carddetails.cardDetails.cardLast4.toString())
                        .then((value) {
                      print("PAYMENTSTSTAUS" + value.toString());
                      if (value.responseStatus == 0) {
                        print("PAYMENTSTATUS" + value.result);
                        Toast.show(value.result, context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                        // Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
                      } else if (value.responseStatus == 1) {
                        setState(() {
                          print("PAYMENTSTATUS" + value.result);
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => PaymentSuccess(widget.orderId, double.parse(widget.grandtotal), value.receiptNumber)),
                          );
                        });
                      } else {
                        Toast.show(value.result, context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                      }
                    });
                    },
                  child: Padding(
                      padding: EdgeInsets.only(left: 0),
                      child: Text(
                        "Confirm",
                        style: TextStyle(color: Colors.white, fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w600),
                        textAlign: TextAlign.start,
                      )),
                )),
          );
        }
        return Container();
      },
    );
  }*/

  String removeAllHtmlTags(String htmlText) {
    RegExp exp = RegExp(r"<[^>]*>", multiLine: true, caseSensitive: true);

    print("MANIBABU" + htmlText.replaceAll(exp, ''));
    return htmlText.replaceAll(exp, '');
  }

  JavascriptChannel _toasterJavascriptChannel(BuildContext context) {
    return JavascriptChannel(
        name: 'Toaster',
        onMessageReceived: (JavascriptMessage message) {
          // ignore: deprecated_member_use
          Scaffold.of(context).showSnackBar(
            SnackBar(content: Text(message.message)),
          );
        });
  }

  void readJS() async {
    String html = await _webViewController.evaluateJavascript("window.document.getElementsByTagName('html')[0].outerHTML;");
    log("html---- $html");
    String fixed = html.replaceAll(r"\'", "'");
    print("FIXED" + json.decode(fixed));

    paymentcarddetailsresponse carddetails = paymentcarddetailsresponse.fromJson(jsonDecode(removeAllHtmlTags(json.decode(fixed))));
    print("JSONREADING====" +
        carddetails.cardDetails.cardLast4.toString() +
        "=========" +
        carddetails.cardDetails.cardToken.toString() +
        "======" +
        carddetails.cardDetails.expiry.toString());

    PayOrderRepository()
        .payOrder(
            widget.orderId,
            "",
            double.parse(widget.selected_tip) + double.parse(widget.grandtotal),
            widget.amount_tendered,
            0,
            1,
            widget.employee_id,
            widget.res_id,
            int.parse(widget.table_number),
            int.parse(widget.no_of_guest),
            double.parse(widget.selected_tip),
            "",
            0,
            carddetails.cardDetails.cardToken.toString(),
            carddetails.cardDetails.expiry.toString(),
            carddetails.cardDetails.cardLast4.toString(),
            widget.use_credits,
            widget.used_credits)
        .then((value) {
      print("PAYMENTSTSTAUS" + value.toString());
      if (value.responseStatus == 0) {
        print("PAYMENTSTATUS" + value.result);
        Toast.show(value.result, context, duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
        Navigator.of(context).pop();
      } else if (value.responseStatus == 1) {
        setState(() {
          print("PAYMENTSTATUS" + value.result);
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => PaymentSuccess(widget.orderId, double.parse(widget.grandtotal), value.receiptNumber)),
            (route) => false,
          );
        });
      } else {
        Toast.show(value.result, context, duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
        Navigator.of(context).pop();
      }
    });
  }
}

import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';
import 'package:page_indicator/page_indicator.dart';
import 'package:toast/toast.dart';
import 'package:waiter/apis/GetOrderListBySelectedDate.dart';
import 'package:waiter/apis/GetOrderListByUniqueId.dart';
import 'package:waiter/apis/GetOrderListDatewiseApi.dart';
import 'package:waiter/payment2.dart';
import 'package:waiter/receipt.dart';
import 'package:waiter/session/userRepository.dart';
import 'package:waiter/splitorderscreen.dart';
import 'package:waiter/utils/all_constans.dart';
import 'package:waiter/utils/sizeconfig.dart';

import 'addfood_screen.dart';
import 'apis/getTableInformationApi.dart';
import 'model/OrderListDatewiseResponse.dart';
import 'model/gettableinformationresponse.dart';

class GetOrderList extends StatefulWidget {
  @override
  _GetOrderListState createState() => _GetOrderListState();
}

class _GetOrderListState extends State<GetOrderList> {
  GlobalKey<PageContainerState> key = GlobalKey();
  final _formKey = GlobalKey<FormState>();
  int selected_pos = -1;
  List<OrdersList> __datewise_order_list = [];
  List<OrdersData> _order_list = [];
  int split_length = 0;
  var cart_count_value = 1;
  int cart_count = 0;
  var date_wise_order = "";
  var order_id = "";
  var no_of_guest = "";
  var restaurant_id = "";
  var employee_id = "";
  var search_string = "", _date = "";

  double screenheight = 0.0;
  double cardview_height = 0.0;
  double toolbar_height = 0.0;
  bool _loading = true, isHavingData = false;
  TextEditingController _search_text_controller = TextEditingController();

  DateTime selectedDate = DateTime.now();

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(context: context, initialDate: selectedDate, firstDate: DateTime(2015, 8), lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });

    var inputFormat = DateFormat("yyyy-MM-dd");
    var date1 = inputFormat.parse("${selectedDate.toLocal()}".split(' ')[0]);

    var outputFormat = DateFormat("MM-dd-yyyy");
    var date2 = outputFormat.format(date1);
    setState(() {
      _date = date2;
    });
    print("SELECTEDDATE" + "${selectedDate.toLocal()}".split(' ')[0] + "-----" + date2.toString());

    UserRepository().getuserdetails().then((userdetails) {
      print("userdata" + userdetails.length.toString());
      setState(() {
        employee_id = userdetails[0];
        //user_id = userdetails[1];
        UserRepository().getRestaurantId().then((restaurantdetails) {
          setState(() {
            restaurant_id = restaurantdetails;
            // Future.delayed(Duration(seconds: 2), () async {
            _loading = true;
            ordersList();
            // });
          });
        });
      });
    });
  }

  @override
  void initState() {
    super.initState();
    // print(widget.main_order_id.toString());

    print("Height" + SizeConfig.screenHeight.toString());
    if (SizeConfig.screenHeight >= 580 && SizeConfig.screenHeight < 800) {
      toolbar_height = 56;
      screenheight = 460.0;
      cardview_height = 245.0;
    }
    if (SizeConfig.screenHeight >= 800) {
      toolbar_height = 66;
      screenheight = 680.0;
      cardview_height = 450.0;
    }

    UserRepository().getuserdetails().then((userdetails) {
      print("userdata" + userdetails.length.toString());
      setState(() {
        employee_id = userdetails[0];
        //user_id = userdetails[1];
        UserRepository().getRestaurantId().then((restaurantdetails) {
          setState(() {
            restaurant_id = restaurantdetails;
            Future.delayed(Duration(seconds: 2), () async {
              setState(() {
                _loading = true;
                defOrdersHistory();
              });
            });
          });
        });
      });
    });
  }

  void refresh() {
    setState(() {});
  }

  void ordersList() {
    GetOrderListBySelectedDateApiRepository().orderListSelectedDate(employee_id, restaurant_id, _date).then((value) {
      print("TABLEINFORMATION RESPSTATUS  " + value.responseStatus.toString());
      debugPrint(base_url + "" + value.result.toString());
      if (value.responseStatus == 1) {
        _order_list.clear();
        setState(() {
          _loading = false;
          if (value.ordersData.isNotEmpty) {
            isHavingData = true;
            date_wise_order = value.ordersData[0].date.toString();
            for (int t = 0; t < value.ordersData.length; t++) {
              _order_list.add(value.ordersData[t]);
              for (int o = 0; o < value.ordersData[t].ordersList.length; o++) {
                __datewise_order_list.add(value.ordersData[t].ordersList[o]);
              }
            }
            print("ORDERLIST length" + __datewise_order_list.length.toString());
            Toast.show(value.result, context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
          } else {
            isHavingData = false;
            Toast.show("No data available", context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
          }
        });
      } else if (value.responseStatus == 3) {
        setState(() {
          _loading = false;
          Toast.show(value.result, context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
        });
      } else if (value.responseStatus == 0) {
        setState(() {
          _loading = false;
          Toast.show(value.result, context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
        });
      }
    });
  }

  void defOrdersHistory() {
    GetOrderListDatewiseApiRepository().orderListDatewise(employee_id, restaurant_id).then((value) {
      print("TABLEINFORMATION RESPSTATUS  " + value.responseStatus.toString());
      debugPrint(base_url + "" + value.result.toString());
      if (value.responseStatus == 1) {
        setState(() {
          _loading = false;
          if (value.ordersData.length > 0) {
            isHavingData = true;
            date_wise_order = value.ordersData[0].date.toString();
            for (int t = 0; t < value.ordersData.length; t++) {
              _order_list.add(value.ordersData[t]);
              for (int o = 0; o < value.ordersData[t].ordersList.length; o++) {
                __datewise_order_list.add(value.ordersData[t].ordersList[o]);
              }
            }
            print("ORDERLIST length" + __datewise_order_list.length.toString());

            Toast.show(value.result, context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
          } else {
            isHavingData = false;
            Toast.show("No data available", context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
          }
        });
      } else if (value.responseStatus == 3) {
        setState(() {
          _loading = false;
          Toast.show(value.result, context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
        });
      } else if (value.responseStatus == 0) {
        setState(() {
          _loading = false;
          Toast.show(value.result, context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
        });
      }
    });
  }

  void orderHistoryById() {
    GetOrderListByUniqueIdApiRepository()
        .orderListUniqieId(employee_id, restaurant_id, _search_text_controller.text.toUpperCase().trim().toString())
        .then((value) {
      print("TABLEINFORMATION RESPSTATUS  " + value.responseStatus.toString());
      debugPrint(base_url + "" + value.result.toString());
      if (value.responseStatus == 1) {
        _order_list.clear();
        setState(() {
          _loading = false;
          if (value.ordersData.isNotEmpty) {
            isHavingData = true;
            date_wise_order = value.ordersData[0].date.toString();
            for (int t = 0; t < value.ordersData.length; t++) {
              _order_list.add(value.ordersData[t]);
              for (int o = 0; o < value.ordersData[t].ordersList.length; o++) {
                __datewise_order_list.add(value.ordersData[t].ordersList[o]);
              }
            }
            print("ORDERLIST length" + __datewise_order_list.length.toString());
            Toast.show(value.result, context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
          } else {
            isHavingData = false;
            Toast.show("No data available.!", context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
          }
        });
      } else if (value.responseStatus == 3) {
        setState(() {
          _loading = false;
          Toast.show(value.result, context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
        });
      } else if (value.responseStatus == 0) {
        setState(() {
          _loading = false;
          Toast.show(value.result, context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => _onBackPressed(),
        child: Scaffold(
          appBar: AppBar(
            toolbarHeight: toolbar_height,
            automaticallyImplyLeading: false,
            elevation: 0.0,
            backgroundColor: Colors.white,
            centerTitle: false,
            title: Text("Order History", style: new TextStyle(color: login_passcode_text, fontSize: 18.0, fontWeight: FontWeight.w700)),
            leading: Builder(
              builder: (BuildContext context) {
                return IconButton(
                  padding: EdgeInsets.only(left: 10.0),
                  icon: Image.asset("images/back_arrow.png", width: 22, height: 22),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                );
              },
            ),
            actions: [
              Container(
                  margin: const EdgeInsets.fromLTRB(15, 15, 24, 15),
                  padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                  alignment: Alignment.center,
                  child: InkWell(
                      child: Image.asset(
                        "images/calender.png",
                        height: 28,
                        width: 28,
                      ),
                      onTap: () {
                        _selectDate(context);
                      }))
            ],
          ),
          body: Container(
              padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
              color: dashboard_bg,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: [
                  ListTile(
                    title: Row(
                      children: <Widget>[
                        Expanded(
                          flex: 4,
                          child: Container(
                              margin: EdgeInsets.fromLTRB(0, 0, 5, 0),
                              height: 42,
                              child: TextField(
                                controller: _search_text_controller,
                                onChanged: (string) {
                                  setState(() {
                                    print("search_string $string");
                                    search_string = string;
                                    if (search_string == "") {
                                      _loading = true;
                                      defOrdersHistory();
                                    }
                                  });
                                },
                                textAlign: TextAlign.center,
                                //controller:coupontextfield_Controller,
                                textCapitalization: TextCapitalization.characters,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.only(
                                    bottom: 30 / 2,
                                    left: 9 / 2,
                                  ),
                                  hintText: 'Search with Order ID',
                                  hintStyle: TextStyle(
                                      fontSize: SizeConfig.safeBlockHorizontal * 3.6,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black12),
                                  filled: true,
                                  fillColor: Color(0xffffffff),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.all(Radius.circular(4.0)),
                                    borderSide: BorderSide(color: Colors.black12),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.all(Radius.circular(4.0)),
                                    borderSide: BorderSide(color: Colors.black12),
                                  ),
                                ),
                              )),
                        ),
                        Expanded(
                          flex: 2,
                          child: FlatButton(
                            height: 42,
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4.0)),
                            color: Colors.lightBlueAccent,
                            textColor: title_item_background,
                            onPressed: () {
                              FocusScope.of(context).unfocus();
                              print("BUTTONPRESSED" + _search_text_controller.text.toString() + "-----" + employee_id + "-----" + restaurant_id);
                              if (_search_text_controller.text.toString() != "") {
                                setState(() {
                                  _loading = true;
                                });
                                orderHistoryById();
                              } else {
                                Toast.show("Order ID is empty!", context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                              }
                            },
                            child: Text(
                              "Search",
                              style: TextStyle(
                                fontSize: SizeConfig.safeBlockHorizontal * 3.4,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  _loading
                      ? Expanded(
                          child: Container(
                              margin: EdgeInsets.only(left: 10, right: 10),
                              color: Colors.white,
                              child: Center(
                                child: SpinKitFadingCircle(color: Colors.lightBlueAccent),
                              )))
                      : Expanded(
                          child: Container(
                          margin: EdgeInsets.only(left: 10, right: 10),
                          child: Container(
                            color: Colors.white,
                            child: Column(children: [
                              Expanded(
                                child: isHavingData
                                    ? ListView.separated(
                                        shrinkWrap: true,
                                        physics: ClampingScrollPhysics(),
                                        scrollDirection: Axis.vertical,
                                        itemCount: _order_list.length,
                                        separatorBuilder: (context, index) {
                                          return Container(
                                            margin: EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 0.0),
                                            child: Divider(
                                              color: Colors.white,
                                            ),
                                          );
                                        },
                                        itemBuilder: (context, index) {
                                          return Container(
                                            child: Column(
                                              children: [
                                                Container(
                                                  alignment: Alignment.topLeft,
                                                  child: Padding(
                                                    padding: EdgeInsets.only(top: 10, left: 12, bottom: 0),
                                                    child: Text(
                                                      /*"${selectedDate.toLocal()}".split(' ')[0]*/
                                                      _order_list[index].date,
                                                      style: TextStyle(
                                                          color: cart_text, fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w600),
                                                    ),
                                                  ),
                                                ),
                                                Container(
                                                  margin: EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 0.0),
                                                  child: Divider(
                                                    color: cart_viewline,
                                                  ),
                                                ),
                                                ListView.separated(
                                                    shrinkWrap: true,
                                                    physics: ClampingScrollPhysics(),
                                                    scrollDirection: Axis.vertical,
                                                    itemCount: _order_list[index].ordersList.length,
                                                    separatorBuilder: (context, pindex) {
                                                      return Container(
                                                        margin: EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 0.0),
                                                        child: Divider(
                                                          color: Colors.white,
                                                        ),
                                                      );
                                                    },
                                                    itemBuilder: (context, pindex) {
                                                      return Container(
                                                          padding: EdgeInsets.all(0),
                                                          child: Card(
                                                            elevation: 2,
                                                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
                                                            child: ClipRRect(
                                                                borderRadius: BorderRadius.circular(5),
                                                                child: Column(
                                                                  children: [
                                                                    Row(
                                                                      children: [
                                                                        Expanded(
                                                                            flex: 1,
                                                                            child: Container(
                                                                                margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                                                alignment: Alignment.centerLeft,
                                                                                decoration: BoxDecoration(
                                                                                    color: add_food_item_bg, borderRadius: BorderRadius.circular(0)),
                                                                                child: Padding(
                                                                                  padding: EdgeInsets.only(left: 10, bottom: 5, top: 5),
                                                                                  child: Text(
                                                                                    "Order ID",
                                                                                    style: TextStyle(
                                                                                        color: Colors.white,
                                                                                        fontSize: 14,
                                                                                        fontFamily: 'Poppins',
                                                                                        fontWeight: FontWeight.w600),
                                                                                  ),
                                                                                ))),
                                                                        Expanded(
                                                                            flex: 2,
                                                                            child: Container(
                                                                                margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                                                alignment: Alignment.centerLeft,
                                                                                decoration: BoxDecoration(
                                                                                    color: dashboard_bg, borderRadius: BorderRadius.circular(0)),
                                                                                child: Padding(
                                                                                  padding: EdgeInsets.only(left: 10, bottom: 5, top: 5),
                                                                                  child: Text(
                                                                                    "#" + _order_list[index].ordersList[pindex].orderUniqueId,
                                                                                    style: TextStyle(
                                                                                        color: coupontextdesc,
                                                                                        fontSize: 14,
                                                                                        fontFamily: 'Poppins',
                                                                                        fontWeight: FontWeight.w600),
                                                                                  ),
                                                                                )))
                                                                      ],
                                                                    ),
                                                                    Row(
                                                                      children: [
                                                                        Expanded(
                                                                            flex: 1,
                                                                            child: Container(
                                                                                margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                                                alignment: Alignment.centerLeft,
                                                                                decoration: BoxDecoration(
                                                                                    color: orderlist_item2, borderRadius: BorderRadius.circular(0)),
                                                                                child: Padding(
                                                                                  padding: EdgeInsets.only(left: 10, bottom: 5, top: 5),
                                                                                  child: Text(
                                                                                    "Check No",
                                                                                    style: TextStyle(
                                                                                        color: Colors.white,
                                                                                        fontSize: 14,
                                                                                        fontFamily: 'Poppins',
                                                                                        fontWeight: FontWeight.w600),
                                                                                  ),
                                                                                ))),
                                                                        Expanded(
                                                                            flex: 2,
                                                                            child: Container(
                                                                                margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                                                alignment: Alignment.centerLeft,
                                                                                decoration: BoxDecoration(
                                                                                    color: Colors.white, borderRadius: BorderRadius.circular(0)),
                                                                                child: Padding(
                                                                                  padding: EdgeInsets.only(left: 10, bottom: 5, top: 5),
                                                                                  child: Text(
                                                                                    _order_list[index].ordersList[pindex].checkNumber.toString(),
                                                                                    style: TextStyle(
                                                                                        color: coupontextdesc,
                                                                                        fontSize: 14,
                                                                                        fontFamily: 'Poppins',
                                                                                        fontWeight: FontWeight.w600),
                                                                                  ),
                                                                                ))),
                                                                      ],
                                                                    ),
                                                                    Row(
                                                                      children: [
                                                                        Expanded(
                                                                            flex: 1,
                                                                            child: Container(
                                                                                margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                                                alignment: Alignment.centerLeft,
                                                                                decoration: BoxDecoration(
                                                                                    color: add_food_item_bg, borderRadius: BorderRadius.circular(0)),
                                                                                child: Padding(
                                                                                  padding: EdgeInsets.only(left: 10, bottom: 5, top: 5),
                                                                                  child: Text(
                                                                                    "Table No",
                                                                                    style: TextStyle(
                                                                                        color: Colors.white,
                                                                                        fontSize: 14,
                                                                                        fontFamily: 'Poppins',
                                                                                        fontWeight: FontWeight.w600),
                                                                                  ),
                                                                                ))),
                                                                        Expanded(
                                                                            flex: 2,
                                                                            child: Container(
                                                                                margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                                                alignment: Alignment.centerLeft,
                                                                                decoration: BoxDecoration(
                                                                                    color: dashboard_bg, borderRadius: BorderRadius.circular(0)),
                                                                                child: Padding(
                                                                                  padding: EdgeInsets.only(left: 10, bottom: 5, top: 5),
                                                                                  child: Text(
                                                                                    _order_list[index].ordersList[pindex].tableNumber.toString(),
                                                                                    style: TextStyle(
                                                                                        color: coupontextdesc,
                                                                                        fontSize: 14,
                                                                                        fontFamily: 'Poppins',
                                                                                        fontWeight: FontWeight.w600),
                                                                                  ),
                                                                                )))
                                                                      ],
                                                                    ),
                                                                    Row(
                                                                      children: [
                                                                        Expanded(
                                                                            flex: 1,
                                                                            child: Container(
                                                                                margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                                                alignment: Alignment.centerLeft,
                                                                                decoration: BoxDecoration(
                                                                                    color: orderlist_item2, borderRadius: BorderRadius.circular(0)),
                                                                                child: Padding(
                                                                                  padding: EdgeInsets.only(left: 10, bottom: 5, top: 5),
                                                                                  child: Text(
                                                                                    "Total Amount",
                                                                                    style: TextStyle(
                                                                                        color: Colors.white,
                                                                                        fontSize: 14,
                                                                                        fontFamily: 'Poppins',
                                                                                        fontWeight: FontWeight.w600),
                                                                                  ),
                                                                                ))),
                                                                        Expanded(
                                                                            flex: 2,
                                                                            child: Container(
                                                                                margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                                                alignment: Alignment.centerLeft,
                                                                                decoration: BoxDecoration(
                                                                                    color: Colors.white, borderRadius: BorderRadius.circular(0)),
                                                                                child: Padding(
                                                                                  padding: EdgeInsets.only(left: 10, bottom: 5, top: 5),
                                                                                  child: Text(
                                                                                    new String.fromCharCodes(new Runes('\u0024')) +
                                                                                        _order_list[index].ordersList[pindex].totalAmount,
                                                                                    style: TextStyle(
                                                                                        color: coupontextdesc,
                                                                                        fontSize: 14,
                                                                                        fontFamily: 'Poppins',
                                                                                        fontWeight: FontWeight.w600),
                                                                                  ),
                                                                                ))),
                                                                      ],
                                                                    ),
                                                                    Row(
                                                                      children: [
                                                                        Expanded(
                                                                            flex: 1,
                                                                            child: Container(
                                                                                margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                                                alignment: Alignment.centerLeft,
                                                                                decoration: BoxDecoration(
                                                                                    color: add_food_item_bg, borderRadius: BorderRadius.circular(0)),
                                                                                child: Padding(
                                                                                  padding: EdgeInsets.only(left: 10, bottom: 5, top: 5),
                                                                                  child: Text(
                                                                                    "Items",
                                                                                    style: TextStyle(
                                                                                        color: Colors.white,
                                                                                        fontSize: 14,
                                                                                        fontFamily: 'Poppins',
                                                                                        fontWeight: FontWeight.w600),
                                                                                  ),
                                                                                ))),
                                                                        Expanded(
                                                                            flex: 2,
                                                                            child: Container(
                                                                                margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                                                alignment: Alignment.centerLeft,
                                                                                decoration: BoxDecoration(
                                                                                    color: dashboard_bg, borderRadius: BorderRadius.circular(0)),
                                                                                child: Padding(
                                                                                  padding: EdgeInsets.only(left: 10, bottom: 5, top: 5),
                                                                                  child: Text(
                                                                                    "",
                                                                                    style: TextStyle(
                                                                                        color: coupontextdesc,
                                                                                        fontSize: 14,
                                                                                        fontFamily: 'Poppins',
                                                                                        fontWeight: FontWeight.w600),
                                                                                  ),
                                                                                )))
                                                                      ],
                                                                    )
                                                                  ],
                                                                )),)
                                                          );
                                                    }),
                                              ],
                                            ),
                                          );
                                        })
                                    : Center(
                                        child: Text(
                                          "No Data Available.",
                                          style: TextStyle(
                                              fontSize: SizeConfig.safeBlockHorizontal * 4,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color: text_split),
                                        ),
                                      ),
                              ),
                            ]),
                          ),
                        )),
                ],
              )),
        ));
  }

  Future<bool> _onBackPressed() async {
    Navigator.of(context).pop(true);
    return true;
  }
}

class CancelDialogs {
  static Future<void> showLoadingDialog(BuildContext context, GlobalKey key) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new WillPopScope(
              onWillPop: () async => false,
              child: SimpleDialog(key: key, backgroundColor: Colors.black54, children: <Widget>[
                Center(
                  child: Column(children: [
                    CircularProgressIndicator(),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Cancelling....",
                      style: TextStyle(color: Colors.lightBlueAccent),
                    )
                  ]),
                )
              ]));
        });
  }
}

Route _createRoute(String table_no) {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => SplitScreen(table_no),
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      var begin = Offset(0.0, 1.0);
      var end = Offset.zero;
      var curve = Curves.ease;

      var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

      return SlideTransition(
        position: animation.drive(tween),
        child: child,
      );
    },
  );
}

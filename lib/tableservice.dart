import 'package:async/async.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:toast/toast.dart';
import 'package:waiter/apis/GetWaiterTableNumberApi.dart';
import 'package:waiter/apis/addtableguestsapi.dart';
import 'package:waiter/model/getwaitertablenumberresponse.dart';
import 'package:waiter/session/userRepository.dart';
import 'package:waiter/utils/DialogClass.dart';
import 'package:waiter/utils/all_constans.dart';

import 'addfood_screen.dart';
import 'apis/GetWaiterServiceAreasApi.dart';
import 'model/getwaiterservicearearesponse.dart';

class TableService extends StatefulWidget {
  @override
  _TableServiceState createState() => _TableServiceState();
}

class _TableServiceState extends State<TableService> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController tablenumber_Controller = TextEditingController();
  TextEditingController noofguestsController = TextEditingController();
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  CancelableOperation cancelableOperation;
  String employee_id = "";
  String first_name = "";
  String last_name = "";
  String restaurant_id = "";
  String servicearea_id = "";
  String user_id = "";
  List<ServiceAreasList> _getwaitersservicearea = [];
  List<TableSetupDetails> _getwaitertablenumber = [];
  bool _loading = true;
  String _selectedHall;
  String selected_hall_id; // Option 2
  String _selectedTableNumber; // Option 2

  String restaurant_code = "";

  @override
  void initState() {
    super.initState();
    // print(widget.main_order_id.toString());
    UserRepository().getuserdetails().then((userdetails) {
      print("userdata" + userdetails.length.toString());
      setState(() {
        employee_id = userdetails[0];
        first_name = userdetails[1];
        last_name = userdetails[2];
        //user_id = userdetails[1];
        print("SESSIONDATAFROMLOGIN" + employee_id + "------" + first_name + "-----" + last_name);
        UserRepository().getRestaurantId().then((restaurantdetails) {
          setState(() {
            restaurant_id = restaurantdetails;
            print("restaurant_id" + restaurant_id);
            UserRepository().getServiceAreaandCentreId().then((serviceareadetails) {
              setState(() {
                servicearea_id = serviceareadetails[0];

                UserRepository().getRestaurantCode().then((restuarantcode) {
                  setState(() {
                    restaurant_code = restuarantcode;
                    print("RESTAURANTCODE" + restaurant_code);
                    GetWaiterServiceAreasApiRepository().getWaiterServiceArea(restaurant_code).then((getwaiterservicearea) {
                      setState(() {
                        _getwaitersservicearea = getwaiterservicearea;
                        selected_hall_id = _getwaitersservicearea[0].id;

                        _selectedHall = selected_hall_id;

                        print("HALLID" + selected_hall_id + "=========" + _selectedHall);
                        setState(() {
                          _loading = false;
                          print("TABLENUMBERSREQUEST" + restaurant_id + "------" + servicearea_id);
                          GetWaiterTableNumberApiRepository().getWaiterTableNumber(restaurant_id, servicearea_id).then((getwaitertablenumber) {
                            setState(() {
                              _getwaitertablenumber = getwaitertablenumber;
                              setState(() {
                                _loading = false;
                                print("TABLENUMBERDETAILS" + _getwaitertablenumber.toString());
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => _onBackPressed(),
        child: Scaffold(
            bottomSheet: Container(
                // margin: EdgeInsets.fromLTRB(40, 30, 40, 80),
                // height: 60,
                width: double.infinity,
                // alignment: Alignment.center,
                // decoration: BoxDecoration(color: login_passcode_bg1, borderRadius: BorderRadius.circular(0)),
                child: Container(
                    margin: EdgeInsets.fromLTRB(40, 30, 40, 30),
                    color: login_passcode_bg1,
                    // alignment: Alignment.bottomCenter,
                    child: TextButton(
                        // minWidth: double.infinity,
                        // height: double.infinity,
                        child:
                            Text("CONTINUE", style: TextStyle(fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w800, color: Colors.white)),
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            _formKey.currentState.save();
                            SignDialogs.showLoadingDialog(context, "Loading...", _keyLoader);
                            cancelableOperation?.cancel();
                            CancelableOperation.fromFuture(Future.delayed(Duration(seconds: 1), () {
                              print("TABLESERVICE_SUBMIT" +
                                  restaurant_id +
                                  "--------" +
                                  employee_id +
                                  "------" +
                                  servicearea_id +
                                  "-----" +
                                  _selectedTableNumber +
                                  "-----" +
                                  _selectedHall);
                              AddTableGuestApiRepository()
                                  .checkAddTableGuest(_selectedTableNumber, noofguestsController.text, employee_id, restaurant_id, _selectedHall)
                                  .then((result) {
                                print(result);
                                Navigator.of(_keyLoader.currentContext, rootNavigator: false).pop();
                                if (result.responseStatus == 0) {
                                  setState(() {
                                    Toast.show(result.result, context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                                  });
                                } else {
                                  //print("LOGINRESPONSE "+ result.userDetails.empId+"--"+ result.userDetails.id+"--"+result.userDetails.firstName+"--"+result.userDetails.lastName);
                                  // UserRepository.save_userid(result.userDetails.empId, result.userDetails.id, result.userDetails.firstName, result.userDetails.lastName);
                                  setState(() {
                                    UserRepository.save_servicearea_id(_selectedHall, _getwaitersservicearea[0].revenueCenter.toString());
                                    UserRepository.save_TablenumbernGuests(_selectedTableNumber, noofguestsController.text.toString());
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => AddFood(true, _selectedTableNumber, noofguestsController.text.toString())),
                                    );
                                  });
                                }
                              });
                            }));
                          }
                        }))),
            appBar: AppBar(
              toolbarHeight: 80,
              automaticallyImplyLeading: false,
              elevation: 0.0,
              backgroundColor: Colors.white,
              centerTitle: true,
              title: Text("", style: new TextStyle(color: Colors.black, fontSize: 24.0, fontWeight: FontWeight.w500)),
              leading: Builder(
                builder: (BuildContext context) {
                  return IconButton(
                    padding: EdgeInsets.only(left: 10.0),
                    icon: Image.asset("images/back_arrow.png", width: 22, height: 22),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  );
                },
              ),
            ),
            body: SingleChildScrollView(
              child: Container(
                  color: Colors.white,
                  margin: EdgeInsets.fromLTRB(10.00, 0.00, 10.00, 0.00),
                  child: Column(children: [
                    Container(
                      child: new Form(
                        key: _formKey, //padding: EdgeInsets.fromLTRB(8, 8, 8, 0),
                        child: new Column(
                          children: <Widget>[
                            Padding(
                                padding: EdgeInsets.symmetric(horizontal: 25, vertical: 10),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    SizedBox(
                                      height: 5.0,
                                    ),
                                    Container(
                                        margin: EdgeInsets.only(bottom: 20),
                                        child: Text(
                                          'Enter the Details',
                                          textAlign: TextAlign.left,
                                          style: TextStyle(color: login_passcode_text, fontSize: 20, fontWeight: FontWeight.bold),
                                        )),
                                    _getwaitersservicearea.length > 0
                                        ? Container(
                                            margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                            alignment: Alignment.center,
                                            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 2),
                                            child: DropdownButton(
                                              isExpanded: true,
                                              underline: SizedBox(),
                                              elevation: 8,
                                              hint: Text(_getwaitersservicearea[0].serviceName),
                                              // Not necessary for Option 1
                                              value: _selectedHall,
                                              onChanged: (newValue) {
                                                setState(() {
                                                  _selectedHall = newValue;
                                                  print("SELECTEDHALL====" + _selectedHall);
                                                  if (_selectedHall != null) {
                                                    _getwaitertablenumber.clear();

                                                    UserRepository.save_servicearea_id(
                                                        _selectedHall, _getwaitersservicearea[0].revenueCenter.toString());
                                                    print("TABLENUMBERSREQUEST" + restaurant_id + "------" + _selectedHall);
                                                    GetWaiterTableNumberApiRepository()
                                                        .getWaiterTableNumber(restaurant_id, _selectedHall)
                                                        .then((getwaitertablenumber) {
                                                      setState(() {
                                                        _getwaitertablenumber = getwaitertablenumber;
                                                        setState(() {
                                                          _loading = false;
                                                          print("TABLENUMBERDETAILS" + _getwaitertablenumber.toString());
                                                        });
                                                      });
                                                    });
                                                  }
                                                });
                                              },
                                              items: _getwaitersservicearea.map((location) {
                                                return DropdownMenuItem(
                                                  child: new Text(location.serviceName.toString()),
                                                  value: location.id.toString(),
                                                );
                                              }).toList(),
                                            ),
                                            decoration: ShapeDecoration(
                                              shape: RoundedRectangleBorder(
                                                side: BorderSide(width: 1, style: BorderStyle.solid, color: login_passcode_text),
                                                borderRadius: BorderRadius.all(Radius.circular(0.0)),
                                              ),
                                            ),
                                          )
                                        : SizedBox(),
                                    SizedBox(
                                      height: 15.0,
                                    ),
                                    _getwaitertablenumber.length > 0
                                        ? Container(
                                            margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                            alignment: Alignment.center,
                                            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 2),
                                            child: DropdownButton(
                                              isExpanded: true,
                                              underline: SizedBox(),
                                              elevation: 8,
                                              hint: Text('Please choose Table Number'),
                                              // Not necessary for Option 1
                                              value: _selectedTableNumber,
                                              onChanged: (newValue) {
                                                setState(() {
                                                  print("NEWVALUETABLE" + newValue);
                                                  _selectedTableNumber = newValue;
                                                  print("_selectedTableNumber====" + _selectedTableNumber);
                                                });
                                              },
                                              items: _getwaitertablenumber.map((tabledetails) {
                                                return DropdownMenuItem(
                                                  child: new Text(tabledetails.tableNumber.toString()),
                                                  value: tabledetails.tableNumber.toString(),
                                                );
                                              }).toList(),
                                            ),
                                            decoration: ShapeDecoration(
                                              shape: RoundedRectangleBorder(
                                                side: BorderSide(width: 1, style: BorderStyle.solid, color: login_passcode_text),
                                                borderRadius: BorderRadius.all(Radius.circular(0.0)),
                                              ),
                                            ),
                                          )
                                        : Container(
                                            margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                            alignment: Alignment.center,
                                            padding: EdgeInsets.only(left: 15, top: 10),
                                            child: Text("No Tables Found",
                                                style: TextStyle(
                                                    color: login_passcode_text, fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w600)),
                                          ),
                                    SizedBox(
                                      height: 15.0,
                                    ),
                                    /* TextFormField(
                                validator: (val) {
                                  if (val.isEmpty) return 'Enter Table Number';
                                  return null;
                                },
                                controller: tablenumber_Controller,
                                obscureText: false,
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: "Enter Table Number",
                                    contentPadding: new EdgeInsets.symmetric(
                                        vertical: 20.0, horizontal: 10.0),
                                    border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(0.0))),
                              ),
                              SizedBox(
                                height: 15.0,
                              ),*/
                                    TextFormField(
                                      validator: (val) {
                                        if (val.isEmpty) return 'Enter No. of Guests';
                                        return null;
                                      },
                                      controller: noofguestsController,
                                      obscureText: false,
                                      keyboardType: TextInputType.number,
                                      decoration: InputDecoration(
                                          filled: true,
                                          fillColor: Colors.white,
                                          hintText: "Enter No. of Guests",
                                          contentPadding: new EdgeInsets.symmetric(vertical: 20.0, horizontal: 10.0),
                                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0))),
                                    ),
                                  ],
                                )),
                          ],
                        ),
                      ),
                    ),
                  ])),
            )));
  }

  Future<bool> _onBackPressed() async {
    Navigator.of(context).pop(true);
    return true;
  }
}

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:waiter/service_areas_screen.dart';
import 'package:waiter/splashscreen.dart';
import 'package:waiter/utils/sizeconfig.dart';
import 'package:waiter/webview_screen.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        // body: ServiceAreasScreen(),
        body: SplashScreen(),
      ),
    );
  }
}

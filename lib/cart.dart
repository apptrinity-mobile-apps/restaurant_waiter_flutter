import 'dart:convert';
import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'package:waiter/apis/addwaiterorder.dart';
import 'package:waiter/apis/updatewaiterorder.dart';
import 'package:waiter/dashboard.dart';
import 'package:waiter/model/cartmodelitems.dart';
import 'package:waiter/model/cartmodelitemsapi.dart';
import 'package:waiter/receipt.dart';
import 'package:waiter/session/userRepository.dart';
import 'package:waiter/splitorderscreen.dart';
import 'package:waiter/utils/Globals.dart';
import 'package:waiter/utils/all_constans.dart';
import 'package:waiter/utils/sizeconfig.dart';

import 'addfood_screen.dart';
import 'addfooditems_screen.dart';
import 'apis/CartsRepository.dart';
import 'coupons.dart';
import 'gettableinformation.dart';
import 'model/SendTaxRatesModel.dart';

class Cart extends StatefulWidget {
  @override
  _CartState createState() => _CartState();
}

class _CartState extends State<Cart> {
  List<bool> _selected = List.generate(6, (i) => false);
  int selected_pos = -1;

  int _counter = 0;
  String _text = 'no taps yet';
  var _controller = TextEditingController(text: 'initial value');
  List<MenuCartItemapi> __cart_items_list = [];
  List<SendTaxRatesModel> taxes_arraylist = [];

  int _itemCount = 0;
  var cart_count_value = 1;
  int cart_count = 0;

  double _items_cart_itemtotal = 0.0;
  double _items_cart_discounttotal = 0.0;
  double _items_cart_modifier_price = 0.0;
  double _items_cart_special_price = 0.0;
  double _items_cart_subtotal = 0.0;
  double _items_cart_taxtotal = 0.0;
  double _items_cart_taxrate = 0.0;
  double _items_cart_grandtotal = 0.0;
  double _items_cart_grandroundup = 0.0;

  double total_discountitemprice = 0.0;
  double total_discountitemfinalprice = 0.0;
  double max_item_discount_amount = 0.0;

  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  bool _loading = true;

  String table_number_session = "",
      no_of_guests_session = "",
      dine_in_option_id = "",
      dine_in_option_name = "",
      dine_in_option_behaviour = "",
      service_area_id = "",
      revenue_centre_id = "",
      employee_id = "",
      restaurant_id = "",
      order_id_updated = "",
      deviceId = "";
  int cart_session_plus = 0;
  double cart_session_minus = 0.0;

  @override
  void initState() {
    super.initState();
    // print(widget.main_order_id.toString());
    UserRepository().getDeviceId().then((value) {
      setState(() {
        deviceId = value;
      });
    });
    Future.delayed(Duration(seconds: 2), () async {
      setState(() {
        CartsRepository().getcartslisting().then((cartList) {
          setState(() {
            __cart_items_list = cartList;
            cart_count = __cart_items_list.length;
            print("CARTSESSIONLENGTH" + cart_count.toString() + "" + __cart_items_list.toString());
            _cart_itemscalculation();
            UserRepository().getGenerateTablenumbernGuests().then((tableservicedetails) {
              setState(() {
                table_number_session = tableservicedetails[0];
                no_of_guests_session = tableservicedetails[1];
                print("TABLESERVICEDETAILS" + table_number_session + "---------" + no_of_guests_session);
                UserRepository().getDineInOptionId().then((dineinoptiondetails) {
                  setState(() {
                    dine_in_option_id = dineinoptiondetails[0];
                    dine_in_option_name = dineinoptiondetails[1];
                    dine_in_option_behaviour = dineinoptiondetails[2];
                    UserRepository().getServiceAreaandCentreId().then((serviceareadetails) {
                      setState(() {
                        service_area_id = serviceareadetails[0];
                        revenue_centre_id = serviceareadetails[1];
                        UserRepository().getuserdetails().then((userdetails) {
                          print("userdata" + userdetails.length.toString());
                          setState(() {
                            employee_id = userdetails[0];
                            UserRepository().getRestaurantId().then((restaurantdetails) {
                              setState(() {
                                restaurant_id = restaurantdetails;
                                UserRepository().getOrderId().then((orderdetailsorderid) {
                                  setState(() {
                                    order_id_updated = orderdetailsorderid[0];
                                  });
                                });
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });

            _loading = false;
          });
        });
      });
    });
  }

  Future<bool> _onBackPressed() async {
    Navigator.of(context).pop(true);
  }

  Widget _incrementButton(int index) {
    return Container(
        margin: EdgeInsets.all(3),
        child: ClipOval(
          child: Material(
            color: cart_minus_bg,
            // button color
            child: InkWell(
              splashColor: login_passcode_bg2,
              // inkwell color
              child: SizedBox(
                  width: 28,
                  height: 28,
                  child: Icon(
                    Icons.add,
                    size: 17,
                  )),
              onTap: () {
                setState(() {
                  __cart_items_list[index].quantity = (int.parse(__cart_items_list[index].quantity) + 1).toString();
                  __cart_items_list[index].modifiersList.forEach((modifier) {
                    modifier.modifierTotalPrice = double.parse(__cart_items_list[index].quantity) * modifier.modifierUnitPrice;
                  });
                  print("INCREMENTQUANTITY" + __cart_items_list[index].quantity);
                  CartsRepository().saveCartList(__cart_items_list);
                  _cart_itemscalculation();
                });
              },
            ),
          ),
        ));
  }

  Widget _decrementButton(int index) {
    return Container(
        margin: EdgeInsets.all(3),
        child: ClipOval(
          child: Material(
            color: cart_minus_bg,
            // button color
            child: InkWell(
              splashColor: login_passcode_bg2,
              // inkwell color
              child: SizedBox(
                  width: 28,
                  height: 28,
                  child: Icon(
                    Icons.remove,
                    size: 17,
                  )),
              onTap: () {
                print("qty==== ${__cart_items_list[index].quantity}");
                setState(() {
                  if (int.parse(__cart_items_list[index].quantity) > 1) {
                    __cart_items_list[index].quantity = (int.parse(__cart_items_list[index].quantity) - 1).toString();
                    __cart_items_list[index].modifiersList.forEach((modifier) {
                      modifier.modifierTotalPrice = double.parse(__cart_items_list[index].quantity) * modifier.modifierUnitPrice;
                    });
                    //_savecartList(__cart_items_list);
                  } else {
                    print(__cart_items_list[index].comboUniqueId + "--comboid--" + __cart_items_list[index].comboId);
                    if (__cart_items_list[index].comboUniqueId != '') {
                      print("remove combo");
                      __cart_items_list.removeWhere((row) => (row.comboUniqueId.contains(__cart_items_list[index].comboUniqueId)));
                      getCartItemModelapi.removeWhere((row) => (row.comboUniqueId.contains(getCartItemModelapi[index].comboUniqueId)));
                      //getCartItemModelapi.removeAt(index);
                    } else {
                      __cart_items_list.removeAt(index);
                      //getCartItemModelapi.removeAt(index);
                      print("remove item");
                    }
                    //__cart_items_list.removeAt(index);
                  }
                  CartsRepository().saveCartList(__cart_items_list);
                  _cart_itemscalculation();
                });
              },
            ),
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomSheet: Container(
          // height: 60,
          child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Expanded(
              child: Container(
                  // alignment: Alignment.center,
                  decoration: BoxDecoration(color: dashboard_quick_order, borderRadius: BorderRadius.circular(0)),
                  child: TextButton(
                      child: Text("SAVE ORDER",
                          style: TextStyle(
                              fontSize: SizeConfig.safeBlockHorizontal * 4, fontFamily: 'Poppins', fontWeight: FontWeight.w800, color: Colors.white)),
                      onPressed: () {
                        _loading = true;
                        print(__cart_items_list);
                        _items_cart_grandroundup = double.parse(_items_cart_subtotal.toStringAsFixed(2));

                        print("GRANDTOTALROUNDUP" +
                            restaurant_id +
                            "=" +
                            service_area_id +
                            "==" +
                            revenue_centre_id +
                            "===" +
                            dine_in_option_id +
                            "===" +
                            dine_in_option_name +
                            "===" +
                            dine_in_option_behaviour +
                            "====" +
                            employee_id +
                            "=====" +
                            _items_cart_grandroundup.toString() +
                            "------" +
                            _items_cart_subtotal.toString() +
                            "ORDERIDUPDATED=====" +
                            order_id_updated);

                        if (__cart_items_list.length > 0) {
                          if (order_id_updated == "" || order_id_updated == null || order_id_updated == "null") {
                            AddWaiterOrderRepository()
                                .addwaiterorder(
                                    __cart_items_list,
                                    "",
                                    "1",
                                    table_number_session,
                                    "0.00",
                                    0,
                                    _items_cart_grandroundup,
                                    double.parse(_items_cart_taxtotal.toStringAsFixed(2)),
                                    double.parse(_items_cart_grandtotal.toStringAsFixed(2)),
                                    dine_in_option_id,
                                    dine_in_option_name,
                                    dine_in_option_behaviour,
                                    service_area_id,
                                    revenue_centre_id,
                                    employee_id,
                                    restaurant_id,
                                    0,
                                    false,
                                    0,
                                    deviceId)
                                .then((result) {
                              print("CARTSAVING" + result.toString());
                              if (result.responseStatus == 0) {
                                _loading = false;
                                //Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
                                Toast.show(result.result, context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                              } else if (result.responseStatus == 1) {
                                setState(() {
                                  print("ADDWAITERORDERID" + result.orderId);
                                  _loading = false;
                                  CartsRepository.removesaved_in_cart();
                                  getCartItemModelapi.clear();
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => HomePage()),
                                  );
                                });
                              } else {
                                Toast.show(result.result, context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                              }
                            });
                          } else {
                            UpdateWaiterOrderRepository()
                                .updatewaiterorder(
                                    order_id_updated,
                                    __cart_items_list,
                                    "0",
                                    0.0,
                                    _items_cart_grandroundup,
                                    double.parse(_items_cart_taxtotal.toStringAsFixed(2)),
                                    double.parse(_items_cart_grandtotal.toStringAsFixed(2)),
                                    restaurant_id,
                                    "1",
                                    employee_id)
                                .then((result) {
                              print("UPDATEWAITERORDER" + result.toString());
                              if (result.responseStatus == 0) {
                                _loading = false;
                                //Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
                                Toast.show(result.result, context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                              } else if (result.responseStatus == 1) {
                                setState(() {
                                  print("UPDATEWAITERORDER" + result.result);
                                  _loading = false;
                                  CartsRepository.removesaved_in_cart();
                                  getCartItemModelapi.clear();
                                  UserRepository.save_OrderId("");
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => GetTableInformation("", table_number_session, true)),
                                  );
                                });
                              } else {
                                Toast.show(result.result, context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                              }
                            });
                          }
                        } else {
                          _loading = false;
                          Toast.show("Please Add items To Save Order!", context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                        }
                        print("CARTCOUNTER ---" + cart_count_value.toString());
                      }))),
          Expanded(
              child: Container(
                  // alignment: Alignment.center,
                  decoration: BoxDecoration(color: login_passcode_bg1, borderRadius: BorderRadius.circular(0)),
                  child: TextButton(
                      child: Text("SEND KITCHEN",
                          style: TextStyle(
                              fontSize: SizeConfig.safeBlockHorizontal * 4, fontFamily: 'Poppins', fontWeight: FontWeight.w800, color: Colors.white)),
                      onPressed: () {
                        _loading = true;
                        print(__cart_items_list);
                        _items_cart_grandroundup = double.parse(_items_cart_subtotal.toStringAsFixed(2));

                        print("GRANDTOTALROUNDUP" +
                            restaurant_id +
                            "=" +
                            service_area_id +
                            "==" +
                            revenue_centre_id +
                            "===" +
                            dine_in_option_id +
                            "===" +
                            dine_in_option_name +
                            "===" +
                            dine_in_option_behaviour +
                            "====" +
                            employee_id +
                            "=====" +
                            _items_cart_grandroundup.toString() +
                            "------" +
                            _items_cart_subtotal.toString() +
                            "ORDERIDUPDATED=====" +
                            order_id_updated);

                        if (__cart_items_list.length > 0) {
                          if (order_id_updated == "" || order_id_updated == null || order_id_updated == "null") {
                            double totalDiscount = 0.00;
                            __cart_items_list.forEach((element) {
                              totalDiscount += element.discountAmount;
                            });
                            AddWaiterOrderRepository()
                                .addwaiterorder(
                                    __cart_items_list,
                                    "0",
                                    "1",
                                    table_number_session,
                                    "0.00",
                                    totalDiscount,
                                    _items_cart_grandroundup,
                                    double.parse(_items_cart_taxtotal.toStringAsFixed(2)),
                                    double.parse(_items_cart_grandtotal.toStringAsFixed(2)),
                                    dine_in_option_id,
                                    dine_in_option_name,
                                    dine_in_option_behaviour,
                                    service_area_id,
                                    revenue_centre_id,
                                    employee_id,
                                    restaurant_id,
                                    0,
                                    false,
                                    1,
                                    deviceId)
                                .then((result) {
                              print("CARTSAVING" + result.toString());
                              if (result.responseStatus == 0) {
                                _loading = false;
                                // Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
                                Toast.show(result.result, context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                              } else if (result.responseStatus == 1) {
                                setState(() {
                                  print("ADDWAITERORDERID" + result.orderId);
                                  _loading = false;
                                  CartsRepository.removesaved_in_cart();
                                  getCartItemModelapi.clear();
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => HomePage()),
                                  );
                                });
                              } else {
                                Toast.show(result.result, context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                              }
                            });
                          } else {
                            UpdateWaiterOrderRepository()
                                .updatewaiterorder(
                                    order_id_updated,
                                    __cart_items_list,
                                    "0",
                                    0.0,
                                    _items_cart_grandroundup,
                                    double.parse(_items_cart_taxtotal.toStringAsFixed(2)),
                                    double.parse(_items_cart_grandtotal.toStringAsFixed(2)),
                                    restaurant_id,
                                    "1",
                                    employee_id)
                                .then((result) {
                              print("UPDATEWAITERORDER" + result.toString());
                              if (result.responseStatus == 0) {
                                _loading = false;
                                // Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
                                Toast.show(result.result, context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                              } else if (result.responseStatus == 1) {
                                setState(() {
                                  print("UPDATEWAITERORDER" + result.result);
                                  _loading = false;
                                  CartsRepository.removesaved_in_cart();
                                  getCartItemModelapi.clear();
                                  UserRepository.save_OrderId("");
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => GetTableInformation(order_id_updated, table_number_session, true)),
                                  );
                                });
                              } else {
                                Toast.show(result.result, context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                              }
                            });
                          }
                        } else {
                          _loading = false;

                          Toast.show("Please Add items To Send Kitchen", context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                        }

                        print("CARTCOUNTER ---" + cart_count_value.toString());
                      })))
        ],
      )),

      /*Container(
          margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
          height: 60,
          alignment: Alignment.center,
          decoration: BoxDecoration(
              color: login_passcode_bg1,
              borderRadius: BorderRadius.circular(0)),
          child: FlatButton(
              minWidth: double.infinity,
              height: double.infinity,
              child: Text("Send to Kitchen",
                  style: TextStyle(
                      fontSize: 16,
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.w800,
                      color: Colors.white)),
              onPressed: () {
                _loading = true;
                print(__cart_items_list);
                _items_cart_grandroundup =   double.parse(_items_cart_subtotal.toStringAsFixed(2));

                print("GRANDTOTALROUNDUP"+restaurant_id+"="+service_area_id+"=="+revenue_centre_id+"==="+dine_in_option_id+"===="+employee_id+"====="+_items_cart_grandroundup.toString()+"------"+_items_cart_subtotal.toString() +"ORDERIDUPDATED====="+order_id_updated);

                if(__cart_items_list.length > 0) {
                  if (order_id_updated == "" || order_id_updated == null || order_id_updated == "null") {
                    AddWaiterOrderRepository().addwaiterorder(
                        __cart_items_list,
                        "12",
                        "1",
                        table_number_session,
                        "0.00",
                        0,
                        _items_cart_grandroundup,
                        0,
                        _items_cart_grandroundup,
                        dine_in_option_id,
                        "Dine In",
                        "dine_in",
                        service_area_id,
                        revenue_centre_id,
                        employee_id,
                        restaurant_id,
                        0,
                        false,
                        1).then((result) {
                      print("CARTSAVING" + result.toString());
                      if (result.responseStatus == 0) {
                        _loading = false;
                        Navigator
                            .of(
                            _keyLoader
                                .currentContext,
                            rootNavigator: true)
                            .pop();
                        Toast.show("No Data Found", context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                      } else {
                        setState(
                                () {
                              print("ADDWAITERORDERID" + result.orderId);
                              _loading = false;
                              CartsRepository.removesaved_in_cart();
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => HomePage()),
                              );
                            }
                        );
                      }
                    });
                  } else {
                    UpdateWaiterOrderRepository().updatewaiterorder(
                        order_id_updated,
                        __cart_items_list,
                        "0",
                        0.0,
                        _items_cart_grandroundup,
                        0,
                        _items_cart_grandroundup,
                        restaurant_id,
                        "1",
                        employee_id).then((result) {
                      print("UPDATEWAITERORDER" + result.toString());
                      if (result.responseStatus == 0) {
                        _loading = false;
                        Navigator
                            .of(
                            _keyLoader
                                .currentContext,
                            rootNavigator: true)
                            .pop();
                        Toast.show("No Data Found", context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                      } else {
                        setState(
                                () {
                              print("UPDATEWAITERORDER" + result.result);
                              _loading = false;
                              CartsRepository.removesaved_in_cart();
                              UserRepository.save_OrderId("");
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => GetTableInformation(int.parse(table_number_session))),
                              );
                            }
                        );
                      }
                    });
                  }
                }else{
                  _loading = false;

                  Toast.show("Please Add items To Send Kitchen", context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                }

                print("CARTCOUNTER ---"+cart_count_value.toString());
              }))*/

      /*Column(mainAxisAlignment:MainAxisAlignment.end,crossAxisAlignment:CrossAxisAlignment.end,children: [Container(
          margin: EdgeInsets.fromLTRB(50, 30, 50, 0),
          height: 60,
          alignment: Alignment.center,
          decoration: BoxDecoration(
              color: login_passcode_bg1,
              borderRadius: BorderRadius.circular(0)),
          child: FlatButton(
              minWidth: double.infinity,
              height: double.infinity,
              child: Text("Save",
                  style: TextStyle(
                      fontSize: 16,
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.w800,
                      color: Colors.white)),
              onPressed: () {
                _loading = true;
                print(__cart_items_list);
                _items_cart_grandroundup =   double.parse(_items_cart_grandtotal.toStringAsFixed(2));

                print("GRANDTOTALROUNDUP"+_items_cart_grandroundup.toString()+"------"+_items_cart_subtotal.toString());

                AddWaiterOrderRepository().addwaiterorder(__cart_items_list,"12","1",table_number_session,"0.00",0,_items_cart_grandroundup,0,_items_cart_grandroundup,"5f69b297d7c855443fc6fb3d","Dine In","dine_in","5f0fe4f05f1497a59eae4f8b","5f23a1161115c19a00ffe337","5ec628eb0ae0b6043743eab8",restaurantId,0,false,0).then((result){
                  print("CARTSAVING"+result.toString());
                  if(result.responseStatus == 0){
                    _loading = false;
                    Navigator
                        .of(
                        _keyLoader
                            .currentContext,
                        rootNavigator: true)
                        .pop();
                    Toast.show("No Data Found", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
                  }else {
                    setState(
                            () {
                          print("ADDWAITERORDERID"+result.orderId);
                          _loading = false;
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => HomePage()),
                          );
                        }
                    );
                  }
                });
                print("CARTCOUNTER ---"+cart_count_value.toString());
              })),Container(
          margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
          height: 60,
          alignment: Alignment.center,
          decoration: BoxDecoration(
              color: login_passcode_bg1,
              borderRadius: BorderRadius.circular(0)),
          child: FlatButton(
              minWidth: double.infinity,
              height: double.infinity,
              child: Text("Send to Kitchen",
                  style: TextStyle(
                      fontSize: 16,
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.w800,
                      color: Colors.white)),
              onPressed: () {
                _loading = true;
                print(__cart_items_list);
                _items_cart_grandroundup =   double.parse(_items_cart_grandtotal.toStringAsFixed(2));

                print("GRANDTOTALROUNDUP"+_items_cart_grandroundup.toString()+"------"+_items_cart_subtotal.toString());

                AddWaiterOrderRepository().addwaiterorder(__cart_items_list,"12","1",table_number_session,"0.00",0,_items_cart_grandroundup,0,_items_cart_grandroundup,"5f69b297d7c855443fc6fb3d","Dine In","dine_in","5f0fe4f05f1497a59eae4f8b","5f23a1161115c19a00ffe337","5ec628eb0ae0b6043743eab8",restaurantId,0,false,1).then((result){
                  print("CARTSAVING"+result.toString());
                  if(result.responseStatus == 0){
                    _loading = false;
                    Navigator
                        .of(
                        _keyLoader
                            .currentContext,
                        rootNavigator: true)
                        .pop();
                    Toast.show("No Data Found", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
                  }else {
                    setState(
                            () {
                          print("ADDWAITERORDERID"+result.orderId);
                          _loading = false;
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => HomePage()),
                          );
                        }
                    );
                  }
                });


                print("CARTCOUNTER ---"+cart_count_value.toString());
                */
      /*Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Receipt()),
                );*/
      /*
              })),],),*/
      appBar: AppBar(
        toolbarHeight: 80,
        automaticallyImplyLeading: false,
        elevation: 0.0,
        backgroundColor: Colors.white,
        centerTitle: false,
        title: Text("Cart", style: new TextStyle(color: login_passcode_text, fontSize: 20.0, fontWeight: FontWeight.w600)),
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              padding: EdgeInsets.only(left: 10.0),
              icon: Image.asset("images/back_arrow.png", width: 20, height: 20),
              onPressed: () {
                _onBackPressed();
                //Navigator.of(context).pop();
              },
            );
          },
        ),
        actions: [
          new Padding(
            padding: const EdgeInsets.fromLTRB(12.0, 10, 12.0, 12.0),
            child: new Container(
                margin: const EdgeInsets.fromLTRB(12.0, 10, 12.0, 12.0),
                alignment: FractionalOffset.center,
                child: new GestureDetector(
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Image.asset(
                        "images/online_order.png",
                        height: 24,
                        width: 24,
                      ),
                      SizedBox(width: 8),
                      Text(
                        'Order',
                        style: TextStyle(color: login_passcode_text, fontSize: 18, fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                )),
          )
        ],
      ),
      body: WillPopScope(
        onWillPop: () async => _onBackPressed(),
        child:_loading
          ? Center(
              child: SpinKitFadingCircle(color: Colors.lightBlueAccent),
            )
          : Container(
              child: SingleChildScrollView(
                child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                    margin: EdgeInsets.only(bottom: 60),
                    color: Colors.white,
                    child: Column(
                      children: [
                        Container(
                            color: dashboard_bg,
                            margin: EdgeInsets.only(left: 0, right: 0),
                            padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 12.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    new Image.asset(
                                      "images/round_table.png",
                                      height: 48,
                                      width: 48,
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(left: 10, bottom: 0),
                                      child: RichText(
                                          text: TextSpan(children: [
                                        TextSpan(
                                            text: "Table no\n",
                                            style:
                                                TextStyle(color: add_food_item_bg, fontSize: 14, fontFamily: 'Poppins', fontWeight: FontWeight.w400)),
                                        TextSpan(
                                            text: table_number_session,
                                            style: new TextStyle(
                                                fontSize: 18, color: add_food_item_bg, fontFamily: 'Poppins', fontWeight: FontWeight.w700))
                                      ])),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    new Image.asset(
                                      "images/round_customers_icon.png",
                                      height: 48,
                                      width: 48,
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(left: 10, bottom: 0),
                                      child: RichText(
                                          text: TextSpan(children: [
                                        TextSpan(
                                            text: "Customers\n",
                                            style:
                                                TextStyle(color: add_food_item_bg, fontSize: 14, fontFamily: 'Poppins', fontWeight: FontWeight.w400)),
                                        TextSpan(
                                            text: no_of_guests_session == null ? 0 : no_of_guests_session,
                                            style: new TextStyle(
                                                fontSize: 18, color: add_food_item_bg, fontFamily: 'Poppins', fontWeight: FontWeight.w700))
                                      ])),
                                    ),
                                  ],
                                )
                              ],
                            )),
                        Container(
                            color: dashboard_bg,
                            //margin: EdgeInsets.all(15),
                            height: 450,
                            child: Card(
                              margin: EdgeInsets.only(left: 10, right: 10),
                              elevation: 3,
                              child: Container(
                                color: Colors.white,
                                child: Column(children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Padding(
                                        padding: EdgeInsets.only(top: 15.0, left: 15, bottom: 0),
                                        child: Text("Quick Orders",
                                            style: TextStyle(color: cart_text, fontSize: 17, fontFamily: 'Poppins', fontWeight: FontWeight.w600)),
                                      ),
                                    ],
                                  ),
                                  Container(
                                    margin: EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 0.0),
                                    child: Divider(
                                      color: cart_viewline,
                                    ),
                                  ),
                                  Expanded(
                                    child: ListView.builder(
                                        shrinkWrap: true,
                                        physics: ClampingScrollPhysics(),
                                        /* separatorBuilder: (context, index) => Divider(
                                            color: Colors.grey,
                                            height: 0.1,
                                          ),*/
                                        scrollDirection: Axis.vertical,
                                        itemCount: __cart_items_list.length,
                                        itemBuilder: (context, index) {
                                          print(__cart_items_list[index].itemName + "--unitprice--" + __cart_items_list[index].unitPrice);

                                          return Container(
                                              margin: EdgeInsets.all(5),
                                              width: MediaQuery.of(context).size.width * 0.3,
                                              child: Column(
                                                children: [
                                                  __cart_items_list[index].comboUniqueId == ""
                                                      ? Container(
                                                          child: Divider(
                                                            color: cart_viewline,
                                                          ),
                                                        )
                                                      : SizedBox(),
                                                  __cart_items_list[index].comboId != '' && __cart_items_list[index].unitPrice != '0'
                                                      ? Text(
                                                          "Combo",
                                                          style: TextStyle(
                                                              color: login_passcode_text,
                                                              fontSize: 16,
                                                              fontFamily: 'Poppins',
                                                              fontWeight: FontWeight.w600),
                                                          textAlign: TextAlign.start,
                                                        )
                                                      : SizedBox(),
                                                  Row(
                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                    children: <Widget>[
                                                      Expanded(
                                                          flex: 4,
                                                          child: Padding(
                                                              padding: EdgeInsets.only(left: 15),
                                                              child: Text(
                                                                __cart_items_list[index].itemName.toString(),
                                                                style: TextStyle(
                                                                    color: login_passcode_text,
                                                                    fontSize: 16,
                                                                    fontFamily: 'Poppins',
                                                                    fontWeight: FontWeight.w600),
                                                                textAlign: TextAlign.start,
                                                              ))),
                                                      __cart_items_list[index].unitPrice != '0'
                                                          ? Expanded(
                                                              flex: 0,
                                                              child: Padding(
                                                                padding: EdgeInsets.only(left: 5, right: 0),
                                                                child: Row(
                                                                  children: [
                                                                    _decrementButton(index),
                                                                    Container(
                                                                        margin: EdgeInsets.all(3),
                                                                        child: Column(
                                                                          children: [
                                                                            Text('${__cart_items_list[index].quantity}',
                                                                                style: TextStyle(
                                                                                    color: cart_text,
                                                                                    fontSize: 16,
                                                                                    fontFamily: 'Poppins',
                                                                                    fontWeight: FontWeight.w600)),
                                                                          ],
                                                                        )),
                                                                    _incrementButton(index),
                                                                  ],
                                                                ),
                                                              ))
                                                          : SizedBox(),
                                                      Expanded(
                                                          flex: 0,
                                                          child: Container(
                                                            alignment: Alignment.topRight,
                                                            margin: EdgeInsets.only(right: 25),
                                                            child: Text(
                                                              __cart_items_list[index].unitPrice != '0'
                                                                  ? '${new String.fromCharCodes(new Runes('\u0024')) + (double.parse(__cart_items_list[index].unitPrice) * double.parse(__cart_items_list[index].quantity)).toStringAsFixed(2)}'
                                                                  : '',
                                                              style: TextStyle(
                                                                  color: cart_minus,
                                                                  fontSize: 16,
                                                                  fontFamily: 'Poppins',
                                                                  fontWeight: FontWeight.w600),
                                                              textAlign: TextAlign.right,
                                                            ),
                                                          )),
                                                    ],
                                                  ),
                                                  __cart_items_list[index].modifiersList.isNotEmpty
                                                      ? ListView.builder(
                                                          shrinkWrap: true,
                                                          physics: ClampingScrollPhysics(),
                                                          scrollDirection: Axis.vertical,
                                                          itemCount: __cart_items_list[index].modifiersList.length,
                                                          itemBuilder: (context, i) {
                                                            return Container(
                                                              margin: EdgeInsets.all(2),
                                                              width: MediaQuery.of(context).size.width * 0.3,
                                                              child: Row(
                                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                children: <Widget>[
                                                                  Expanded(
                                                                    flex: 5,
                                                                    child: Padding(
                                                                      padding: EdgeInsets.only(left: 10, right: 15),
                                                                      child: Text(
                                                                        __cart_items_list[index].modifiersList[i].modifierName.toString(),
                                                                        style: TextStyle(
                                                                            color: coupontext,
                                                                            fontSize: 16,
                                                                            fontFamily: 'Poppins',
                                                                            fontWeight: FontWeight.w400),
                                                                        maxLines: 2,
                                                                        textAlign: TextAlign.start,
                                                                        softWrap: true,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  Expanded(
                                                                    flex: 0,
                                                                    child: Padding(
                                                                      padding: EdgeInsets.only(left: 0, right: 5),
                                                                      child: Text(
                                                                        new String.fromCharCodes(new Runes('\u0024')) +
                                                                            (__cart_items_list[index]
                                                                                    .modifiersList[i]
                                                                                    .modifierTotalPrice /** double.parse(__cart_items_list[index].quantity)*/)
                                                                                .toStringAsFixed(2),
                                                                        style: TextStyle(
                                                                            color: coupontext,
                                                                            fontSize: 14,
                                                                            fontFamily: 'Poppins',
                                                                            fontWeight: FontWeight.w400),
                                                                        textAlign: TextAlign.start,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  Expanded(
                                                                      flex: 0,
                                                                      child: InkWell(
                                                                          child: Padding(
                                                                              padding: EdgeInsets.only(left: 0, right: 5),
                                                                              child: Image.asset(
                                                                                'images/cancel.png',
                                                                                height: 20,
                                                                                width: 20,
                                                                              )),
                                                                          onTap: () {
                                                                            setState(() {
                                                                              __cart_items_list[index].modifiersList.removeAt(i);
                                                                              CartsRepository().saveCartList(__cart_items_list);
                                                                              _cart_itemscalculation();
                                                                            });
                                                                          }))
                                                                ],
                                                              ),
                                                            );
                                                          })
                                                      : SizedBox(),
                                                  __cart_items_list[index].specialRequestList.isNotEmpty
                                                      ? ListView.builder(
                                                          shrinkWrap: true,
                                                          physics: ClampingScrollPhysics(),
                                                          scrollDirection: Axis.vertical,
                                                          itemCount: __cart_items_list[index].specialRequestList.length,
                                                          itemBuilder: (context, i) {
                                                            return Container(
                                                              margin: EdgeInsets.all(2),
                                                              width: MediaQuery.of(context).size.width * 0.3,
                                                              child: Row(
                                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                children: <Widget>[
                                                                  Expanded(
                                                                      flex: 5,
                                                                      child: Padding(
                                                                          padding: EdgeInsets.only(left: 10, right: 0),
                                                                          child: Text(
                                                                            __cart_items_list[index].specialRequestList[i].name.toString(),
                                                                            style: TextStyle(
                                                                                color: coupontext,
                                                                                fontSize: 14,
                                                                                fontFamily: 'Poppins',
                                                                                fontWeight: FontWeight.w400),
                                                                            textAlign: TextAlign.start,
                                                                          ))),
                                                                  Expanded(
                                                                      flex: 2,
                                                                      child: Padding(
                                                                        padding: EdgeInsets.only(left: 0, right: 5),
                                                                        child: Text(
                                                                          new String.fromCharCodes(new Runes('\u0024')) +
                                                                              __cart_items_list[index]
                                                                                  .specialRequestList[i]
                                                                                  .requestPrice
                                                                                  .toStringAsFixed(2),
                                                                          style: TextStyle(
                                                                              color: coupontext,
                                                                              fontSize: 14,
                                                                              fontFamily: 'Poppins',
                                                                              fontWeight: FontWeight.w400),
                                                                          textAlign: TextAlign.end,
                                                                        ),
                                                                      )),
                                                                  Expanded(
                                                                      flex: 0,
                                                                      child: InkWell(
                                                                          child: Padding(
                                                                              padding: EdgeInsets.only(left: 0, right: 5),
                                                                              child: Image.asset(
                                                                                'images/cancel.png',
                                                                                height: 20,
                                                                                width: 20,
                                                                              )),
                                                                          onTap: () {
                                                                            setState(() {
                                                                              __cart_items_list[index].specialRequestList.removeAt(i);
                                                                              CartsRepository().saveCartList(__cart_items_list);
                                                                              _cart_itemscalculation();
                                                                            });
                                                                          }))
                                                                ],
                                                              ),
                                                            );
                                                          })
                                                      : SizedBox(),
                                                  __cart_items_list[index].discountAmount > 0
                                                      ? Row(
                                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                          children: <Widget>[
                                                            Padding(
                                                                padding: EdgeInsets.only(left: 10, right: 20),
                                                                child: Text(
                                                                  __cart_items_list[index].discountName.toString(),
                                                                  style: TextStyle(
                                                                      color: coupontext,
                                                                      fontSize: 16,
                                                                      fontFamily: 'Poppins',
                                                                      fontWeight: FontWeight.w500),
                                                                  textAlign: TextAlign.start,
                                                                )),
                                                            Padding(
                                                                padding: EdgeInsets.only(left: 0, right: 10),
                                                                child: Text(
                                                                  new String.fromCharCodes(new Runes('\u0024')) +
                                                                      __cart_items_list[index].discountAmount.toStringAsFixed(2),
                                                                  style: TextStyle(
                                                                      color: coupontext,
                                                                      fontSize: 16,
                                                                      fontFamily: 'Poppins',
                                                                      fontWeight: FontWeight.w500),
                                                                  textAlign: TextAlign.start,
                                                                )),
                                                            Container(
                                                              alignment: Alignment.topRight,
                                                              child: Padding(
                                                                padding: EdgeInsets.only(left: 0, right: 35),
                                                                child: Text(
                                                                  new String.fromCharCodes(new Runes('\u0024')) +
                                                                      ((double.parse(__cart_items_list[index].unitPrice) -
                                                                                  __cart_items_list[index].discountAmount) *
                                                                              double.parse(__cart_items_list[index].quantity))
                                                                          .toStringAsFixed(2),
                                                                  style: TextStyle(
                                                                      color: coupontext,
                                                                      fontSize: 16,
                                                                      fontFamily: 'Poppins',
                                                                      fontWeight: FontWeight.w500),
                                                                  textAlign: TextAlign.start,
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        )
                                                      : SizedBox(),
                                                  __cart_items_list[index].comboUniqueId == ""
                                                      ? Container(
                                                          child: Divider(
                                                            color: cart_viewline,
                                                          ),
                                                        )
                                                      : SizedBox(),
                                                ],
                                              ));
                                        }),
                                  ),
                                  Container(
                                    margin: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 0.0),
                                    alignment: Alignment.topRight,
                                    child: Padding(
                                      padding: EdgeInsets.only(left: 15, bottom: 0),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.end,
                                        children: [
                                          RichText(
                                              text: TextSpan(
                                            children: [
                                              TextSpan(
                                                  text: "Bill Amount : ",
                                                  style: new TextStyle(
                                                      fontSize: 16, color: text_split, fontFamily: 'Poppins', fontWeight: FontWeight.w500))
                                            ],
                                          )),
                                          RichText(
                                              text: TextSpan(children: [
                                            TextSpan(
                                                text: new String.fromCharCodes(new Runes('\u0024')) + _items_cart_subtotal.toStringAsFixed(2),
                                                style: new TextStyle(
                                                    fontSize: 16, color: text_split, fontFamily: 'Poppins', fontWeight: FontWeight.w500))
                                          ]))
                                        ],
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 0.0),
                                    alignment: Alignment.topRight,
                                    child: Padding(
                                      padding: EdgeInsets.only(left: 15, bottom: 0),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.end,
                                        children: [
                                          RichText(
                                              text: TextSpan(
                                            children: [
                                              TextSpan(
                                                  text: "Tax Total : ",
                                                  style: new TextStyle(
                                                      fontSize: 16, color: text_split, fontFamily: 'Poppins', fontWeight: FontWeight.w500))
                                            ],
                                          )),
                                          RichText(
                                              text: TextSpan(children: [
                                            TextSpan(
                                                text: new String.fromCharCodes(new Runes('\u0024')) + _items_cart_taxtotal.toStringAsFixed(2),
                                                style: new TextStyle(
                                                    fontSize: 16, color: text_split, fontFamily: 'Poppins', fontWeight: FontWeight.w500))
                                          ]))
                                        ],
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 0.0),
                                    alignment: Alignment.topRight,
                                    child: Padding(
                                      padding: EdgeInsets.only(left: 15, bottom: 0),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.end,
                                        children: [
                                          RichText(
                                              text: TextSpan(
                                            children: [
                                              TextSpan(
                                                  text: "Discount : ",
                                                  style: new TextStyle(
                                                      fontSize: 16, color: text_split, fontFamily: 'Poppins', fontWeight: FontWeight.w500))
                                            ],
                                          )),
                                          RichText(
                                              text: TextSpan(children: [
                                            TextSpan(
                                                text: new String.fromCharCodes(new Runes('\u0024')) + _items_cart_discounttotal.toStringAsFixed(2),
                                                style: new TextStyle(
                                                    fontSize: 16, color: text_split, fontFamily: 'Poppins', fontWeight: FontWeight.w500))
                                          ]))
                                        ],
                                      ),
                                    ),
                                  ),
                                ]),
                              ),
                            )),
                        Container(
                          margin: EdgeInsets.fromLTRB(15.0, 5.0, 30.0, 0.0),
                          alignment: Alignment.topRight,
                          child: Padding(
                            padding: EdgeInsets.only(left: 15, bottom: 0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                RichText(
                                    text: TextSpan(
                                  children: [
                                    TextSpan(
                                        text: "Grand Total : ",
                                        style: new TextStyle(
                                            fontSize: 18, color: login_passcode_text, fontFamily: 'Poppins', fontWeight: FontWeight.w500))
                                  ],
                                )),
                                RichText(
                                    text: TextSpan(children: [
                                  TextSpan(
                                      text: new String.fromCharCodes(new Runes('\u0024')) + _items_cart_grandtotal.toStringAsFixed(2),
                                      style:
                                          new TextStyle(fontSize: 18, color: login_passcode_text, fontFamily: 'Poppins', fontWeight: FontWeight.w700))
                                ]))
                              ],
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(15, 12, 15, 0),
                          width: double.infinity,
                          color: login_passcode_star,
                          child: TextButton(
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => Coupons()),
                              );
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text("APPLY COUPON",
                                    style: TextStyle(fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w800, color: Colors.white)),
                                Padding(
                                    padding: EdgeInsets.only(right: 15),
                                    child: Image.asset(
                                      'images/menu_arrow.png',
                                      height: 20,
                                      width: 20,
                                      color: Colors.white,
                                    )),
                              ],
                            ),
                          ),
                        )
                      ],
                    )),
              ),
            ),)
    );
  }

  _cart_itemscalculation() {
    CartsRepository().getcartslisting().then((cartList) {
      setState(() {
        print(cartList.length);
        cart_count = cartList.length;
        //_items_cart_subtotal = 0.0;
        List<MenuCartItemapi> _cart_list = cartList;
        if (_cart_list.length < 0) {
          _items_cart_subtotal = 0.0;
          _items_cart_grandtotal = 0.0;
          _items_cart_discounttotal = 0.0;
          _items_cart_taxtotal = 0.0;
        } else {
          _items_cart_subtotal = 0.0;
          _items_cart_grandtotal = 0.0;
          _items_cart_discounttotal = 0.0;
          _items_cart_taxtotal = 0.0;

          for (var i = 0; i < _cart_list.length; i++) {
            _items_cart_itemtotal = 0.0;
            _items_cart_itemtotal = (double.parse(_cart_list[i].unitPrice) * double.parse(_cart_list[i].quantity));
            for (var t = 0; t < _cart_list[i].taxesList.length; t++) {
              _items_cart_taxrate = _cart_list[i].taxesList[t].taxRate;
              print("ITEMTAXRATE" + _items_cart_taxrate.toString());
              // _items_cart_taxtotal =  _items_cart_taxtotal +  (_cart_list[i].taxesList[t].tax);
              _items_cart_taxrate = _items_cart_itemtotal * (_items_cart_taxrate / 100);
              print("ITEMTOTALANDTAXRATE" + _items_cart_taxrate.toStringAsFixed(2) + "=====" + _items_cart_taxtotal.toStringAsFixed(2));
              _items_cart_taxtotal = _items_cart_taxtotal + _items_cart_taxrate;
              print("CARTTAXTOTAL" +
                  _items_cart_itemtotal.toString() +
                  "------" +
                  _items_cart_taxtotal.toStringAsFixed(2) +
                  "TAXRATEPERCENT" +
                  _items_cart_taxrate.toStringAsFixed(2));
            }
            _items_cart_discounttotal = _items_cart_discounttotal + _cart_list[i].discountAmount;
            print(i.toString() + "--" + _cart_list[i].discountAmount.toString());
            _items_cart_modifier_price = 0.0;
            for (int m = 0; m < _cart_list[i].modifiersList.length; m++) {
              _items_cart_modifier_price =
                  (_cart_list[i].modifiersList[m].modifierUnitPrice * double.parse(_cart_list[i].quantity)) + _items_cart_modifier_price;
            }
            _items_cart_special_price = 0.0;
            for (int s = 0; s < _cart_list[i].specialRequestList.length; s++) {
              _items_cart_special_price = _cart_list[i].specialRequestList[s].requestPrice + _items_cart_special_price;
            }
            print("BEFOREADD=====" +
                _items_cart_subtotal.toString() +
                "-----" +
                _items_cart_modifier_price.toString() +
                "-----" +
                _items_cart_special_price.toString());
            _items_cart_itemtotal = _items_cart_itemtotal + _items_cart_modifier_price + _items_cart_special_price;
            _items_cart_subtotal = _items_cart_subtotal + _items_cart_itemtotal;
            _items_cart_grandtotal = _items_cart_subtotal - _items_cart_discounttotal + _items_cart_taxtotal;
          }
          print(_items_cart_discounttotal.toString() +
              "--" +
              _items_cart_grandtotal.toString() +
              "TAXRATE" +
              _items_cart_taxrate.toString() +
              "TAXTOTAL" +
              _items_cart_taxtotal.toStringAsFixed(2));
        }
      });
    });
  }
}

class CancelDialogs {
  static Future<void> showLoadingDialog(BuildContext context, GlobalKey key) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new WillPopScope(
              onWillPop: () async => false,
              child: SimpleDialog(key: key, backgroundColor: Colors.black54, children: <Widget>[
                Center(
                  child: Column(children: [
                    CircularProgressIndicator(),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Cancelling....",
                      style: TextStyle(color: Colors.lightBlueAccent),
                    )
                  ]),
                )
              ]));
        });
  }
}

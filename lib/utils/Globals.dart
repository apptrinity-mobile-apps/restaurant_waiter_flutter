


import 'dart:math';

import 'package:waiter/model/cartmodelitems.dart';
import 'package:waiter/model/cartmodelitemsapi.dart';
import 'package:waiter/model/gettableinformationresponse.dart';


extension CapExtension on String {
  String get inCaps => '${this[0].toUpperCase()}${this.substring(1)}';
  String get allInCaps => this.toUpperCase();
  String get capitalizeFirstofEach => this.split(" ").map((str) => str.inCaps).join(" ");
}

List<MenuCartItem> getCartItemModel = [];
List<MenuCartItemapi> getCartItemModelapi = [];
List<ItemsList> getmtableenuitemsapi = [];

double alwaysDown(double value, int precision) {
  final isNegative = value.isNegative;
  final mod = pow(10.0, precision);
  final roundDown = (((value.abs() * mod).floor()) / mod);
  return isNegative ? -roundDown : roundDown;
}

double alwaysUp(double value) {
  final isNegative = value.isNegative;
  double offSet = ((value.sign) / 100);
  final roundUp = value + offSet;
  return isNegative ? -roundUp : roundUp;
}

double getNumber(double input, {int precision = 2}) => double.parse(
    '$input'.substring(0, '$input'.indexOf('.') + precision + 1));

double halfUp(double val, int places) {
  double mod = pow(10.0, places);
  return ((val * mod).round().toDouble() / mod);
}

double halfEven(double val, int places) {
  String y = val.toString().split('.')[1];
  List<String> result = y.split('');
  if (int.parse(result[1]).isEven && int.parse(result[2]) > 5) {
    double result = val + 0.0;
    return getNumber(result, precision: 2);
  } else if (int.parse(result[1]).isEven && int.parse(result[2]) <= 5) {
    double result = val + 0.0;
    return getNumber(result, precision: 2);
  } else if (int.parse(result[1]).isOdd && int.parse(result[2]) < 5) {
    double result = val + 0.0;

    return getNumber(result, precision: 2);
  } else if (int.parse(result[1]).isOdd && int.parse(result[2]) >= 5) {
    double result = val + 0.010;
    return getNumber(result, precision: 2);
  }
}
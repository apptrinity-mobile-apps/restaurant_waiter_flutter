class get_all_bogo_model {
  List<DiscountsBogo> discounts;
  int responseStatus;
  String result;

  get_all_bogo_model({this.discounts, this.responseStatus, this.result});

  get_all_bogo_model.fromJson(Map<String, dynamic> json) {
    if (json['discounts'] != null) {
      discounts = [];
      json['discounts'].forEach((v) {
        discounts.add(new DiscountsBogo.fromJson(v));
      });
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.discounts != null) {
      data['discounts'] = this.discounts.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class DiscountsBogo {
  bool allowOtherDiscounts;
  Null appliesTo;
  String buyDiscountValue;
  List<BuyItems> buyItems;
  int discountApplyType;
  String discountValue;
  String discountValueType;
  String getDiscountValue;
  List<GetItems> getItems;
  String id;
  double maxDiscountAmount;
  double minDiscountAmount;
  String name;
  int orderValue;
  int status;
  String type;
  String uniqueNumber;
  Null value;

  DiscountsBogo(
      {this.allowOtherDiscounts,
      this.appliesTo,
      this.buyDiscountValue,
      this.buyItems,
      this.discountApplyType,
      this.discountValue,
      this.discountValueType,
      this.getDiscountValue,
      this.getItems,
      this.id,
      this.maxDiscountAmount,
      this.minDiscountAmount,
      this.name,
      this.orderValue,
      this.status,
      this.type,
      this.uniqueNumber,
      this.value});

  DiscountsBogo.fromJson(Map<String, dynamic> json) {
    allowOtherDiscounts = json['allowOtherDiscounts'];
    appliesTo = json['appliesTo'];
    buyDiscountValue = json['buyDiscountValue'];
    if (json['buyItems'] != null) {
      buyItems = [];
      json['buyItems'].forEach((v) {
        buyItems.add(new BuyItems.fromJson(v));
      });
    }
    discountApplyType = json['discountApplyType'];
    discountValue = json['discountValue'];
    discountValueType = json['discountValueType'];
    getDiscountValue = json['getDiscountValue'];
    if (json['getItems'] != null) {
      getItems = [];
      json['getItems'].forEach((v) {
        getItems.add(new GetItems.fromJson(v));
      });
    }
    id = json['id'];
    maxDiscountAmount = json['maxDiscountAmount'];
    minDiscountAmount = json['minDiscountAmount'];
    name = json['name'];
    orderValue = json['orderValue'];
    status = json['status'];
    type = json['type'];
    uniqueNumber = json['uniqueNumber'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['allowOtherDiscounts'] = this.allowOtherDiscounts;
    data['appliesTo'] = this.appliesTo;
    data['buyDiscountValue'] = this.buyDiscountValue;
    if (this.buyItems != null) {
      data['buyItems'] = this.buyItems.map((v) => v.toJson()).toList();
    }
    data['discountApplyType'] = this.discountApplyType;
    data['discountValue'] = this.discountValue;
    data['discountValueType'] = this.discountValueType;
    data['getDiscountValue'] = this.getDiscountValue;
    if (this.getItems != null) {
      data['getItems'] = this.getItems.map((v) => v.toJson()).toList();
    }
    data['id'] = this.id;
    data['maxDiscountAmount'] = this.maxDiscountAmount;
    data['minDiscountAmount'] = this.minDiscountAmount;
    data['name'] = this.name;
    data['orderValue'] = this.orderValue;
    data['status'] = this.status;
    data['type'] = this.type;
    data['uniqueNumber'] = this.uniqueNumber;
    data['value'] = this.value;
    return data;
  }
}

class BuyItems {
  List<Bitems> bitems;

  BuyItems({this.bitems});

  BuyItems.fromJson(Map<String, dynamic> json) {
    if (json['bitems'] != null) {
      bitems = [];
      json['bitems'].forEach((v) {
        bitems.add(new Bitems.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.bitems != null) {
      data['bitems'] = this.bitems.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Bitems {
  String id;
  String name;
  int quantity;
  int status;
  String type;
  String uniqueNumber;

  Bitems({this.id, this.name, this.quantity, this.status, this.type, this.uniqueNumber});

  Bitems.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    quantity = json['quantity'];
    status = json['status'];
    type = json['type'];
    uniqueNumber = json['uniqueNumber'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['quantity'] = this.quantity;
    data['status'] = this.status;
    data['type'] = this.type;
    data['uniqueNumber'] = this.uniqueNumber;
    return data;
  }
}

class GetItems {
  Null getItem;
  List<Gitems> gitems;

  GetItems({this.getItem, this.gitems});

  GetItems.fromJson(Map<String, dynamic> json) {
    getItem = json['getItem'];
    if (json['gitems'] != null) {
      gitems = [];
      json['gitems'].forEach((v) {
        gitems.add(new Gitems.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['getItem'] = this.getItem;
    if (this.gitems != null) {
      data['gitems'] = this.gitems.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Gitems {
  String id;
  Null items;
  String name;
  int quantity;
  int status;
  String type;
  String uniqueNumber;

  Gitems({this.id, this.items, this.name, this.quantity, this.status, this.type, this.uniqueNumber});

  Gitems.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    items = json['items'];
    name = json['name'];
    quantity = json['quantity'];
    status = json['status'];
    type = json['type'];
    uniqueNumber = json['uniqueNumber'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['items'] = this.items;
    data['name'] = this.name;
    data['quantity'] = this.quantity;
    data['status'] = this.status;
    data['type'] = this.type;
    data['uniqueNumber'] = this.uniqueNumber;
    return data;
  }
}

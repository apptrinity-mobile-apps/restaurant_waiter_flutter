class DineOptionsResponse {
  List<DinningOptionsList> dinningOptionsList;
  int responseStatus;
  String result;

  DineOptionsResponse({this.dinningOptionsList, this.responseStatus, this.result});

  DineOptionsResponse.fromJson(Map<String, dynamic> json) {
    if (json['dinningOptionsList'] != null) {
      dinningOptionsList = <DinningOptionsList>[];
      json['dinningOptionsList'].forEach((v) {
        dinningOptionsList.add(new DinningOptionsList.fromJson(v));
      });
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.dinningOptionsList != null) {
      data['dinningOptionsList'] = this.dinningOptionsList.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class DinningOptionsList {
  String behavior;
  String id;
  String optionName;

  DinningOptionsList({this.behavior, this.id, this.optionName});

  DinningOptionsList.fromJson(Map<String, dynamic> json) {
    behavior = json['behavior'];
    id = json['id'];
    optionName = json['optionName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['behavior'] = this.behavior;
    data['id'] = this.id;
    data['optionName'] = this.optionName;
    return data;
  }
}

class waitergenerateotpresponse {
  String oTP;
  int responseStatus;
  String restaurantId;
  String result;

  waitergenerateotpresponse({this.oTP, this.responseStatus, this.restaurantId, this.result});

  waitergenerateotpresponse.fromJson(Map<String, dynamic> json) {
    oTP = json['OTP'];
    responseStatus = json['responseStatus'];
    restaurantId = json['restaurantId'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['OTP'] = this.oTP;
    data['responseStatus'] = this.responseStatus;
    data['restaurantId'] = this.restaurantId;
    data['result'] = this.result;
    return data;
  }
}

import 'getitemmodifiersresponse.dart';

class GetCartItems {
  List<MenuCartItem> menuItem;

  GetCartItems({this.menuItem});

  GetCartItems.fromJson(Map<String, dynamic> json) {
    if (json['menuItem'] != null) {
      menuItem = [];
      json['menuItem'].forEach((v) {
        menuItem.add(new MenuCartItem.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.menuItem != null) {
      data['menuItem'] = this.menuItem.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class MenuCartItem {
  String id;
  String menuId;
  String menuGroupId;
  String name;
  String basePrice;
  String item_count;
  String item_total;
  String item_text_desc;
  List<ModifierList> modifierlist;

//  List<SizeList> sizeList;
  // List<TimePriceList> timePriceList;

  MenuCartItem(
      this.id, this.menuId, this.menuGroupId, this.name, this.basePrice, this.item_count, this.item_total, this.item_text_desc, this.modifierlist);

  MenuCartItem.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    menuId = json['menuId'];
    menuGroupId = json['menuGroupId'];
    name = json['name'];
    basePrice = json['basePrice'] as String;
    item_count = json['item_count'] as String;
    item_total = json['item_total'] as String;
    item_text_desc = json['item_text_desc'] as String;
    if (json['modifierlist'] != null) {
      modifierlist = [];
      json['modifierlist'].forEach((v) {
        modifierlist.add(new ModifierList.fromJson(v));
      });
    } else {
      modifierlist = [];
      modifierlist = [];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['menuId'] = this.menuId;
    data['menuGroupId'] = this.menuGroupId;
    data['name'] = this.name;
    data['basePrice'] = this.basePrice;
    data['item_count'] = this.item_count;
    data['item_total'] = this.item_total;
    data['item_text_desc'] = this.item_text_desc;
    if (this.modifierlist != null) {
      data['modifierlist'] = this.modifierlist.map((v) => v.toJson()).toList();
    }

    return data;
  }
}

/*
class ModifierList {
  String id;
  String name;
  String price;

  ModifierList({this.id, this.name,this.price});

  ModifierList.fromJson(Map<String, dynamic> json) {
    id = json['id'] as String;
    name = json['name'] as String;
    price = json['price'] as String;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['price'] = this.price;
    return data;
  }
}
*/

/*class SizeList {
  double price;
  String sizeName;

  SizeList({this.price, this.sizeName});

  SizeList.fromJson(Map<String, dynamic> json) {
    price = json['price'] as double;
    sizeName = json['sizeName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['price'] = this.price;
    data['sizeName'] = this.sizeName;
    return data;
  }
}

class TimePriceList {
  List<String> days;
  double price;
  String timeFrom;
  String timeTo;

  TimePriceList({this.days, this.price, this.timeFrom, this.timeTo});

  TimePriceList.fromJson(Map<String, dynamic> json) {
    days = json['days'].cast<String>();
    price = json['price'] as double;
    timeFrom = json['timeFrom'];
    timeTo = json['timeTo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['days'] = this.days;
    data['price'] = this.price;
    data['timeFrom'] = this.timeFrom;
    data['timeTo'] = this.timeTo;
    return data;
  }

}*/

class paymentcarddetailsresponse {
  int responseStatus;
  String result;
  CardDetails cardDetails;

  paymentcarddetailsresponse({this.responseStatus, this.result, this.cardDetails});

  paymentcarddetailsresponse.fromJson(Map<String, dynamic> json) {
    responseStatus = json['responseStatus'];
    result = json['result'];
    cardDetails = json['cardDetails'] != null ? new CardDetails.fromJson(json['cardDetails']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    if (this.cardDetails != null) {
      data['cardDetails'] = this.cardDetails.toJson();
    }
    return data;
  }
}

class CardDetails {
  String cardHolderName;
  String cardLast4;
  String cardToken;
  String expiry;

  CardDetails({
    this.cardHolderName,
    this.cardLast4,
    this.cardToken,
    this.expiry,
  });

  CardDetails.fromJson(Map<String, dynamic> json) {
    cardHolderName = json['cardHolderName'];
    cardLast4 = json['cardLast4'];
    cardToken = json['cardToken'];
    expiry = json['expiry'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['cardHolderName'] = this.cardHolderName;
    data['cardLast4'] = this.cardLast4;
    data['cardToken'] = this.cardToken;
    data['expiry'] = this.expiry;
    return data;
  }
}

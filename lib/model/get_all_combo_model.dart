class get_all_combo_model {
  List<Discounts> discounts;
  int responseStatus;
  String result;

  get_all_combo_model({this.discounts, this.responseStatus, this.result});

  get_all_combo_model.fromJson(Map<String, dynamic> json) {
    if (json['discounts'] != null) {
      discounts = [];
      json['discounts'].forEach((v) {
        discounts.add(new Discounts.fromJson(v));
      });
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.discounts != null) {
      data['discounts'] = this.discounts.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class Discounts {
  bool allowOtherDiscounts;
  Null appliesTo;
  List<ComboList> comboList;
  double discountValue;
  Null discountValueType;
  String id;
  double maxDiscountAmount;
  double minDiscountAmount;
  String name;
  int orderValue;
  int status;
  String type;
  String uniqueNumber;
  Null value;

  Discounts(
      {this.allowOtherDiscounts,
      this.appliesTo,
      this.comboList,
      this.discountValue,
      this.discountValueType,
      this.id,
      this.maxDiscountAmount,
      this.minDiscountAmount,
      this.name,
      this.orderValue,
      this.status,
      this.type,
      this.uniqueNumber,
      this.value});

  Discounts.fromJson(Map<String, dynamic> json) {
    allowOtherDiscounts = json['allowOtherDiscounts'];
    appliesTo = json['appliesTo'];
    if (json['comboList'] != null) {
      comboList = [];
      json['comboList'].forEach((v) {
        comboList.add(new ComboList.fromJson(v));
      });
    }
    discountValue = json['discountValue'];
    discountValueType = json['discountValueType'];
    id = json['id'];
    maxDiscountAmount = json['maxDiscountAmount'];
    minDiscountAmount = json['minDiscountAmount'];
    name = json['name'];
    orderValue = json['orderValue'];
    status = json['status'];
    type = json['type'];
    uniqueNumber = json['uniqueNumber'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['allowOtherDiscounts'] = this.allowOtherDiscounts;
    data['appliesTo'] = this.appliesTo;
    if (this.comboList != null) {
      data['comboList'] = this.comboList.map((v) => v.toJson()).toList();
    }
    data['discountValue'] = this.discountValue;
    data['discountValueType'] = this.discountValueType;
    data['id'] = this.id;
    data['maxDiscountAmount'] = this.maxDiscountAmount;
    data['minDiscountAmount'] = this.minDiscountAmount;
    data['name'] = this.name;
    data['orderValue'] = this.orderValue;
    data['status'] = this.status;
    data['type'] = this.type;
    data['uniqueNumber'] = this.uniqueNumber;
    data['value'] = this.value;
    return data;
  }
}

class ComboList {
  List<Items> items;

  ComboList({this.items});

  ComboList.fromJson(Map<String, dynamic> json) {
    if (json['items'] != null) {
      items = [];
      json['items'].forEach((v) {
        items.add(new Items.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.items != null) {
      data['items'] = this.items.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Items {
  String comboId;
  String id;
  String name;
  int quantity;
  int status;
  String type;
  String uniqueNumber;

  Items({this.comboId, this.id, this.name, this.quantity, this.status, this.type, this.uniqueNumber});

  Items.fromJson(Map<String, dynamic> json) {
    comboId = json['comboId'];
    id = json['id'];
    name = json['name'];
    quantity = json['quantity'];
    status = json['status'];
    type = json['type'];
    uniqueNumber = json['uniqueNumber'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['comboId'] = this.comboId;
    data['id'] = this.id;
    data['name'] = this.name;
    data['quantity'] = this.quantity;
    data['status'] = this.status;
    data['type'] = this.type;
    data['uniqueNumber'] = this.uniqueNumber;
    return data;
  }
}

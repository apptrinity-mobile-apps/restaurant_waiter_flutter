import 'package:waiter/model/SendTaxRatesModel.dart';
import 'package:waiter/model/getmenuitems.dart';

class getallvoidreasonsresponse {
  int responseStatus;
  String result;
  List<VoidReasonsList> voidReasonsList;

  getallvoidreasonsresponse({this.responseStatus, this.result, this.voidReasonsList});

  getallvoidreasonsresponse.fromJson(Map<String, dynamic> json) {
    responseStatus = json['responseStatus'];
    result = json['result'];
    if (json['void_reasonsList'] != null) {
      voidReasonsList = [];
      json['void_reasonsList'].forEach((v) {
        voidReasonsList.add(new VoidReasonsList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    if (this.voidReasonsList != null) {
      data['void_reasonsList'] = this.voidReasonsList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class VoidReasonsList {
  String description;
  String id;
  bool isActive;
  String name;
  int orderValue;

  VoidReasonsList({this.description, this.id, this.isActive, this.name, this.orderValue});

  VoidReasonsList.fromJson(Map<String, dynamic> json) {
    description = json['description'];
    id = json['id'];
    isActive = json['isActive'];
    name = json['name'];
    orderValue = json['orderValue'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['description'] = this.description;
    data['id'] = this.id;
    data['isActive'] = this.isActive;
    data['name'] = this.name;
    data['orderValue'] = this.orderValue;
    return data;
  }
}

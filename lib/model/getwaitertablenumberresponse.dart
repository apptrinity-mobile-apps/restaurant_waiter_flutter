class getwaitertablenumberresponse {
  int responseStatus;
  String result;
  List<TableSetupDetails> tableSetupDetails;

  getwaitertablenumberresponse({this.responseStatus, this.result, this.tableSetupDetails});

  getwaitertablenumberresponse.fromJson(Map<String, dynamic> json) {
    responseStatus = json['responseStatus'];
    result = json['result'];
    if (json['tableSetupDetails'] != null) {
      tableSetupDetails = [];
      json['tableSetupDetails'].forEach((v) {
        tableSetupDetails.add(new TableSetupDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    if (this.tableSetupDetails != null) {
      data['tableSetupDetails'] =
          this.tableSetupDetails.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class TableSetupDetails {
  bool activeStatus;
  int id;
  String revenueCenterId;
  int tableNumber;

  TableSetupDetails(
      {this.activeStatus, this.id, this.revenueCenterId, this.tableNumber});

  TableSetupDetails.fromJson(Map<String, dynamic> json) {
    activeStatus = json['activeStatus'];
    id = json['id'];
    revenueCenterId = json['revenueCenterId'];
    tableNumber = json['tableNumber'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['activeStatus'] = this.activeStatus;
    data['id'] = this.id;
    data['revenueCenterId'] = this.revenueCenterId;
    data['tableNumber'] = this.tableNumber;
    return data;
  }
}



class splitwaiterorderresponse {
  String mainOrderId;
  int responseStatus;
  String result;

  splitwaiterorderresponse({this.mainOrderId, this.responseStatus, this.result});

  splitwaiterorderresponse.fromJson(Map<String, dynamic> json) {
    mainOrderId = json['mainOrderId'];
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['mainOrderId'] = this.mainOrderId;
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

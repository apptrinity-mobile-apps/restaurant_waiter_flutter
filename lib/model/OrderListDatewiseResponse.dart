import 'package:waiter/model/SendTaxRatesModel.dart';
import 'package:waiter/model/getmenuitems.dart';

class OrderListDatewiseResponse {
  List<OrdersData> ordersData;
  int responseStatus;
  String result;

  OrderListDatewiseResponse({this.ordersData, this.responseStatus, this.result});

  OrderListDatewiseResponse.fromJson(Map<String, dynamic> json) {
    if (json['ordersData'] != null) {
      ordersData = [];
      json['ordersData'].forEach((v) {
        ordersData.add(new OrdersData.fromJson(v));
      });
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.ordersData != null) {
      data['ordersData'] = this.ordersData.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class OrdersData {
  String date;
  String dateForSorting;
  List<OrdersList> ordersList;

  OrdersData({this.date, this.dateForSorting, this.ordersList});

  OrdersData.fromJson(Map<String, dynamic> json) {
    date = json['date'];
    dateForSorting = json['date_for_sorting'];
    if (json['ordersList'] != null) {
      ordersList = [];
      json['ordersList'].forEach((v) {
        ordersList.add(new OrdersList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['date'] = this.date;
    data['date_for_sorting'] = this.dateForSorting;
    if (this.ordersList != null) {
      data['ordersList'] = this.ordersList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class OrdersList {
  int checkNumber;
  String checkUniqueId;
  String id;
  int orderNumber;
  String orderUniqueId;
  int tableNumber;
  String totalAmount;

  OrdersList({this.checkNumber, this.checkUniqueId, this.id, this.orderNumber, this.orderUniqueId, this.tableNumber, this.totalAmount});

  OrdersList.fromJson(Map<String, dynamic> json) {
    checkNumber = json['checkNumber'];
    checkUniqueId = json['checkUniqueId'];
    id = json['id'];
    orderNumber = json['orderNumber'];
    orderUniqueId = json['orderUniqueId'];
    tableNumber = json['tableNumber'];
    totalAmount = json['totalAmount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['checkNumber'] = this.checkNumber;
    data['checkUniqueId'] = this.checkUniqueId;
    data['id'] = this.id;
    data['orderNumber'] = this.orderNumber;
    data['orderUniqueId'] = this.orderUniqueId;
    data['tableNumber'] = this.tableNumber;
    data['totalAmount'] = this.totalAmount;
    return data;
  }
}

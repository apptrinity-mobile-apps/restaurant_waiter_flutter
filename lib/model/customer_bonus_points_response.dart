class CustomerBonusPointsResponse {
  CreditsData creditsData;
  int responseStatus;
  String result;

  CustomerBonusPointsResponse(
      {this.creditsData, this.responseStatus, this.result});

  CustomerBonusPointsResponse.fromJson(Map<String, dynamic> json) {
    creditsData = json['creditsData'] != null
        ? new CreditsData.fromJson(json['creditsData'])
        : null;
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.creditsData != null) {
      data['creditsData'] = this.creditsData.toJson();
    }
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class CreditsData {
  double restarantAvailablePoints;
  double restaurantCreditPoints;
  double restaurantDebitPoints;
  double totalAvailablePoints;
  double totalCreditPoints;
  double totalDebitPoints;

  CreditsData(
      {this.restarantAvailablePoints,
        this.restaurantCreditPoints,
        this.restaurantDebitPoints,
        this.totalAvailablePoints,
        this.totalCreditPoints,
        this.totalDebitPoints});

  CreditsData.fromJson(Map<String, dynamic> json) {
    restarantAvailablePoints = json['restarant_available_points'];
    restaurantCreditPoints = json['restaurant_credit_points'];
    restaurantDebitPoints = json['restaurant_debit_points'];
    totalAvailablePoints = json['total_available_points'];
    totalCreditPoints = json['total_credit_points'];
    totalDebitPoints = json['total_debit_points'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['restarant_available_points'] = this.restarantAvailablePoints;
    data['restaurant_credit_points'] = this.restaurantCreditPoints;
    data['restaurant_debit_points'] = this.restaurantDebitPoints;
    data['total_available_points'] = this.totalAvailablePoints;
    data['total_credit_points'] = this.totalCreditPoints;
    data['total_debit_points'] = this.totalDebitPoints;
    return data;
  }
}

class BaseResponse {
  int responseStatus;
  String result;
  String deviceId;

  BaseResponse({this.responseStatus, this.result});

  BaseResponse.fromJson(Map<String, dynamic> json) {
    responseStatus = json['responseStatus'];
    result = json['result'];
    deviceId = json['deviceId'] == null ? "" : json['deviceId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    data['deviceId'] = this.deviceId;
    return data;
  }
}

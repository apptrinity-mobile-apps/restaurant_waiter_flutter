import 'package:waiter/model/SendTaxRatesModel.dart';
import 'package:waiter/model/getmenuitems.dart';

class GetCustomerCreditsByPhone {
  CustomerData customerData;
  int responseStatus;
  String result;

  GetCustomerCreditsByPhone({this.customerData, this.responseStatus, this.result});

  GetCustomerCreditsByPhone.fromJson(Map<String, dynamic> json) {
    customerData = json['customerData'] != null ? new CustomerData.fromJson(json['customerData']) : null;
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.customerData != null) {
      data['customerData'] = this.customerData.toJson();
    }
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class CustomerData {
  String createdBy;
  String createdOn;
  String credits;
  String email;
  String firstName;
  String id;
  String lastName;
  String phoneNumber;
  int preCardNo;
  String restaurantId;

  CustomerData(
      {this.createdBy,
      this.createdOn,
      this.credits,
      this.email,
      this.firstName,
      this.id,
      this.lastName,
      this.phoneNumber,
      this.preCardNo,
      this.restaurantId});

  CustomerData.fromJson(Map<String, dynamic> json) {
    createdBy = json['createdBy'];
    createdOn = json['createdOn'];
    credits = json['credits'];
    email = json['email'];
    firstName = json['firstName'];
    id = json['id'];
    lastName = json['lastName'];
    phoneNumber = json['phoneNumber'];
    preCardNo = json['preCardNo'];
    restaurantId = json['restaurantId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createdBy'] = this.createdBy;
    data['createdOn'] = this.createdOn;
    data['credits'] = this.credits;
    data['email'] = this.email;
    data['firstName'] = this.firstName;
    data['id'] = this.id;
    data['lastName'] = this.lastName;
    data['phoneNumber'] = this.phoneNumber;
    data['preCardNo'] = this.preCardNo;
    data['restaurantId'] = this.restaurantId;
    return data;
  }
}

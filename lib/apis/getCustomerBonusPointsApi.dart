import 'dart:convert';

import 'package:requests/requests.dart';
import 'package:waiter/model/customer_bonus_points_response.dart';
import 'package:waiter/utils/all_constans.dart';

class GetCustomerBonusPointsApiRepository {
  Future<CustomerBonusPointsResponse> fetchBonusPoints(String restaurantId, String customerId) async {
    var body = json.encode({'customerId': customerId, 'restaurantId': restaurantId});
    dynamic response = await Requests.post(base_url + "fetch_customer_bonus_points", body: body, headers: {'Content-type': 'application/json'});
    CustomerBonusPointsResponse result = CustomerBonusPointsResponse.fromJson(jsonDecode(response));
    return result;
  }
}

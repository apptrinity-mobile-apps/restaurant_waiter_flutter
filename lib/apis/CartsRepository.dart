import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:waiter/model/cartmodelitems.dart';
import 'package:waiter/model/cartmodelitemsapi.dart';

class CartsRepository {
  static String keySaveToCart = "save_to_cartlist";

  Future<List<MenuCartItemapi>> getcartslisting() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    // final key = 'save_to_cartlist';
    final value = prefs.getString(keySaveToCart) ?? "[]";
    if (value == "") {
      // print('read: $value');
      Iterable list = json.decode(value);
      return list;
    } else {
      // print('read: $value');
      Iterable list = json.decode(value);
      return list.map((model) => MenuCartItemapi.fromJson(model)).toList();
    }
  }

  saveCartList(cartlist) async {
    final prefs = await SharedPreferences.getInstance();
    // final key1 = 'save_to_cartlist';
    final cartlist_value = jsonEncode(cartlist);
    prefs.setString(keySaveToCart, cartlist_value);
    print('savedCART $cartlist_value');
  }

  /* static Future ClearCartSession() async {
    print("CLEARCART============");
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.remove('save_to_cartlist');
  }
*/
  static Future removesaved_in_cart() async {
    print("CLEARCART============");
    SharedPreferences preferences = await SharedPreferences.getInstance();
    // await preferences.remove('save_to_cartlist');
    await preferences.remove(keySaveToCart);
  }
}

import 'dart:convert';

import 'package:requests/requests.dart';
import 'package:waiter/model/getwaitertablenumberresponse.dart';
import 'package:waiter/utils/all_constans.dart';
class GetWaiterTableNumberApiRepository {

  Future<List<TableSetupDetails>> getWaiterTableNumber(String restaurantId,String serviceAreaId) async {
    var body = json.encode({'restaurantId':restaurantId,'serviceAreaId':serviceAreaId});
    print(base_url + "get_waiter_table_setup"+ body.toString());

    dynamic response = await Requests.post(base_url + "get_waiter_table_setup",
        body: body, headers: {'Content-type': 'application/json'});

    print(base_url + "WAITERTABLENUMBERSRESPONSE"+ response.toString());
    final res = json.decode(response);
    Iterable list = res['tableSetupDetails'];
    return list.map((model) => TableSetupDetails.fromJson(model)).toList();
  }
}


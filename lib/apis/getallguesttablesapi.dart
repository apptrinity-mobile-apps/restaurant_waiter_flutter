import 'dart:convert';
import 'dart:developer' as dev;

import 'package:requests/requests.dart';
import 'package:waiter/model/getallguesttableresponse.dart';
import 'package:waiter/utils/all_constans.dart';
class GetAllGuestTableApiRepository {

  Future<List<GuestTableList>> checkGetAllGuestTables(String restaurantId, String serviceAreaId,String employeeId) async {
    var body = json.encode({'restaurantId':restaurantId,'serviceAreaId': serviceAreaId,'employeeId':employeeId});
    print(base_url + "get_dashboard_tables_data"+ body.toString());

    dynamic response = await Requests.post(base_url + "get_dashboard_tables_data",
        body: body, headers: {'Content-type': 'application/json'});
dev.log(response);
    print(base_url + "GETTABLESRESPONSE"+ response.toString());
    final res = json.decode(response);
    Iterable list = res['ordersInfo'];
    return list.map((model) => GuestTableList.fromJson(model)).toList();

  }
}




/*
class LoginRepository {
  Future checklogin(String email, String password) async {
    Map<String, dynamic> body = {'email': email, 'password': password};
    print("reviworder----email " + email + " password " + password);

    final response = await http.post(base_url + "userLogin",
        body: body,
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/x-www-form-urlencoded"
        },
        encoding: Encoding.getByName("utf-8"));

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      print("login page " + response.body);
      Map<String, dynamic> user = json.decode(response.body);
      if (user['response'] == true) {
        //print("login page "+user['userId']);

        return user['userId'];
      } else {
        return "failed";
        // print("Failed ");
      }
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }
}*/

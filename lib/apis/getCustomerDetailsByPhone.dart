import 'dart:convert';

import 'package:requests/requests.dart';
import 'package:waiter/model/GetCustomerCreditsByPhone.dart';
import 'package:waiter/model/getallguesttableresponse.dart';
import 'package:waiter/utils/all_constans.dart';
class GetCreditsApiRepository {
  Future<GetCustomerCreditsByPhone> getCreditsByPhone(String phonenumber, String userid,String restaurantId) async {
    var body = json.encode({
      'phonenumber': phonenumber,
      'userId': userid,
      'restaurantId': restaurantId
    });
    print(base_url + "Payment_REQUEST" + body.toString());

    dynamic response = await Requests.post(base_url + "get_customer_details_by_phone_no",
        body: body, headers: {'Content-type': 'application/json'});


    GetCustomerCreditsByPhone receiptresult_data =
    GetCustomerCreditsByPhone.fromJson(jsonDecode(response));
    return receiptresult_data;
  }
}

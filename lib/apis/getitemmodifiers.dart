import 'dart:convert';

import 'package:requests/requests.dart';
import 'package:waiter/model/getitemmodifiersresponse.dart';
import 'package:waiter/model/getmenugroups.dart';
import 'package:waiter/utils/all_constans.dart';

class GetItemModifiers {
  Future<List<ModifiersGroupsList>> getmenu(
      String res_id,
      String menu_item_id,
      ) async {

    var body = json.encode({
      'restaurantId': res_id,
      'menu_item_id': menu_item_id
    });
    print(base_url+"===="+body);
    dynamic response = await Requests.post(base_url + "get_modifer_and_modier_groups_for_menu_item",
        body: body, headers: {'Content-type': 'application/json'});

    final res = json.decode(response);
    Iterable list = res['modifiers_groups_list'];
    return list.map((model) => ModifiersGroupsList.fromJson(model)).toList();

  }
}
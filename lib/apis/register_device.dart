import 'dart:convert';

import 'package:requests/requests.dart';
import 'package:waiter/model/base_response.dart';
import 'package:waiter/model/dine_options_response.dart';
import 'package:waiter/model/service_areas_response.dart';
import 'package:waiter/utils/all_constans.dart';

class RegisterDeviceRepository {
  Future<BaseResponse> checkDeviceNameApi(String restaurantId, String deviceName) async {
    var body = json.encode({'restaurantId': restaurantId, "deviceName": deviceName});
    print(base_url + "check_existing_device_name_REQUEST" + body.toString());
    dynamic response = await Requests.post(base_url + "check_existing_device_name", body: body, headers: {'Content-type': 'application/json'});
    BaseResponse result = BaseResponse.fromJson(jsonDecode(response));
    return result;
  }

  Future<ServiceAreasResponse> getWaiterServiceAreasApi(String restaurantCode) async {
    var body = json.encode({'restaurantCode': restaurantCode});
    print(base_url + "get_waiter_service_areas_REQUEST" + body.toString());
    dynamic response = await Requests.post(base_url + "get_waiter_service_areas", body: body, headers: {'Content-type': 'application/json'});
    ServiceAreasResponse result = ServiceAreasResponse.fromJson(jsonDecode(response));
    return result;
  }

  Future<BaseResponse> addDeviceApi(String restaurantId, String deviceName, String deviceToken, String serviceAreaId) async {
    var body = json.encode({'restaurantId': restaurantId, "deviceName": deviceName, "deviceToken": deviceToken, "serviceAreaId": serviceAreaId});
    print(base_url + "add_active_waiter_device_REQUEST" + body.toString());
    dynamic response = await Requests.post(base_url + "add_active_waiter_device", body: body, headers: {'Content-type': 'application/json'});
    BaseResponse result = BaseResponse.fromJson(jsonDecode(response));
    print(result.result + result.deviceId);
    return result;
  }

  Future<DineOptionsResponse> getAllDinningOptionsApi(String userId, String restaurantId) async {
    var body = json.encode({'userId': userId, 'restaurantId': restaurantId});
    print(base_url + "get_all_dinning_options_REQUEST" + body.toString());
    dynamic response = await Requests.post(base_url + "get_all_dinning_options", body: body, headers: {'Content-type': 'application/json'});
    DineOptionsResponse result = DineOptionsResponse.fromJson(jsonDecode(response));
    return result;
  }
}

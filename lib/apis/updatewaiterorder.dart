import 'dart:convert';
import 'dart:developer' as dev;

import 'package:requests/requests.dart';
import 'package:waiter/model/addwaiterorderresponse.dart';
import 'package:waiter/model/loginresponse.dart';
import 'package:waiter/model/splitwaiterorderresponse.dart';
import 'package:waiter/utils/all_constans.dart';
import 'package:http/http.dart' as http;
class UpdateWaiterOrderRepository {

  Future<addwaiterorderresponse> updatewaiterorder(id,orderItems,String tipAmount,double discountAmount,double subTotal,double taxAmount,double totalAmount,String restaurantId,String status,String userId) async {
    /*var body = json.encode({'id':id,'orderItems':orderItems,'tipAmount':tipAmount,'discountAmount':discountAmount,'subTotal':subTotal,'taxAmount':taxAmount,'totalAmount':totalAmount,'restaurantId':restaurantId,'status':status});*/
    var orderdetails = json.encode({'userId':userId,'restaurantId':restaurantId,'orderDetails':{'id':id,'orderItems':orderItems,'tipAmount':tipAmount,'discountAmount':discountAmount,'subTotal':subTotal,'taxAmount':taxAmount,'totalAmount':totalAmount,'status':status}});

    dev.log(base_url + "UPDATEWAITERORDERREQUEST"+ orderdetails.toString());
    dynamic response = await Requests.post(base_url + "update_waiter_order",
        body: orderdetails, headers: {'Content-type': 'application/json'});

    addwaiterorderresponse loginresult_data =
    addwaiterorderresponse.fromJson(jsonDecode(response));
    print(loginresult_data.result);
    return loginresult_data;

  }


}




import 'dart:convert';

import 'package:requests/requests.dart';
import 'package:waiter/model/loginresponse.dart';
import 'package:waiter/model/receiptresponse.dart';
import 'package:waiter/utils/all_constans.dart';
import 'package:http/http.dart' as http;
class ReceiptApiRepository {
  Future<receiptresponse> getreceipt(String receiptnumber, String userid,String restaurantId) async {
    var body = json.encode({
      'receiptNumber': receiptnumber,
      'userId': userid,
      'restaurantId': restaurantId
    });
    print(base_url + "get_receipt_order_info   " + body.toString());

    dynamic response = await Requests.post(base_url + "get_receipt_order_info",
        body: body, headers: {'Content-type': 'application/json'});


    receiptresponse receiptresult_data =
    receiptresponse.fromJson(jsonDecode(response));
    return receiptresult_data;
  }
}

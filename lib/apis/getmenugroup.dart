import 'dart:convert';

import 'package:requests/requests.dart';
import 'package:waiter/model/getmenugroups.dart';
import 'package:waiter/utils/all_constans.dart';

class GetMenuGroups {
  Future<List<MenugroupsList>> getmenu(
      String userid,
      String res_id,
      String menu_id,
      ) async {

    var body = json.encode({
      'userId': userid,
      'restaurantId': res_id,
      'menuId': menu_id
    });
    print(body);
    dynamic response = await Requests.post(base_url + "get_all_menu_groups",
        body: body, headers: {'Content-type': 'application/json'});

    final res = json.decode(response);
    Iterable list = res['menugroupsList'];
    return list.map((model) => MenugroupsList.fromJson(model)).toList();

  }
}
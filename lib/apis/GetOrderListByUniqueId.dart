import 'dart:convert';

import 'package:requests/requests.dart';
import 'package:waiter/model/OrderListDatewiseResponse.dart';
import 'package:waiter/model/getallguesttableresponse.dart';
import 'package:waiter/model/gettableinformationresponse.dart';
import 'package:waiter/utils/all_constans.dart';

class GetOrderListByUniqueIdApiRepository {
  Future<OrderListDatewiseResponse> orderListUniqieId(String employeeId, String restaurantId, String orderUniqueId) async {
    var body = json.encode({'employeeId': employeeId, 'restaurantId': restaurantId, 'orderUniqueId': orderUniqueId});
    print(base_url + "orders_list_by_unique_id" + "GETORDERLISTUNIQUEID" + employeeId + "----" + restaurantId);
    dynamic response = await Requests.post(base_url + "orders_list_by_unique_id", body: body, headers: {'Content-type': 'application/json'});
    print(base_url + "" + response.toString());
    OrderListDatewiseResponse orderlistdatewise_data = OrderListDatewiseResponse.fromJson(jsonDecode(response));
    return orderlistdatewise_data;
  }
}

/*
class LoginRepository {
  Future checklogin(String email, String password) async {
    Map<String, dynamic> body = {'email': email, 'password': password};
    print("reviworder----email " + email + " password " + password);

    final response = await http.post(base_url + "userLogin",
        body: body,
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/x-www-form-urlencoded"
        },
        encoding: Encoding.getByName("utf-8"));

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      print("login page " + response.body);
      Map<String, dynamic> user = json.decode(response.body);
      if (user['response'] == true) {
        //print("login page "+user['userId']);

        return user['userId'];
      } else {
        return "failed";
        // print("Failed ");
      }
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }
}*/

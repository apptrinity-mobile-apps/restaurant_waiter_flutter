import 'package:async/async.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:toast/toast.dart';
import 'package:waiter/apis/GiftCardBalanceEnquiryApi.dart';
import 'package:waiter/apis/PayOrderApi.dart';
import 'package:waiter/apis/getCustomerBonusPointsApi.dart';
import 'package:waiter/payment_success.dart';
import 'package:waiter/session/userRepository.dart';
import 'package:waiter/utils/all_constans.dart';
import 'package:waiter/webview_screen.dart';

import 'apis/getCustomerDetailsByPhone.dart';
import 'model/getmenugroups.dart';

double usedBonusCredits = 0.00;
bool useLoyaltyPoints = false;

class Payment2 extends StatefulWidget {
  final table_number;
  final noofguest;
  final grand_total;
  final order_id;
  final selected_unique_id;
  final customer_id;

  const Payment2(this.table_number, this.noofguest, this.order_id, this.grand_total, this.selected_unique_id, this.customer_id, {Key key})
      : super(key: key);

  @override
  _Payment2State createState() => _Payment2State();
}

class _Payment2State extends State<Payment2> {
  final List<String> numbers = [
    "1",
    "5",
    "6",
    "7",
    "8",
    "2",
    "10",
    "15",
    "20",
  ];
  List<MenugroupsList> _get_menu_groups;
  bool _loading = true;
  int cart_count = 0;
  double total_qty = 0.0;

  CancelableOperation cancelableOperation;
  final GlobalKey<State> _keyLoader = GlobalKey<State>();

  int selected_pos = -1;
  String userid = "", no_of_guests = "", table_number = "";
  String employee_id = "";
  String res_id = "";
  String menu_id = "";
  double selected_tip = 0.0;
  TextEditingController customtip_Controller = TextEditingController();
  TextEditingController amount_tendered_Controller = TextEditingController();
  bool customtip_visible = false, hasLoyaltyPoints = false;
  double _payment_grandroundup = 0.0, availableLoyaltyPoints = 0.00;

  /*  final AnimationController _controller = AnimationController(
    duration: const Duration(seconds: 3),
    vsync: this,
  )..repeat();
   final Animation<double> _animation = CurvedAnimation(
    parent: _controller,
    curve: Curves.fastOutSlowIn,
  );*/

  @override
  void initState() {
    super.initState();
    print("${widget.noofguest}====${widget.table_number}");
    setState(() {
      if (widget.table_number == null || widget.table_number == "") {
        table_number = "0";
      } else {
        table_number = widget.table_number;
      }
      if (widget.noofguest == null || widget.noofguest == "") {
        no_of_guests = "0";
      } else {
        no_of_guests = widget.noofguest;
      }
      usedBonusCredits = 0.00;
      useLoyaltyPoints = false;
    });
    UserRepository().getuserdetails().then((userdetails) {
      setState(() {
        userid = userdetails[0];

        UserRepository().getRestaurantId().then((restaurantdetails) {
          setState(() {
            res_id = restaurantdetails;
            print("restaurant_id" + res_id);
            _loading = false;
            UserRepository().getuserdetails().then((userdetails) {
              print("userdata" + userdetails.length.toString());
              setState(() {
                employee_id = userdetails[0];
                print(
                    "SELECTEDORDERID" + widget.order_id + "-------" + widget.grand_total.toString() + "--------" + res_id + "-------" + employee_id);
                if (selected_tip > 0) {
                  _payment_grandroundup = selected_tip + double.parse(widget.grand_total);
                  amount_tendered_Controller.text = String.fromCharCodes(Runes('\u0024')) + _payment_grandroundup.toString();
                } else {
                  _payment_grandroundup = double.parse(widget.grand_total);
                  amount_tendered_Controller.text = String.fromCharCodes(Runes('\u0024')) + _payment_grandroundup.toString();
                }
              });
            });
            setState(() {
              if (widget.customer_id != "") {
                hasLoyaltyPoints = true;
                print("bonus-- $res_id ${widget.customer_id}");
                GetCustomerBonusPointsApiRepository().fetchBonusPoints(res_id, widget.customer_id).then((value) {
                  setState(() {
                    if (value.responseStatus == 1) {
                      // availableLoyaltyPoints = value.creditsData.restarantAvailablePoints;
                      availableLoyaltyPoints = 10.00;
                    } else {
                      // availableLoyaltyPoints = 0.00;
                      availableLoyaltyPoints = 10.00;
                    }
                    usedBonusCredits = availableLoyaltyPoints;
                    print("bonus-- $availableLoyaltyPoints");
                  });
                });
              } else {
                hasLoyaltyPoints = false;
              }
            });
          });
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => _onBackPressed(),
        child: Scaffold(
            appBar: AppBar(
              toolbarHeight: 100,
              automaticallyImplyLeading: false,
              elevation: 0.0,
              backgroundColor: Colors.white,
              centerTitle: false,
              title: Text("Bill Payment", style: TextStyle(color: login_passcode_text, fontSize: 24.0, fontWeight: FontWeight.w500)),
              leading: Builder(
                builder: (BuildContext context) {
                  return IconButton(
                    padding: EdgeInsets.only(left: 10.0),
                    icon: Image.asset("images/back_arrow.png", width: 22, height: 22),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  );
                },
              ),
            ),
            body: SingleChildScrollView(
                child: _loading
                    ? Center(
                        child: SpinKitFadingCircle(color: Colors.lightBlueAccent),
                      )
                    : Container(
                        color: Colors.white,
                        child: Container(
                          margin: EdgeInsets.only(left: 15, right: 15),
                          color: dashboard_bg,
                          child: Column(
                            children: [
                              Container(
                                color: dashboard_bg,
                                margin: EdgeInsets.only(left: 10, right: 10),
                                padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 12.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        Padding(
                                          padding: EdgeInsets.only(left: 15, bottom: 0, right: 0),
                                          child: RichText(
                                              text: TextSpan(children: [
                                            TextSpan(
                                                text: "Order No\n",
                                                style:
                                                    TextStyle(color: Colors.black, fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w300)),
                                            TextSpan(
                                                text: "#" + widget.selected_unique_id,
                                                style:
                                                    TextStyle(fontSize: 18, color: Colors.black, fontFamily: 'Poppins', fontWeight: FontWeight.w700))
                                          ])),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        Padding(
                                          padding: EdgeInsets.only(left: 0, right: 10, bottom: 0),
                                          child: RichText(
                                              text: TextSpan(children: [
                                            TextSpan(
                                                text: "Amount\n",
                                                style:
                                                    TextStyle(color: Colors.black, fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w300)),
                                            TextSpan(
                                                text: String.fromCharCodes(Runes('\u0024')) + widget.grand_total,
                                                style:
                                                    TextStyle(fontSize: 18, color: Colors.black, fontFamily: 'Poppins', fontWeight: FontWeight.w700))
                                          ])),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 20, right: 20),
                                padding: EdgeInsets.symmetric(horizontal: 0.0, vertical: 16.0),
                                // height: /*MediaQuery.of(context).size.height * 0.30*/ 300,
                                color: Colors.white,
                                child: Column(
                                  children: [
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        Padding(
                                          padding: EdgeInsets.only(top: 5.0, left: 15, bottom: 0),
                                          child: Text("Add a Tip Amount",
                                              style: TextStyle(
                                                  color: login_passcode_text, fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w600)),
                                        ),
                                      ],
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(left: 15, right: 15),
                                      padding: EdgeInsets.symmetric(horizontal: 0.0, vertical: 5.0),
                                      height: MediaQuery.of(context).size.height * 0.12,
                                      color: Colors.white,
                                      child: ListView.builder(
                                          scrollDirection: Axis.horizontal,
                                          itemCount: numbers.length,
                                          itemBuilder: (context, index) {
                                            print("DEFAULTSELECTEDTIP" + selected_tip.toString());
                                            return Container(
                                              margin: EdgeInsets.all(5),
                                              width: MediaQuery.of(context).size.width * 0.2,
                                              child: InkWell(
                                                  onTap: () {
                                                    setState(() {
                                                      if (selected_pos == index) {
                                                        selected_tip = 0.0;
                                                        selected_pos = -1;
                                                      } else if (selected_pos != index || selected_pos == -1) {
                                                        selected_pos = index;
                                                        selected_tip = double.parse(numbers[index]);
                                                      }
                                                      print(numbers[index]);
                                                      if (selected_tip > 0) {
                                                        _payment_grandroundup = selected_tip + double.parse(widget.grand_total);
                                                        amount_tendered_Controller.text =
                                                            String.fromCharCodes(Runes('\u0024')) + _payment_grandroundup.toStringAsFixed(2);
                                                      } else {
                                                        _payment_grandroundup = double.parse(widget.grand_total);
                                                        amount_tendered_Controller.text =
                                                            String.fromCharCodes(Runes('\u0024')) + _payment_grandroundup.toStringAsFixed(2);
                                                      }
                                                      print("SELECTEDTIP" + selected_tip.toString() + "-------" + _payment_grandroundup.toString());
                                                    });
                                                  },
                                                  child: Card(
                                                    elevation: 2,
                                                    color: selected_pos == index ? Colors.lightBlueAccent : Colors.white,
                                                    shape: RoundedRectangleBorder(
                                                      borderRadius: BorderRadius.circular(100),
                                                    ),
                                                    child: Container(
                                                      decoration: BoxDecoration(
                                                        border: Border.all(
                                                          color: login_passcode_bg2,
                                                        ),
                                                        borderRadius: BorderRadius.all(Radius.circular(100)),
                                                      ),
                                                      child: Column(
                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                        children: <Widget>[
                                                          Text(
                                                            String.fromCharCodes(Runes('\u0024')) + numbers[index],
                                                            style: TextStyle(
                                                                color: selected_pos == index ? Colors.white : add_food_item_bg, fontSize: 20),
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  )),
                                            );
                                          }),
                                    ),
                                    Container(
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          Padding(
                                            padding: EdgeInsets.only(top: 0.0, left: 15, bottom: 0),
                                            child: GestureDetector(
                                              onTap: () {
                                                setState(() {
                                                  customtip_visible = !customtip_visible;
                                                });
                                              },
                                              child: Text("Custom Tip ?",
                                                  style: TextStyle(
                                                      color: login_passcode_bg1, fontSize: 14, fontFamily: 'Poppins', fontWeight: FontWeight.w600)),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    /*SizeTransition(
                  sizeFactor: _animation,
                  axis: Axis.horizontal,
                  axisAlignment: -1,
                  child: const Center(
                    child: FlutterLogo(size: 200.0),
                  ),
                ),*/
                                    Container(
                                      margin: EdgeInsets.only(left: 15, top: 0, right: 15, bottom: 7),
                                      child: Visibility(
                                        visible: customtip_visible,
                                        maintainSize: true,
                                        maintainAnimation: true,
                                        maintainState: true,
                                        child: TextFormField(
                                          validator: (val) {
                                            if (val.isEmpty) return 'Enter your amount';
                                            return null;
                                          },
                                          onChanged: (amount) {
                                            setState(() {
                                              selected_pos = -1;
                                              if (amount != "") {
                                                selected_tip = double.parse(amount);
                                                _payment_grandroundup = selected_tip + double.parse(widget.grand_total);
                                                amount_tendered_Controller.text =
                                                    String.fromCharCodes(Runes('\u0024')) + _payment_grandroundup.toStringAsFixed(2);
                                              } else {
                                                selected_tip = 0.0;
                                                _payment_grandroundup = selected_tip + double.parse(widget.grand_total);
                                                amount_tendered_Controller.text =
                                                    String.fromCharCodes(Runes('\u0024')) + _payment_grandroundup.toStringAsFixed(2);
                                              }
                                            });
                                          },
                                          controller: customtip_Controller,
                                          obscureText: false,
                                          keyboardType: TextInputType.number,
                                          decoration: InputDecoration(
                                              enabledBorder: OutlineInputBorder(
                                                borderRadius: BorderRadius.all(Radius.circular(0.0)),
                                                borderSide: BorderSide(color: edit_text_border_color),
                                              ),
                                              filled: true,
                                              fillColor: Colors.white,
                                              hintText: "Enter your Tip amount",
                                              hintStyle: TextStyle(fontSize: 16.0, color: text_hint_color),
                                              contentPadding: EdgeInsets.only(
                                                bottom: 30 / 2,
                                                left: 50 / 2, // HERE THE IMPORTANT PART
                                                // HERE THE IMPORTANT PART
                                              ),
                                              border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0))),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(left: 15, top: 0, right: 15, bottom: 1),
                                      child: TextFormField(
                                        validator: (val) {
                                          if (val.isEmpty) return 'Enter Tendered amount';
                                          return null;
                                        },
                                        controller: amount_tendered_Controller,
                                        obscureText: false,
                                        keyboardType: TextInputType.number,
                                        decoration: InputDecoration(
                                            enabledBorder: OutlineInputBorder(
                                              borderRadius: BorderRadius.all(Radius.circular(0.0)),
                                              borderSide: BorderSide(color: edit_text_border_color),
                                            ),
                                            filled: true,
                                            fillColor: Colors.white,
                                            hintText: "Enter Tendered amount",
                                            hintStyle: TextStyle(fontSize: 16.0, color: text_hint_color),
                                            contentPadding: EdgeInsets.only(
                                              bottom: 30 / 2,
                                              left: 50 / 2, // HERE THE IMPORTANT PART
                                              // HERE THE IMPORTANT PART
                                            ),
                                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0))),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 0.0),
                                alignment: Alignment.center,
                                child: Padding(
                                  padding: EdgeInsets.only(left: 10, right: 10),
                                  child: Column(
                                    children: [
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          RichText(
                                              text: TextSpan(
                                            children: [
                                              TextSpan(
                                                  text: "Total Amount : ",
                                                  style: TextStyle(
                                                      fontSize: 18, color: login_passcode_text, fontFamily: 'Poppins', fontWeight: FontWeight.w500))
                                            ],
                                          )),
                                          RichText(
                                              text: TextSpan(children: [
                                            TextSpan(
                                                text: String.fromCharCodes(Runes('\u0024')) + _payment_grandroundup.toStringAsFixed(2),
                                                style: TextStyle(
                                                    fontSize: 24, color: login_passcode_text, fontFamily: 'Poppins', fontWeight: FontWeight.w700))
                                          ]))
                                        ],
                                      ),
                                      hasLoyaltyPoints
                                          ? Material(
                                              child: CheckboxListTile(
                                                title: Text(
                                                    'Redeem loyalty points (${String.fromCharCodes(Runes('\u0024'))}${availableLoyaltyPoints.toStringAsFixed(2)})',
                                                    style: TextStyle(
                                                        fontSize: 18,
                                                        color: login_passcode_text,
                                                        fontFamily: 'Poppins',
                                                        fontWeight: FontWeight.w500)),
                                                value: useLoyaltyPoints,
                                                onChanged: (newValue) {
                                                  setState(() {
                                                    useLoyaltyPoints = newValue;
                                                    String liveprice = amount_tendered_Controller.text.toString();
                                                    String newStr = liveprice.replaceAll(String.fromCharCodes(Runes('\u0024')), "");
                                                    if (useLoyaltyPoints) {
                                                      if (availableLoyaltyPoints > double.parse(newStr)) {
                                                        showDialog(
                                                            context: context,
                                                            barrierDismissible: false,
                                                            builder: (context) {
                                                              return WillPopScope(
                                                                onWillPop: () async => false,
                                                                child: AlertDialog(
                                                                  title: Text('Loyalty Points',
                                                                      style: TextStyle(
                                                                          fontSize: 18,
                                                                          color: login_passcode_text,
                                                                          fontFamily: 'Poppins',
                                                                          fontWeight: FontWeight.w700)),
                                                                  content: Text('$newStr loyalty points will be redeemed.!',
                                                                      style: TextStyle(
                                                                          fontSize: 15, fontFamily: 'Poppins', fontWeight: FontWeight.w500)),
                                                                  actions: <Widget>[
                                                                    TextButton(
                                                                      child: Text('Cancel'),
                                                                      onPressed: () {
                                                                        setState(() {
                                                                          useLoyaltyPoints = false;
                                                                        });
                                                                        Navigator.of(context).pop();
                                                                      },
                                                                    ),
                                                                    TextButton(
                                                                      child: Text('Proceed'),
                                                                      onPressed: () {
                                                                        PayOrderRepository()
                                                                            .payOrder(
                                                                                widget.order_id,
                                                                                "",
                                                                            selected_tip + widget.grand_total,
                                                                                double.parse(newStr),
                                                                                0,
                                                                                0,
                                                                                employee_id,
                                                                                "res_id",
                                                                                int.parse(table_number),
                                                                                int.parse(no_of_guests),
                                                                                selected_tip,
                                                                                "",
                                                                                0,
                                                                                "",
                                                                                "",
                                                                                "",
                                                                                useLoyaltyPoints,
                                                                                double.parse(newStr))
                                                                            .then((value) {
                                                                          print("PAYMENTSTSTAUS" + value.toString());
                                                                          if (value.responseStatus == 0) {
                                                                            print("PAYMENTSTATUSCASH" + value.result);
                                                                            Toast.show(value.result, context,
                                                                                duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                                                                          } else if (value.responseStatus == 1) {
                                                                            setState(() {
                                                                              print("PAYMENTSTATUS" +
                                                                                  value.result +
                                                                                  "----" +
                                                                                  widget.order_id +
                                                                                  "---" +
                                                                                  double.parse(widget.grand_total).toString());
                                                                              Navigator.pushAndRemoveUntil(
                                                                                  context,
                                                                                  MaterialPageRoute(
                                                                                      builder: (context) => PaymentSuccess(widget.order_id,
                                                                                          /*double.parse(newStr)*/(selected_tip + widget.grand_total), value.receiptNumber)),
                                                                                  (route) => false);
                                                                            });
                                                                          } else {
                                                                            Toast.show(value.result, context,
                                                                                duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                                                                          }
                                                                        });
                                                                      },
                                                                    ),
                                                                  ],
                                                                ),
                                                              );
                                                            });
                                                      } else {
                                                        setState(() {
                                                          _payment_grandroundup =
                                                              selected_tip + double.parse(widget.grand_total) - availableLoyaltyPoints;
                                                          amount_tendered_Controller.text =
                                                              String.fromCharCodes(Runes('\u0024')) + _payment_grandroundup.toStringAsFixed(2);
                                                        });
                                                      }
                                                    } else {
                                                      setState(() {
                                                        _payment_grandroundup = selected_tip + double.parse(widget.grand_total);
                                                        amount_tendered_Controller.text =
                                                            String.fromCharCodes(Runes('\u0024')) + _payment_grandroundup.toStringAsFixed(2);
                                                      });
                                                    }
                                                  });
                                                },
                                                controlAffinity: ListTileControlAffinity.leading,
                                                contentPadding: EdgeInsets.all(0),
                                              ),
                                            )
                                          : SizedBox()
                                    ],
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.all(5),
                                width: MediaQuery.of(context).size.width * 0.9,
                                child: Card(
                                    color: Colors.white,
                                    child: Container(
                                      height: 56,
                                      child: InkWell(
                                        onTap: () {
                                          setState(() {
                                            String liveprice = amount_tendered_Controller.text.toString();
                                            String newStr = liveprice.replaceAll(String.fromCharCodes(Runes('\u0024')), "");
                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                builder: (context) => WebViewExample(
                                                    widget.order_id,
                                                    /*widget.grand_total*/
                                                    _payment_grandroundup.toStringAsFixed(2),
                                                    double.parse(newStr),
                                                    employee_id,
                                                    res_id,
                                                    table_number,
                                                    no_of_guests,
                                                    selected_tip.toString(),
                                                    useLoyaltyPoints,
                                                    usedBonusCredits),
                                              ),
                                            );
                                          });
                                        },
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Row(
                                              children: [
                                                Padding(
                                                    padding: EdgeInsets.only(left: 15),
                                                    child: Image.asset(
                                                      'images/swipe_card.png',
                                                      height: 30,
                                                      width: 30,
                                                    )),
                                                Padding(
                                                    padding: EdgeInsets.only(left: 15),
                                                    child: Text(
                                                      "Credit Card",
                                                      style: TextStyle(
                                                          color: login_passcode_text,
                                                          fontSize: 20,
                                                          fontFamily: 'Poppins',
                                                          fontWeight: FontWeight.w500),
                                                      textAlign: TextAlign.start,
                                                    )),
                                              ],
                                            ),
                                            Padding(
                                                padding: EdgeInsets.only(right: 15),
                                                child: Image.asset(
                                                  'images/menu_arrow.png',
                                                  height: 20,
                                                  width: 20,
                                                )),
                                          ],
                                        ),
                                      ),
                                    )),
                              ),
                              Container(
                                margin: EdgeInsets.all(5),
                                width: MediaQuery.of(context).size.width * 0.9,
                                child: Card(
                                    color: Colors.white,
                                    child: Container(
                                      height: 56,
                                      child: InkWell(
                                        onTap: () {
                                          setState(() {
                                            String liveprice = amount_tendered_Controller.text.toString();
                                            String newStr = liveprice.replaceAll(String.fromCharCodes(Runes('\u0024')), "");
                                            print("REMOVEDOLLAR====" + newStr + "====" + selected_tip.toString());
                                            double tendered = 0.00;
                                            if (useLoyaltyPoints) {
                                              tendered = double.parse(newStr) - availableLoyaltyPoints;
                                            } else {
                                              tendered = double.parse(newStr);
                                            }
                                            double change = 0.00;
                                            if ((double.parse(newStr) - _payment_grandroundup) < 0) {
                                              change = 0.00;
                                            } else {
                                              change = (double.parse(newStr) - _payment_grandroundup);
                                            }
                                            PayOrderRepository()
                                                .payOrder(
                                                    widget.order_id,
                                                    "",
                                                    /*_payment_grandroundup*/
                                                    selected_tip + double.parse(widget.grand_total),
                                                    double.parse(newStr) /*tendered*/,
                                                    change,
                                                    0,
                                                    employee_id,
                                                    res_id,
                                                    int.parse(table_number),
                                                    int.parse(no_of_guests),
                                                    selected_tip,
                                                    "",
                                                    0,
                                                    "",
                                                    "",
                                                    "",
                                                    useLoyaltyPoints,
                                                    usedBonusCredits)
                                                .then((value) {
                                              print("PAYMENTSTSTAUS" + value.toString());
                                              if (value.responseStatus == 0) {
                                                print("PAYMENTSTATUSCASH" + value.result);
                                                Toast.show(value.result, context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                                                //Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
                                              } else if (value.responseStatus == 1) {
                                                setState(() {
                                                  print("PAYMENTSTATUS" +
                                                      value.result +
                                                      "----" +
                                                      widget.order_id +
                                                      "---" +
                                                      double.parse(widget.grand_total).toString());
                                                  Navigator.pushAndRemoveUntil(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              PaymentSuccess(widget.order_id, /*double.parse(newStr)*/(selected_tip + double.parse(widget.grand_total)), value.receiptNumber)),
                                                      (route) => false);
                                                });
                                              } else {
                                                Toast.show(value.result, context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                                              }
                                            });
                                          });
                                        },
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Row(
                                              children: [
                                                Padding(
                                                    padding: EdgeInsets.only(left: 15),
                                                    child: Image.asset(
                                                      'images/cash.png',
                                                      height: 30,
                                                      width: 30,
                                                    )),
                                                Padding(
                                                    padding: EdgeInsets.only(left: 15),
                                                    child: Text(
                                                      "Cash",
                                                      style: TextStyle(
                                                          color: login_passcode_text,
                                                          fontSize: 20,
                                                          fontFamily: 'Poppins',
                                                          fontWeight: FontWeight.w500),
                                                      textAlign: TextAlign.start,
                                                    )),
                                              ],
                                            ),
                                            Padding(
                                                padding: EdgeInsets.only(right: 15),
                                                child: Image.asset(
                                                  'images/menu_arrow.png',
                                                  height: 20,
                                                  width: 20,
                                                )),
                                          ],
                                        ),
                                      ),
                                    )),
                              ),
                              Container(
                                margin: EdgeInsets.all(5),
                                width: MediaQuery.of(context).size.width * 0.9,
                                child: Card(
                                    color: Colors.white,
                                    child: Container(
                                      height: 56,
                                      child: InkWell(
                                        onTap: () {
                                          setState(() {
                                            String liveprice = amount_tendered_Controller.text.toString();
                                            String newStr = liveprice.replaceAll(String.fromCharCodes(Runes('\u0024')), "");
                                            print("REMOVEDOLLAR====" + newStr);
                                            showDialog(
                                                context: context,
                                                builder: (context) {
                                                  return _MyDialog(
                                                      table_number: table_number,
                                                      no_of_guest: no_of_guests,
                                                      restaurant_id: res_id,
                                                      employee_id: employee_id,
                                                      order_id: widget.order_id,
                                                      grand_total: /*widget.grand_total*/ _payment_grandroundup.toStringAsFixed(2),
                                                      tipAmount: selected_tip,
                                                      amount_tendered: double.parse(newStr));
                                                });
                                          });
                                        },
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Row(
                                              children: [
                                                Padding(
                                                    padding: EdgeInsets.only(left: 15),
                                                    child: Image.asset(
                                                      'images/gift_card.png',
                                                      height: 30,
                                                      width: 30,
                                                    )),
                                                Padding(
                                                    padding: EdgeInsets.only(left: 15),
                                                    child: Text(
                                                      "Gift Card",
                                                      style: TextStyle(
                                                          color: login_passcode_text,
                                                          fontSize: 20,
                                                          fontFamily: 'Poppins',
                                                          fontWeight: FontWeight.w500),
                                                      textAlign: TextAlign.start,
                                                    )),
                                              ],
                                            ),
                                            Padding(
                                                padding: EdgeInsets.only(right: 15),
                                                child: Image.asset(
                                                  'images/menu_arrow.png',
                                                  height: 20,
                                                  width: 20,
                                                )),
                                          ],
                                        ),
                                      ),
                                    )),
                              ),
                              Container(
                                margin: EdgeInsets.all(5),
                                width: MediaQuery.of(context).size.width * 0.9,
                                child: Card(
                                    color: Colors.white,
                                    child: Container(
                                      height: 56,
                                      child: InkWell(
                                        onTap: () {
                                          setState(() {
                                            String liveprice = amount_tendered_Controller.text.toString();
                                            String newStr = liveprice.replaceAll(String.fromCharCodes(Runes('\u0024')), "");
                                            print("REMOVEDOLLAR====" + newStr);

                                            showDialog(
                                                context: context,
                                                builder: (context) {
                                                  return _MyDialogCredits(
                                                      table_number: table_number,
                                                      no_of_guest: no_of_guests,
                                                      restaurant_id: res_id,
                                                      employee_id: employee_id,
                                                      order_id: widget.order_id,
                                                      grand_total: /*widget.grand_total*/ _payment_grandroundup.toStringAsFixed(2),
                                                      tipAmount: selected_tip,
                                                      amount_tendered: double.parse(newStr));
                                                });
                                          });
                                        },
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Row(
                                              children: [
                                                Padding(
                                                    padding: EdgeInsets.only(left: 15),
                                                    child: Image.asset(
                                                      'images/gift_card.png',
                                                      height: 30,
                                                      width: 30,
                                                    )),
                                                Padding(
                                                    padding: EdgeInsets.only(left: 15),
                                                    child: Text(
                                                      "Credits",
                                                      style: TextStyle(
                                                          color: login_passcode_text,
                                                          fontSize: 20,
                                                          fontFamily: 'Poppins',
                                                          fontWeight: FontWeight.w500),
                                                      textAlign: TextAlign.start,
                                                    )),
                                              ],
                                            ),
                                            Padding(
                                                padding: EdgeInsets.only(right: 15),
                                                child: Image.asset(
                                                  'images/menu_arrow.png',
                                                  height: 20,
                                                  width: 20,
                                                )),
                                          ],
                                        ),
                                      ),
                                    )),
                              ),
                              Container(
                                margin: EdgeInsets.fromLTRB(0, 0, 0, 20),
                                width: MediaQuery.of(context).size.width * 0.9,
                                child: Card(
                                    color: Colors.white,
                                    child: Container(
                                      height: 56,
                                      child: InkWell(
                                        onTap: () {
                                          setState(() {
                                            Toast.show("Coming Soon......", context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                                          });
                                        },
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Row(
                                              children: [
                                                Padding(
                                                    padding: EdgeInsets.only(left: 15),
                                                    child: Image.asset(
                                                      'images/send_payment_req.png',
                                                      height: 30,
                                                      width: 30,
                                                    )),
                                                Padding(
                                                    padding: EdgeInsets.only(left: 15),
                                                    child: Text(
                                                      "Send Payment Request",
                                                      style: TextStyle(
                                                          color: login_passcode_text,
                                                          fontSize: 18,
                                                          fontFamily: 'Poppins',
                                                          fontWeight: FontWeight.w500),
                                                      textAlign: TextAlign.start,
                                                    )),
                                              ],
                                            ),
                                            Padding(
                                                padding: EdgeInsets.only(right: 15),
                                                child: Image.asset(
                                                  'images/menu_arrow.png',
                                                  height: 20,
                                                  width: 20,
                                                )),
                                          ],
                                        ),
                                      ),
                                    )),
                              )
                            ],
                          ),
                        )))));
  }

  Future<bool> _onBackPressed() async {
    Navigator.of(context).pop(true);
    return true;
  }
}

class CancelDialogs {
  static Future<void> showLoadingDialog(BuildContext context, GlobalKey key) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return WillPopScope(
              onWillPop: () async => false,
              child: SimpleDialog(key: key, backgroundColor: Colors.black54, children: <Widget>[
                Center(
                  child: Column(children: [
                    CircularProgressIndicator(),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Cancelling....",
                      style: TextStyle(color: Colors.lightBlueAccent),
                    )
                  ]),
                )
              ]));
        });
  }
}

_paymentType(String pay_type) async {}

class _MyDialog extends StatefulWidget {
  _MyDialog({
    this.table_number,
    this.no_of_guest,
    this.restaurant_id,
    this.employee_id,
    this.order_id,
    this.grand_total,
    this.tipAmount,
    this.amount_tendered,
  });

  final String table_number;
  final String no_of_guest;
  final String restaurant_id;
  final String employee_id;
  final String order_id;
  final String grand_total;
  final double tipAmount;
  final double amount_tendered;

  @override
  _MyDialogState createState() => _MyDialogState();
}

class _MyDialogState extends State<_MyDialog> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _giftcardnumber_controller = TextEditingController();
  final GlobalKey<State> _keyLoader = GlobalKey<State>();
  bool _loading = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
        backgroundColor: Colors.white,
        insetPadding: EdgeInsets.all(20),
        child: SingleChildScrollView(
            child: Container(
                //height: MediaQuery.of(context).size.height,
                child: Center(
          child: Column(children: [
            Form(
              key: _formKey, //padding: EdgeInsets.fromLTRB(8, 8, 8, 0),
              child: Column(
                children: <Widget>[
                  Container(
                    alignment: Alignment.centerRight,
                    child: TextButton(
                      onPressed: () {
                        FocusScope.of(context).unfocus();
                        Navigator.of(context).pop();
                      },
                      child: Padding(
                          padding: EdgeInsets.all(10),
                          child: Image.asset(
                            'images/cancel.png',
                            height: 25,
                            width: 25,
                          )),
                    ),
                  ),
                  Padding(
                      padding: EdgeInsets.symmetric(horizontal: 25, vertical: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(
                            height: 5.0,
                          ),
                          Container(
                              margin: EdgeInsets.only(bottom: 8),
                              child: Text(
                                'Gift Card',
                                textAlign: TextAlign.left,
                                style: TextStyle(color: login_username_label_color, fontSize: 16, fontWeight: FontWeight.bold),
                              )),
                          TextFormField(
                            validator: (val) {
                              if (val.isEmpty) return 'Enter Gift Card Number';
                              return null;
                            },
                            controller: _giftcardnumber_controller,
                            obscureText: false,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                filled: true,
                                fillColor: Colors.white,
                                hintText: "Enter Gift Card Number",
                                contentPadding: EdgeInsets.only(
                                  bottom: 30 / 2,
                                  left: 50 / 2, // HERE THE IMPORTANT PART
                                  // HERE THE IMPORTANT PART
                                ),
                                border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0))),
                          ),
                        ],
                      )),
                  Container(
                      margin: EdgeInsets.fromLTRB(25, 10, 25, 20),
                      height: 50,
                      width: double.infinity,
                      //alignment: Alignment.center,
                      decoration: BoxDecoration(color: login_passcode_bg1, borderRadius: BorderRadius.circular(0)),
                      child: TextButton(
                          // minWidth: double.infinity,
                          // height: double.infinity,
                          child:
                              Text("SUBMIT", style: TextStyle(fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w800, color: Colors.white)),
                          onPressed: () {
                            if (_formKey.currentState.validate()) {
                              _formKey.currentState.save();
                              GiftCardBalanceEnquiryRepository()
                                  .giftCardBalanceEnquiry(int.parse(_giftcardnumber_controller.text.toString()), widget.restaurant_id)
                                  .then((value) {
                                print("GIFTCARDRESP" + value.responseStatus.toString());
                                if (value.responseStatus == 0) {
                                  print("GIFTCARDSTATUS" + value.result);
                                  Toast.show(value.result, context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                                  // Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
                                } else if (value.responseStatus == 2) {
                                  Toast.show(value.result, context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                                  // Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
                                } else if (value.responseStatus == 1) {
                                  print("GIFTCARDAMOUNT" +
                                      value.result +
                                      "----" +
                                      value.giftCard.amount +
                                      "------" +
                                      widget.amount_tendered.toString());
                                  if (double.parse(value.giftCard.amount) >= widget.amount_tendered) {
                                    PayOrderRepository()
                                        .payOrder(
                                            widget.order_id,
                                            "",
                                            widget.tipAmount + double.parse(widget.grand_total),
                                            widget.amount_tendered,
                                            0,
                                            2,
                                            widget.employee_id,
                                            widget.restaurant_id,
                                            int.parse(widget.table_number),
                                            int.parse(widget.no_of_guest),
                                            widget.tipAmount,
                                            _giftcardnumber_controller.text.toString(),
                                            0,
                                            "",
                                            "",
                                            "",
                                            useLoyaltyPoints,
                                            usedBonusCredits)
                                        .then((value) {
                                      print("PAYMENTSTSTAUS" + value.toString());
                                      if (value.responseStatus == 0) {
                                        print("PAYMENTSTATUS" + value.result);
                                        Toast.show(value.result, context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                                        // Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
                                      } else if (value.responseStatus == 1) {
                                        setState(() {
                                          print("PAYMENTSTATUS" +
                                              value.result +
                                              "----" +
                                              widget.order_id +
                                              "---" +
                                              double.parse(widget.grand_total).toString());
                                          Navigator.pushAndRemoveUntil(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) => PaymentSuccess(widget.order_id, /*widget.amount_tendered*/(widget.tipAmount + double.parse(widget.grand_total)), value.receiptNumber)),
                                              (route) => false);
                                        });
                                      } else {
                                        Toast.show(value.result, context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                                      }
                                    });
                                  } else {
                                    Toast.show("Insufficeint Amount", context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                                  }
                                } else {
                                  Toast.show(value.result, context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                                }
                              });
                            }
                          }))
                ],
              ),
            )
          ]),
        ))));
  }
}

class _MyDialogCredits extends StatefulWidget {
  _MyDialogCredits({
    this.table_number,
    this.no_of_guest,
    this.restaurant_id,
    this.employee_id,
    this.order_id,
    this.grand_total,
    this.tipAmount,
    this.amount_tendered,
  });

  final String table_number;
  final String no_of_guest;
  final String restaurant_id;
  final String employee_id;
  final String order_id;
  final String grand_total;
  final double tipAmount;
  final double amount_tendered;

  @override
  _MyDialogCreditsState createState() => _MyDialogCreditsState();
}

class _MyDialogCreditsState extends State<_MyDialogCredits> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _phonenumber_controller = TextEditingController();
  final GlobalKey<State> _keyLoader = GlobalKey<State>();
  bool _loading = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
        backgroundColor: Colors.white,
        insetPadding: EdgeInsets.all(20),
        child: SingleChildScrollView(
            child: Container(
                //height: MediaQuery.of(context).size.height,
                child: Center(
          child: Column(children: [
            Form(
              key: _formKey, //padding: EdgeInsets.fromLTRB(8, 8, 8, 0),
              child: Column(
                children: <Widget>[
                  Container(
                    alignment: Alignment.centerRight,
                    child: TextButton(
                      onPressed: () {
                        FocusScope.of(context).unfocus();
                        Navigator.of(context).pop();
                      },
                      child: Padding(
                          padding: EdgeInsets.all(10),
                          child: Image.asset(
                            'images/cancel.png',
                            height: 25,
                            width: 25,
                          )),
                    ),
                  ),
                  Padding(
                      padding: EdgeInsets.symmetric(horizontal: 25, vertical: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(
                            height: 5.0,
                          ),
                          Container(
                              margin: EdgeInsets.only(bottom: 8),
                              child: Text(
                                'Phone Number',
                                textAlign: TextAlign.left,
                                style: TextStyle(color: login_username_label_color, fontSize: 16, fontWeight: FontWeight.bold),
                              )),
                          TextFormField(
                            validator: (val) {
                              if (val.isEmpty) return 'Enter Phone Number';
                              return null;
                            },
                            controller: _phonenumber_controller,
                            obscureText: false,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                filled: true,
                                fillColor: Colors.white,
                                hintText: "Enter Phone Number",
                                contentPadding: EdgeInsets.only(
                                  bottom: 30 / 2,
                                  left: 50 / 2, // HERE THE IMPORTANT PART
                                  // HERE THE IMPORTANT PART
                                ),
                                border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0))),
                          ),
                        ],
                      )),
                  Container(
                      margin: EdgeInsets.fromLTRB(25, 10, 25, 20),
                      height: 50,
                      width: double.infinity,
                      // alignment: Alignment.center,
                      decoration: BoxDecoration(color: login_passcode_bg1, borderRadius: BorderRadius.circular(0)),
                      child: TextButton(
                          // minWidth: double.infinity,
                          // height: double.infinity,
                          child:
                              Text("SUBMIT", style: TextStyle(fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w800, color: Colors.white)),
                          onPressed: () {
                            if (_formKey.currentState.validate()) {
                              _formKey.currentState.save();
                              GetCreditsApiRepository()
                                  .getCreditsByPhone(_phonenumber_controller.text.toString(), widget.employee_id, widget.restaurant_id)
                                  .then((value) {
                                print("CREDITSRESP" + value.responseStatus.toString());
                                if (value.responseStatus == 0) {
                                  print("CREDITSRESPTATUS" + value.result);
                                  Toast.show(value.result, context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                                  // Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
                                } else if (value.responseStatus == 1) {
                                  print("CREDITSAMOUNT" +
                                      value.result +
                                      "----" +
                                      value.customerData.credits +
                                      "------" +
                                      widget.amount_tendered.toString());
                                  if (double.parse(value.customerData.credits) >= widget.amount_tendered) {
                                    PayOrderRepository()
                                        .payOrder(
                                            widget.order_id,
                                            "",
                                            widget.tipAmount + double.parse(widget.grand_total),
                                            widget.amount_tendered,
                                            0,
                                            3,
                                            widget.employee_id,
                                            widget.restaurant_id,
                                            int.parse(widget.table_number),
                                            int.parse(widget.no_of_guest),
                                            widget.tipAmount,
                                            "",
                                            value.customerData.preCardNo,
                                            "",
                                            "",
                                            "",
                                            useLoyaltyPoints,
                                            usedBonusCredits)
                                        .then((value) {
                                      print("PAYMENTSTSTAUS" + value.toString());
                                      if (value.responseStatus == 0) {
                                        print("PAYMENTSTATUS" + value.result);
                                        Toast.show(value.result, context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                                        // Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
                                      } else if (value.responseStatus == 1) {
                                        setState(() {
                                          print("PAYMENTSTATUS" +
                                              value.result +
                                              "----" +
                                              widget.order_id +
                                              "---" +
                                              double.parse(widget.grand_total).toString());
                                          Navigator.pushAndRemoveUntil(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) => PaymentSuccess(widget.order_id, /*widget.amount_tendered*/widget.tipAmount + double.parse(widget.grand_total), value.receiptNumber)),
                                              (route) => false);
                                        });
                                      } else {
                                        Toast.show(value.result, context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                                      }
                                    });
                                  } else {
                                    Toast.show("Insufficeint Credits", context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                                  }
                                } else {
                                  Toast.show(value.result, context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                                }
                              });
                            }
                          }))
                ],
              ),
            )
          ]),
        ))));
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:waiter/apis/register_device.dart';
import 'package:waiter/service_areas_screen.dart';
import 'package:waiter/session/userRepository.dart';
import 'package:waiter/utils/DialogClass.dart';
import 'package:waiter/utils/all_constans.dart';
import 'package:waiter/utils/sizeconfig.dart';
import 'package:async/async.dart';
import 'package:waiter/utils/snackbar.dart';

class DeviceNameScreen extends StatefulWidget {
  const DeviceNameScreen({Key key}) : super(key: key);

  @override
  _DeviceNameScreenState createState() => _DeviceNameScreenState();
}

class _DeviceNameScreenState extends State<DeviceNameScreen> {
  CancelableOperation cancelableOperation;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  TextEditingController deviceNameController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  double logoMarginHeight;
  bool isUniqueName = false;
  String restaurantId = "";

  @override
  void initState() {
    UserRepository().getRestaurantId().then((value) {
      setState(() {
        restaurantId = value;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    print("Height" + SizeConfig.screenHeight.toString());
    if (SizeConfig.screenHeight >= 580 && SizeConfig.screenHeight < 800) {
      logoMarginHeight = 120;
    } else if (SizeConfig.screenHeight >= 800) {
      logoMarginHeight = 220;
    } else {
      logoMarginHeight = 100;
    }
    return Scaffold(
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(image: DecorationImage(image: ExactAssetImage("images/splash_bg.png"), fit: BoxFit.cover)),
        child: SingleChildScrollView(
            child: Column(
          children: [
            Container(
                margin: EdgeInsets.fromLTRB(40, logoMarginHeight, 0, 0),
                alignment: Alignment.centerLeft,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image(
                      image: AssetImage('images/logo_dashboard.png'),
                      width: 150,
                      height: 110,
                    ),
                    Padding(
                        padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                        child: Text(
                          'The Waiter App',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: SizeConfig.blockSizeHorizontal * 4,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w400,
                          ),
                        ))
                  ],
                )),
            Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                      margin: EdgeInsets.fromLTRB(0, 50, 0, 0),
                      alignment: Alignment.center,
                      child: Column(
                        children: [
                          Container(
                              margin: EdgeInsets.fromLTRB(35, 0, 35, 0),
                              alignment: Alignment.topLeft,
                              child: Text(
                                enterDeviceName,
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: SizeConfig.blockSizeHorizontal * 3.2,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w400,
                                ),
                              )),
                          Container(
                            margin: EdgeInsets.fromLTRB(35, 5, 35, 0),
                            child: TextFormField(
                              validator: (val) {
                                if (val.isEmpty) return emptyDeviceName;
                                return null;
                              },
                              controller: deviceNameController,
                              obscureText: false,
                              keyboardType: TextInputType.text,
                              textCapitalization: TextCapitalization.words,
                              decoration: InputDecoration(
                                  filled: true,
                                  fillColor: Colors.white,
                                  hintText: enterDeviceName,
                                  hintStyle: TextStyle(color: Colors.grey),
                                  contentPadding: EdgeInsets.only(
                                    bottom: 30 / 2,
                                    left: 50 / 2,
                                  ),
                                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0))),
                            ),
                          )
                        ],
                      )),
                  Container(
                      margin: EdgeInsets.fromLTRB(35, 20, 35, 0),
                      width: double.infinity,
                      decoration: BoxDecoration(color: login_passcode_bg1, borderRadius: BorderRadius.circular(0)),
                      child: TextButton(
                          child: Text(isUniqueName ? next.toUpperCase() : submit.toUpperCase(),
                              style: TextStyle(fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w800, color: Colors.white)),
                          onPressed: () {
                            FocusScope.of(context).unfocus();
                            if (!isUniqueName) {
                              if (_formKey.currentState.validate()) {
                                _formKey.currentState.save();
                                SignDialogs.showLoadingDialog(context, "Checking", _keyLoader);
                                cancelableOperation?.cancel();
                                CancelableOperation.fromFuture(Future.delayed(Duration(seconds: 1), () {
                                  RegisterDeviceRepository().checkDeviceNameApi(restaurantId, deviceNameController.text.trim()).then((value) {
                                    Navigator.of(context).pop();
                                    if (value.responseStatus == 1) {
                                      showSnackBar(context, nameAvailable);
                                      setState(() {
                                        isUniqueName = true;
                                      });
                                      UserRepository.saveDeviceName(deviceNameController.text.trim());
                                    } else {
                                      showSnackBar(context, nameAlreadyUsed);
                                    }
                                  });
                                }));
                              }
                            } else {
                              Navigator.of(context).push(MaterialPageRoute(builder: (context) => ServiceAreasScreen()));
                            }
                          }))
                ],
              ),
            )
          ],
        )),
      ),
    );
  }
}

class CancelDialogs {
  static Future<void> showLoadingDialog(BuildContext context, GlobalKey key) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new WillPopScope(
              onWillPop: () async => false,
              child: SimpleDialog(key: key, backgroundColor: Colors.black54, children: <Widget>[
                Center(
                  child: Column(children: [
                    CircularProgressIndicator(),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Cancelling....",
                      style: TextStyle(color: Colors.lightBlueAccent),
                    )
                  ]),
                )
              ]));
        });
  }
}

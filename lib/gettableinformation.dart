import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:page_indicator/page_indicator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'package:waiter/apis/voiditemApiRepository.dart';
import 'package:waiter/model/SendTaxRatesModel.dart';
import 'package:waiter/model/cartmodelitemsapi.dart';
import 'package:waiter/model/getitemmodifierssubmitresponse.dart';
import 'package:waiter/payment2.dart';
import 'package:waiter/receipt.dart';
import 'package:waiter/session/userRepository.dart';
import 'package:waiter/splitorderscreen.dart';
import 'package:waiter/utils/Globals.dart';
import 'package:waiter/utils/all_constans.dart';
import 'package:waiter/utils/sizeconfig.dart';

import 'addfood_screen.dart';
import 'apis/CartsRepository.dart';
import 'apis/getAllVoidReasonsApi.dart';
import 'apis/getTableInformationApi.dart';
import 'apis/updatewaiterorder.dart';
import 'dashboard.dart';
import 'model/getallvoidreasonsresponse.dart';
import 'model/getallvoidreasonssendtoApi.dart';
import 'model/gettableinformationresponse.dart';

class GetTableInformation extends StatefulWidget {
  final String orderId;
  final String tableNumber;
  final bool isFromCart;

  const GetTableInformation(this.orderId, this.tableNumber, this.isFromCart, {Key key}) : super(key: key);

  @override
  _GetTableInformationState createState() => _GetTableInformationState();
}

class _GetTableInformationState extends State<GetTableInformation> {
  PageController controller;

  GlobalKey<PageContainerState> key = GlobalKey();
  int selected_pos = -1;

  List<TableInfo> __cart_items_list = [];
  List<ItemsList> __dialog_items_list = [];
  int split_length = 0;
  int selected_page_position = 0;
  double _itemsubTotal = 0.0;
  var _itemGrandTotalPrice = "";
  var cart_count_value = 1;
  int cart_count = 0;
  var table_number = "";
  var order_id = "";
  var no_of_guest = "";
  var restaurant_id = "";
  var employee_id = "";
  double screenheight = 0.0;
  double cardview_height = 0.0;
  double toolbar_height = 0.0;
  bool _loading = true;
  String selected_order_id = "", customerId = "";
  String selected_unique_id = "";
  String grand_total = "";
  String tax_total = "";
  String check_payment_status = "";
  String receipt_no = "";
  String check_payment_type = "";
  String payment_status = "";
  String kitchen_status = "";
  String subtotal = "";
  String item_selected_status = "0";
  String returnVal = "";
  TextEditingController tablenumber_Controller = TextEditingController();
  TextEditingController no_of_guest_Controller = TextEditingController();

  double _items_cart_grandroundup = 0.0;
  double _items_cart_subtotal = 0.0;
  String order_id_updated = "";
  String dine_in_option_id = "";
  String service_area_id = "";
  String revenue_centre_id = "";

  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  final List<Map> data = List.generate(4, (index) => {'id': index, 'name': 'Item $index', 'isSelected': false});

  @override
  void initState() {
    super.initState();
    // print(widget.main_order_id.toString());
    controller = PageController();
    TextEditingController grand_total_text_controller = new TextEditingController();

    print("Height" + SizeConfig.screenHeight.toString());
    if (SizeConfig.screenHeight >= 580 && SizeConfig.screenHeight < 800) {
      toolbar_height = 56;
      screenheight = 650.0;
      cardview_height = 345.0;
      print("MANIBABUHEIGHT" + screenheight.toString());
    }
    if (SizeConfig.screenHeight >= 800) {
      toolbar_height = 66;
      screenheight = 680.0;
      cardview_height = 450.0;
    }
    UserRepository().getuserdetails().then((userdetails) {
      print("userdata----" + userdetails.length.toString());
      setState(() {
        employee_id = userdetails[0];
        //user_id = userdetails[1];
        UserRepository().getRestaurantId().then((restaurantdetails) {
          setState(() {
            restaurant_id = restaurantdetails;
            print("TABLEINFORMATION----" +
                "TABLENUMBER===" +
                widget.orderId.toString() +
                " EMPLOYEEID===" +
                employee_id +
                " RESTAURANTID====" +
                restaurant_id);
            Future.delayed(Duration(seconds: 2), () async {
              setState(() {
                UserRepository().getDineInOptionId().then((dineinoptiondetails) {
                  setState(() {
                    dine_in_option_id = dineinoptiondetails[0];
                    UserRepository().getServiceAreaandCentreId().then((serviceareadetails) {
                      setState(() {
                        service_area_id = serviceareadetails[0];
                        revenue_centre_id = serviceareadetails[1];
                        UserRepository().getuserdetails().then((userdetails) {
                          print("userdata" + userdetails.length.toString());
                          setState(() {
                            employee_id = userdetails[0];
                            UserRepository().getRestaurantId().then((restaurantdetails) {
                              setState(() {
                                restaurant_id = restaurantdetails;
                                UserRepository().getOrderId().then((orderdetailsorderid) {
                                  setState(() {
                                    order_id_updated = orderdetailsorderid[0];

                                    GetTableInformationApiRepository()
                                        .checktableinformation(widget.orderId, employee_id, restaurant_id)
                                        .then((value) {
                                      print("TABLEINFORMATION RESPSTATUS  " + value.responseStatus.toString());
                                      debugPrint(base_url + "" + value.result.toString());
                                      if (value.responseStatus == 1) {
                                        setState(() {
                                          _loading = false;
                                          //split_length = value.tableInfo.length;
                                          table_number = value.orderInfo[0].tableNumber.toString();
                                          order_id = value.orderInfo[0].orderId.toString();
                                          no_of_guest = value.orderInfo[0].noOfGuest.toString();
                                          // tablenumber_Controller.text = table_number;

                                          kitchen_status = value.orderInfo[0].status.toString();
                                          subtotal = value.orderInfo[0].subTotal.toString();

                                          print(table_number +
                                              "======" +
                                              no_of_guest +
                                              "====" +
                                              tablenumber_Controller.text +
                                              "_____" +
                                              no_of_guest_Controller.text.toString());
                                          if (no_of_guest == null || no_of_guest == "null") {
                                            no_of_guest = "0";
                                            no_of_guest_Controller.text = no_of_guest;
                                          } else {
                                            no_of_guest_Controller.text = no_of_guest;
                                          }

                                          for (int t = 0; t < value.orderInfo.length; t++) {
                                            __cart_items_list.add(value.orderInfo[t]);
                                            print("itemslength" + value.orderInfo[t].itemsList.length.toString() + "" + payment_status);
                                            __dialog_items_list = __cart_items_list[t].itemsList;

                                            if (value.orderInfo[t].itemsList.length > 0) {
                                              split_length = split_length + 1;
                                            }
                                            check_payment_status = __cart_items_list[t].paymentStatus.toString();
                                            receipt_no = __cart_items_list[t].receiptNumber.toString();
                                          }

                                          // check_payment_status = value.orderInfo[0].paymentStatus.toString();

                                          print("PAYMENTSTATUS====" + check_payment_status.toString());
                                        });
                                      } else if (value.responseStatus == 3) {
                                        setState(() {
                                          _loading = false;
                                          Toast.show("NoneType' object has no attribute 'id'", context,
                                              duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                                        });
                                      } else if (value.responseStatus == 0) {
                                        setState(() {
                                          _loading = false;
                                          Toast.show(value.result, context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                                        });
                                      }
                                    });
                                  });
                                });
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  }

  void refresh() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => _onBackPressed(),
        child: Scaffold(
          appBar: AppBar(
            toolbarHeight: toolbar_height,
            automaticallyImplyLeading: false,
            elevation: 0.0,
            backgroundColor: Colors.white,
            centerTitle: false,
            title: Text("Order", style: new TextStyle(color: login_passcode_text, fontSize: 18.0, fontWeight: FontWeight.w700)),
            leading: Builder(
              builder: (BuildContext context) {
                return IconButton(
                  padding: EdgeInsets.only(left: 10.0),
                  icon: Image.asset("images/back_arrow.png", width: 22, height: 22),
                  onPressed: () {
                    _onBackPressed();
                  },
                );
              },
            ),
            actions: [
              check_payment_status == "0"
                  ? Container(
                      margin: const EdgeInsets.fromLTRB(15, 15, 24, 15),
                      height: 35,
                      padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(color: dashboard_bg, borderRadius: BorderRadius.circular(0)),
                      child: InkWell(
                          child: Text("Split",
                              style: TextStyle(
                                  fontSize: SizeConfig.safeBlockHorizontal * 4,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w400,
                                  color: text_split)),
                          onTap: () {
                            print("SPLITORDERID" + order_id);
                            Navigator.push(context, _createRoute(order_id));
                          }))
                  : SizedBox()
            ],
          ),
          bottomSheet: _loading
              ? SizedBox()
              : Container(
                  //height: 60,
                  child: kitchen_status == "0"
                      ? Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Expanded(
                                child: Container(
                                    // alignment: Alignment.center,
                                    decoration: BoxDecoration(color: dashboard_quick_order, borderRadius: BorderRadius.circular(0)),
                                    child: TextButton(
                                        child: Text("ADD ORDER",
                                            style: TextStyle(
                                                fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w800,
                                                color: Colors.white)),
                                        onPressed: () {
                                          print("selected_order_id--" + selected_order_id);
                                          UserRepository.save_OrderId(selected_order_id);
                                          UserRepository.save_TablenumbernGuests(table_number, no_of_guest);

                                          addCartItems(__cart_items_list[selected_page_position]);

                                          Navigator.push(context, MaterialPageRoute(builder: (context) => AddFood(false, table_number, no_of_guest)));
                                        }))),
                            Expanded(
                                child: Container(
                                    // alignment: Alignment.center,
                                    decoration: BoxDecoration(color: login_passcode_bg1, borderRadius: BorderRadius.circular(0)),
                                    child: TextButton(
                                        child: Text("SEND TO KITCHEN",
                                            style: TextStyle(
                                                fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w800,
                                                color: Colors.white)),
                                        onPressed: () {
                                          print(kitchen_status);

                                          _loading = true;
                                          print(__cart_items_list);
                                          _items_cart_grandroundup = double.parse(_items_cart_subtotal.toStringAsFixed(2));

                                          print("ORDERIDUPDATED" +
                                              subtotal +
                                              "===" +
                                              tax_total +
                                              "====" +
                                              grand_total +
                                              "=====" +
                                              restaurant_id +
                                              "------" +
                                              employee_id +
                                              "ORDERIDUPDATED=====" +
                                              widget.orderId);

                                          if (__dialog_items_list.length > 0) {
                                            UpdateWaiterOrderRepository()
                                                .updatewaiterorder(widget.orderId, __dialog_items_list, "0", 0.0, double.parse(subtotal),
                                                    double.parse(tax_total), double.parse(grand_total), restaurant_id, "1", employee_id)
                                                .then((result) {
                                              // print("UPDATEWAITERORDER" + result.toString());
                                              if (result.responseStatus == 0) {
                                                _loading = false;
                                                Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
                                                Toast.show("No Data Found", context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                                              } else {
                                                setState(() {
                                                  // print("ADDWAITERORDERID" + result.orderId);
                                                  _loading = false;
                                                  CartsRepository.removesaved_in_cart();
                                                  getCartItemModelapi.clear();
                                                  Navigator.push(
                                                    context,
                                                    MaterialPageRoute(builder: (context) => HomePage()),
                                                  );
                                                });
                                              }
                                            });
                                          } else {
                                            _loading = false;
                                            Toast.show("Please Add items To Send Kitchen", context,
                                                duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                                          }
                                        })))
                          ],
                        )
                      : check_payment_status == "0"
                          ? Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Expanded(
                                    child: Container(
                                        // alignment: Alignment.center,
                                        decoration: BoxDecoration(color: dashboard_quick_order, borderRadius: BorderRadius.circular(0)),
                                        child: TextButton(
                                            child: Text("ADD ORDER",
                                                style: TextStyle(
                                                    fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                    fontFamily: 'Poppins',
                                                    fontWeight: FontWeight.w800,
                                                    color: Colors.white)),
                                            onPressed: () {
                                              print("selected_order_id--" + selected_order_id);
                                              UserRepository.save_OrderId(selected_order_id);
                                              UserRepository.save_TablenumbernGuests(table_number, no_of_guest);

                                              addCartItems(__cart_items_list[selected_page_position]);

                                              Navigator.push(
                                                  context, MaterialPageRoute(builder: (context) => AddFood(false, table_number, no_of_guest)));
                                            }))),
                                Expanded(
                                    child: Container(
                                        // alignment: Alignment.center,
                                        decoration: BoxDecoration(color: login_passcode_bg1, borderRadius: BorderRadius.circular(0)),
                                        child: TextButton(
                                            child: Text("BILL PAY",
                                                style: TextStyle(
                                                    fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                    fontFamily: 'Poppins',
                                                    fontWeight: FontWeight.w800,
                                                    color: Colors.white)),
                                            onPressed: () {
                                              print("SELECTEDPAGEPOSTION" + selected_page_position.toString());
                                              var header_bool = false;
                                              print("PAYMENTSCREEN" +
                                                  selected_order_id +
                                                  "---" +
                                                  grand_total +
                                                  "---" +
                                                  tablenumber_Controller.text.toString() +
                                                  "-----" +
                                                  no_of_guest_Controller.text.toString());
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) => Payment2(
                                                          tablenumber_Controller.text.toString(),
                                                          no_of_guest_Controller.text.toString(),
                                                          selected_order_id,
                                                          grand_total.toString(),
                                                          selected_unique_id,
                                                          customerId)));
                                            })))
                              ],
                            )
                          : Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Expanded(
                                    child: Container(
                                        // alignment: Alignment.center,
                                        width: double.infinity,
                                        decoration: BoxDecoration(color: dashboard_quick_order, borderRadius: BorderRadius.circular(0)),
                                        child: TextButton(
                                            child: Text("Receipt",
                                                style: TextStyle(
                                                    fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                    fontFamily: 'Poppins',
                                                    fontWeight: FontWeight.w800,
                                                    color: Colors.white)),
                                            onPressed: () {
                                              //TODO add Reciept Number
                                              Navigator.push(context, MaterialPageRoute(builder: (context) => Receipt(receipt_no)));
                                            }))),
                              ],
                            )),
          body: _loading
              ? Center(
                  child: SpinKitFadingCircle(color: Colors.lightBlueAccent),
                )
              : Container(
                  //padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                  color: Colors.white,
                  child: Directionality(
                    textDirection: TextDirection.ltr,
                    child: Column(
                      children: <Widget>[
                        Container(
                          color: dashboard_bg,
                          child: SingleChildScrollView(
                              child: Container(
                            height: SizeConfig.screenHeight - 100,
                            child: PageIndicatorContainer(
                              key: key,
                              child: PageView.builder(
                                onPageChanged: (value) {
                                  print("ONPAGECHANGE" + value.toString());
                                  selected_page_position = value;
                                  setState(() {
                                    check_payment_status = __cart_items_list[value].paymentStatus.toString();
                                    receipt_no = __cart_items_list[value].receiptNumber.toString();
                                    print("slide_PAYMENTSTATUS-" +
                                        __cart_items_list[value].paymentStatus.toString() +
                                        "======" +
                                        __cart_items_list[value].orderNumber.toString());
                                    // print("23.57-" + "6136f587839497d6d6192453");
                                    // print("95.48-" + "6136f572839497d6d619244d");
                                  });
                                },
                                itemCount: split_length,
                                physics: BouncingScrollPhysics(),
                                itemBuilder: (BuildContext context, int pindex) {
                                  return SingleChildScrollView(
                                    child: Container(
                                        color: Colors.white,
                                        child: Expanded(
                                          child: Column(
                                            children: [
                                              Container(
                                                color: dashboard_bg,
                                                margin: EdgeInsets.only(left: 0, right: 0),
                                                padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 12.0),
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    Row(
                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                      children: <Widget>[
                                                        new Image.asset(
                                                          "images/round_table.png",
                                                          height: 46,
                                                          width: 46,
                                                        ),
                                                        Padding(
                                                          padding: EdgeInsets.only(left: 8, bottom: 0),
                                                          child: Column(
                                                            children: [
                                                              Text("Table no",
                                                                  style: TextStyle(
                                                                      color: login_passcode_bg1,
                                                                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                                      fontFamily: 'Poppins',
                                                                      fontWeight: FontWeight.w600)),
                                                              Container(
                                                                width: 40,
                                                                height: 20,
                                                                // do it in both Container
                                                                child: kitchen_status == "0"
                                                                    ? TextField(
                                                                        controller: tablenumber_Controller,
                                                                        keyboardType: TextInputType.number,
                                                                        decoration: InputDecoration(
                                                                            filled: false,
                                                                            fillColor: Colors.white,
                                                                            hintText: table_number,
                                                                            hintStyle: TextStyle(
                                                                              color: text_split,
                                                                              fontSize: SizeConfig.blockSizeHorizontal * 3,
                                                                              fontFamily: 'Poppins',
                                                                              fontWeight: FontWeight.w400,
                                                                            ),
                                                                            contentPadding: EdgeInsets.only(
                                                                              bottom: 0 / 2,
                                                                              left: 25 / 2,
                                                                              // HERE THE IMPORTANT PART
                                                                            ),
                                                                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0))))
                                                                    : TextField(
                                                                        controller: tablenumber_Controller,
                                                                        enabled: false,
                                                                        keyboardType: TextInputType.number,
                                                                        decoration: InputDecoration(
                                                                            filled: false,
                                                                            fillColor: Colors.white,
                                                                            hintText: table_number,
                                                                            hintStyle: TextStyle(
                                                                              color: text_split,
                                                                              fontSize: SizeConfig.blockSizeHorizontal * 3,
                                                                              fontFamily: 'Poppins',
                                                                              fontWeight: FontWeight.w400,
                                                                            ),
                                                                            contentPadding: EdgeInsets.only(
                                                                              bottom: 0 / 2,
                                                                              left: 25 / 2,
                                                                              // HERE THE IMPORTANT PART
                                                                            ),
                                                                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0)))),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    Row(
                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                      children: <Widget>[
                                                        new Image.asset(
                                                          "images/round_customers_icon.png",
                                                          height: 46,
                                                          width: 46,
                                                        ),
                                                        Padding(
                                                          padding: EdgeInsets.only(left: 8, bottom: 0),
                                                          child: Column(
                                                            children: [
                                                              Text("Customers",
                                                                  style: TextStyle(
                                                                      color: login_passcode_bg1,
                                                                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                                      fontFamily: 'Poppins',
                                                                      fontWeight: FontWeight.w600)),
                                                              Container(
                                                                width: 40,
                                                                height: 20,
                                                                // do it in both Container
                                                                child: kitchen_status == "0"
                                                                    ? TextField(
                                                                        controller: no_of_guest_Controller,
                                                                        keyboardType: TextInputType.number,
                                                                        decoration: InputDecoration(
                                                                            filled: false,
                                                                            fillColor: Colors.white,
                                                                            hintText: no_of_guest,
                                                                            hintStyle: TextStyle(
                                                                              color: text_split,
                                                                              fontSize: SizeConfig.blockSizeHorizontal * 3,
                                                                              fontFamily: 'Poppins',
                                                                              fontWeight: FontWeight.w400,
                                                                            ),
                                                                            contentPadding: EdgeInsets.only(
                                                                              bottom: 0 / 2,
                                                                              left: 25 / 2, // HERE THE IMPORTANT PART
                                                                              // HERE THE IMPORTANT PART
                                                                            ),
                                                                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0))))
                                                                    : TextField(
                                                                        controller: no_of_guest_Controller,
                                                                        keyboardType: TextInputType.number,
                                                                        enabled: false,
                                                                        decoration: InputDecoration(
                                                                            filled: false,
                                                                            fillColor: Colors.white,
                                                                            hintText: no_of_guest,
                                                                            hintStyle: TextStyle(
                                                                              color: text_split,
                                                                              fontSize: SizeConfig.blockSizeHorizontal * 3,
                                                                              fontFamily: 'Poppins',
                                                                              fontWeight: FontWeight.w400,
                                                                            ),
                                                                            contentPadding: EdgeInsets.only(
                                                                              bottom: 0 / 2,
                                                                              left: 25 / 2, // HERE THE IMPORTANT PART
                                                                              // HERE THE IMPORTANT PART
                                                                            ),
                                                                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0)))),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    )
                                                  ],
                                                ),
                                              ),
                                              Container(
                                                  color: dashboard_bg,
                                                  //margin: EdgeInsets.all(15),
                                                  height: cardview_height,
                                                  child: Card(
                                                    margin: EdgeInsets.only(left: 10, right: 10),
                                                    elevation: 4,
                                                    child: Container(
                                                      color: Colors.white,
                                                      child: Column(children: [
                                                        Row(
                                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                          children: <Widget>[
                                                            Padding(
                                                              padding: EdgeInsets.only(top: 10.0, left: 20, bottom: 0, right: 35),
                                                              child: Text("Orders",
                                                                  style: TextStyle(
                                                                      color: cart_text,
                                                                      fontSize: SizeConfig.safeBlockHorizontal * 5,
                                                                      fontFamily: 'Poppins',
                                                                      fontWeight: FontWeight.w600)),
                                                            ),
                                                            check_payment_status == "0"
                                                                ? Container(
                                                                    // height: 30,
                                                                    // alignment: Alignment.centerRight,
                                                                    decoration: BoxDecoration(
                                                                        gradient: LinearGradient(
                                                                            begin: Alignment.topCenter,
                                                                            end: Alignment.bottomCenter,
                                                                            colors: [cancelgradient1, cancelgradient2]),
                                                                        borderRadius: BorderRadius.circular(0)),
                                                                    child: TextButton(
                                                                        child: Text("VOID",
                                                                            style: TextStyle(
                                                                                fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                                                fontFamily: 'Poppins',
                                                                                fontWeight: FontWeight.w800,
                                                                                color: Colors.white)),
                                                                        onPressed: () {
                                                                          print("OPENDIALOG" + employee_id);
                                                                          gewinner(pindex);
                                                                        }))
                                                                : SizedBox(),
                                                            item_selected_status == 1
                                                                ? Container(
                                                                    margin: const EdgeInsets.fromLTRB(15, 15, 24, 15),
                                                                    height: 35,
                                                                    padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                                                    alignment: Alignment.center,
                                                                    decoration:
                                                                        BoxDecoration(color: dashboard_bg, borderRadius: BorderRadius.circular(0)),
                                                                    child: InkWell(
                                                                        child: Text("Split",
                                                                            style: TextStyle(
                                                                                fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                                                fontFamily: 'Poppins',
                                                                                fontWeight: FontWeight.w400,
                                                                                color: text_split)),
                                                                        onTap: () {
                                                                          print("SPLITORDERID" + order_id);
                                                                          Navigator.push(context, _createRoute(order_id));
                                                                        }))
                                                                : SizedBox()
                                                          ],
                                                        ),
                                                        Row(
                                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                          children: <Widget>[
                                                            Column(
                                                              children: [
                                                                Padding(
                                                                  padding: EdgeInsets.only(top: 10.0, left: 20, bottom: 0, right: 35),
                                                                  child: Text("Order Id",
                                                                      style: TextStyle(
                                                                          color: cart_text,
                                                                          fontSize: SizeConfig.safeBlockHorizontal * 3,
                                                                          fontFamily: 'Poppins',
                                                                          fontWeight: FontWeight.w600)),
                                                                ),
                                                                Padding(
                                                                  padding: EdgeInsets.only(top: 0.0, left: 20, bottom: 0, right: 35),
                                                                  child: Text("#" + __cart_items_list[pindex].orderUniqueId.toString(),
                                                                      style: TextStyle(
                                                                          color: login_passcode_text,
                                                                          fontSize: SizeConfig.safeBlockHorizontal * 3.5,
                                                                          fontFamily: 'Poppins',
                                                                          fontWeight: FontWeight.w600)),
                                                                ),
                                                              ],
                                                            ),
                                                            Column(
                                                              children: [
                                                                Padding(
                                                                  padding: EdgeInsets.only(top: 10.0, left: 20, bottom: 0, right: 20),
                                                                  child: Text("CheckNumber",
                                                                      style: TextStyle(
                                                                          color: cart_text,
                                                                          fontSize: SizeConfig.safeBlockHorizontal * 3,
                                                                          fontFamily: 'Poppins',
                                                                          fontWeight: FontWeight.w600)),
                                                                ),
                                                                Padding(
                                                                  padding: EdgeInsets.only(top: 0.0, left: 20, bottom: 0, right: 20),
                                                                  child: Text(__cart_items_list[pindex].checkNumber.toString(),
                                                                      style: TextStyle(
                                                                          color: login_passcode_text,
                                                                          fontSize: SizeConfig.safeBlockHorizontal * 3.5,
                                                                          fontFamily: 'Poppins',
                                                                          fontWeight: FontWeight.w600)),
                                                                ),
                                                              ],
                                                            )
                                                          ],
                                                        ),
                                                        Container(
                                                          margin: EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 0.0),
                                                          child: Divider(
                                                            color: cart_viewline,
                                                          ),
                                                        ),
                                                        Expanded(
                                                            child: ListView.builder(
                                                          shrinkWrap: true,
                                                          physics: ClampingScrollPhysics(),
                                                          scrollDirection: Axis.vertical,
                                                          itemCount: __cart_items_list[pindex].itemsList.length,
                                                          itemBuilder: (context, index) {
                                                            selected_order_id = __cart_items_list[selected_page_position].orderId;
                                                            selected_unique_id = __cart_items_list[selected_page_position].orderUniqueId;
                                                            grand_total = __cart_items_list[selected_page_position].totalAmount;
                                                            tax_total = __cart_items_list[selected_page_position].taxAmount;
                                                            if (__cart_items_list[selected_page_position].haveCustomer) {
                                                              customerId = __cart_items_list[selected_page_position].customerDetails.customerId;
                                                            } else {
                                                              customerId = "";
                                                            }
                                                            print("CHECKGRANDTOTAL" +
                                                                grand_total.toString() +
                                                                "----" +
                                                                check_payment_status.toString() +
                                                                "-----" +
                                                                selected_order_id +
                                                                "=====" +
                                                                __cart_items_list[pindex].itemsList[index].itemStatus.toString());
                                                            return Card(
                                                                margin: EdgeInsets.all(10),
                                                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                                                                // The color depends on this is selected or not
                                                                /* color: data[index][
                                                                    'isSelected'] ==
                                                                true
                                                            ? login_passcode_text
                                                            : Colors.white,*/
                                                                child: ListTile(
                                                                  onTap: () {
                                                                    // if this item isn't selected yet, "isSelected": false -> true
                                                                    // If this item already is selected: "isSelected": true -> false
                                                                    setState(() {
                                                                      // gewinner();
                                                                    });
                                                                  },
                                                                  title: Container(
                                                                      margin: EdgeInsets.all(5),
                                                                      width: MediaQuery.of(context).size.width * 0.3,
                                                                      child: Column(
                                                                        children: [
                                                                          Row(
                                                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                            children: <Widget>[
                                                                              __cart_items_list[pindex].itemsList[index].itemStatus == 2
                                                                                  ? Expanded(
                                                                                      flex: 0,
                                                                                      child: Padding(
                                                                                          padding: EdgeInsets.only(left: 5, right: 5),
                                                                                          child: Image.asset(
                                                                                            'images/check_fulfill.png',
                                                                                            height: 20,
                                                                                            width: 20,
                                                                                          )))
                                                                                  : SizedBox(),
                                                                              Expanded(
                                                                                flex: 3,
                                                                                child: Padding(
                                                                                    padding: EdgeInsets.only(left: 0, right: 15),
                                                                                    child: Text(
                                                                                      __cart_items_list[pindex].itemsList[index].itemName.toString(),
                                                                                      style: TextStyle(
                                                                                          color: login_passcode_text,
                                                                                          fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                                                          fontFamily: 'Poppins',
                                                                                          fontWeight: FontWeight.w600),
                                                                                      textAlign: TextAlign.start,
                                                                                    )),
                                                                              ),
                                                                              Padding(
                                                                                  padding: EdgeInsets.only(left: 15, right: 0),
                                                                                  child: Text(
                                                                                    "x " +
                                                                                        __cart_items_list[pindex]
                                                                                            .itemsList[index]
                                                                                            .quantity
                                                                                            .toString(),
                                                                                    style: TextStyle(
                                                                                        color: coupontext,
                                                                                        fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                                                        fontFamily: 'Poppins',
                                                                                        fontWeight: FontWeight.w400),
                                                                                    textAlign: TextAlign.start,
                                                                                  )),
                                                                              Padding(
                                                                                  padding: EdgeInsets.only(left: 15, right: 15),
                                                                                  child: Text(
                                                                                    new String.fromCharCodes(new Runes('\u0024')) +
                                                                                        (__cart_items_list[pindex].itemsList[index].quantity *
                                                                                                double.parse(__cart_items_list[pindex]
                                                                                                    .itemsList[index]
                                                                                                    .unitPrice))
                                                                                            .toStringAsFixed(2),
                                                                                    style: TextStyle(
                                                                                        color: login_passcode_text,
                                                                                        fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                                                        fontFamily: 'Poppins',
                                                                                        fontWeight: FontWeight.w700),
                                                                                    textAlign: TextAlign.start,
                                                                                  )),
                                                                            ],
                                                                          ),
                                                                          __cart_items_list[pindex].itemsList[index].modifiersList.length > 0
                                                                              ? ListView.builder(
                                                                                  shrinkWrap: true,
                                                                                  physics: ClampingScrollPhysics(),
                                                                                  scrollDirection: Axis.vertical,
                                                                                  itemCount:
                                                                                      __cart_items_list[pindex].itemsList[index].modifiersList.length,
                                                                                  itemBuilder: (context, i) {
                                                                                    return Container(
                                                                                      margin: EdgeInsets.all(2),
                                                                                      width: MediaQuery.of(context).size.width * 0.3,
                                                                                      child: Row(
                                                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                        children: <Widget>[
                                                                                          Expanded(
                                                                                            child: Padding(
                                                                                                padding: EdgeInsets.only(left: 30, right: 20),
                                                                                                child: Text(
                                                                                                  __cart_items_list[pindex]
                                                                                                      .itemsList[index]
                                                                                                      .modifiersList[i]
                                                                                                      .modifierName
                                                                                                      .toString(),
                                                                                                  style: TextStyle(
                                                                                                      color: coupontext,
                                                                                                      fontSize: SizeConfig.safeBlockHorizontal * 3,
                                                                                                      fontFamily: 'Poppins',
                                                                                                      fontWeight: FontWeight.w400),
                                                                                                  textAlign: TextAlign.start,
                                                                                                )),
                                                                                            flex: 5,
                                                                                          ),
                                                                                          Expanded(
                                                                                            child: Padding(
                                                                                              padding: EdgeInsets.only(left: 10, right: 20),
                                                                                              child: Text(
                                                                                                new String.fromCharCodes(new Runes('\u0024')) +
                                                                                                    __cart_items_list[pindex]
                                                                                                        .itemsList[index]
                                                                                                        .modifiersList[i]
                                                                                                        .modifierTotalPrice
                                                                                                        .toStringAsFixed(2),
                                                                                                style: TextStyle(
                                                                                                    color: coupontext,
                                                                                                    fontSize: SizeConfig.safeBlockHorizontal * 3,
                                                                                                    fontFamily: 'Poppins',
                                                                                                    fontWeight: FontWeight.w400),
                                                                                                textAlign: TextAlign.right,
                                                                                              ),
                                                                                            ),
                                                                                            flex: 0,
                                                                                          ),
                                                                                        ],
                                                                                      ),
                                                                                    );
                                                                                  })
                                                                              : SizedBox(),
                                                                          __cart_items_list[pindex].itemsList[index].specialRequestList.length > 0
                                                                              ? ListView.builder(
                                                                                  shrinkWrap: true,
                                                                                  physics: ClampingScrollPhysics(),
                                                                                  scrollDirection: Axis.vertical,
                                                                                  itemCount: __cart_items_list[pindex]
                                                                                      .itemsList[index]
                                                                                      .specialRequestList
                                                                                      .length,
                                                                                  itemBuilder: (context, i) {
                                                                                    return Container(
                                                                                      margin: EdgeInsets.all(2),
                                                                                      child: Row(
                                                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                        children: <Widget>[
                                                                                          Expanded(
                                                                                            child: Padding(
                                                                                                padding: EdgeInsets.only(left: 30, right: 20),
                                                                                                child: Text(
                                                                                                  __cart_items_list[pindex]
                                                                                                      .itemsList[index]
                                                                                                      .specialRequestList[i]
                                                                                                      .name
                                                                                                      .toString(),
                                                                                                  maxLines: 2,
                                                                                                  style: TextStyle(
                                                                                                      color: coupontext,
                                                                                                      fontSize: SizeConfig.safeBlockHorizontal * 3,
                                                                                                      fontFamily: 'Poppins',
                                                                                                      fontWeight: FontWeight.w400),
                                                                                                  textAlign: TextAlign.start,
                                                                                                )),
                                                                                            flex: 5,
                                                                                          ),
                                                                                          Expanded(
                                                                                            child: Padding(
                                                                                              padding: EdgeInsets.only(left: 10, right: 20),
                                                                                              child: Text(
                                                                                                new String.fromCharCodes(new Runes('\u0024')) +
                                                                                                    __cart_items_list[pindex]
                                                                                                        .itemsList[index]
                                                                                                        .specialRequestList[i]
                                                                                                        .requestPrice
                                                                                                        .toStringAsFixed(2),
                                                                                                style: TextStyle(
                                                                                                    color: coupontext,
                                                                                                    fontSize: SizeConfig.safeBlockHorizontal * 3,
                                                                                                    fontFamily: 'Poppins',
                                                                                                    fontWeight: FontWeight.w400),
                                                                                                textAlign: TextAlign.right,
                                                                                              ),
                                                                                            ),
                                                                                            flex: 0,
                                                                                          ),
                                                                                        ],
                                                                                      ),
                                                                                    );
                                                                                  })
                                                                              : SizedBox(),
                                                                          /* Container(
                                                              margin: EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 0.0),
                                                              child: Divider(
                                                                color: cart_viewline,
                                                              ),
                                                            ),*/
                                                                        ],
                                                                      )),
                                                                ));
                                                          },
                                                        )),
                                                      ]),
                                                    ),
                                                  )),
                                              Container(
                                                color: dashboard_bg,
                                                margin: EdgeInsets.fromLTRB(0.0, 0, 0.0, 0.0),
                                                alignment: Alignment.centerRight,
                                                child: Column(
                                                  children: [
                                                    Container(
                                                      margin: EdgeInsets.fromLTRB(0.0, 15, 0.0, 0.0),
                                                      child: Padding(
                                                        padding: EdgeInsets.only(left: 15, right: 15),
                                                        child: Row(
                                                          mainAxisAlignment: MainAxisAlignment.end,
                                                          children: [
                                                            RichText(
                                                                text: TextSpan(
                                                              children: [
                                                                TextSpan(
                                                                    text: "Bill Amount : ",
                                                                    style: new TextStyle(
                                                                        fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                                        color: text_split,
                                                                        fontFamily: 'Poppins',
                                                                        fontWeight: FontWeight.w500))
                                                              ],
                                                            )),
                                                            RichText(
                                                                text: TextSpan(children: [
                                                              TextSpan(
                                                                  text: new String.fromCharCodes(new Runes('\u0024')) +
                                                                      __cart_items_list[pindex].subTotal,
                                                                  style: new TextStyle(
                                                                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                                      color: text_split,
                                                                      fontFamily: 'Poppins',
                                                                      fontWeight: FontWeight.w500))
                                                            ]))
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      color: dashboard_bg,
                                                      margin: EdgeInsets.fromLTRB(5.0, 0.0, 15.0, 0.0),
                                                      alignment: Alignment.topRight,
                                                      child: Padding(
                                                        padding: EdgeInsets.only(left: 15, bottom: 0),
                                                        child: Row(
                                                          mainAxisAlignment: MainAxisAlignment.end,
                                                          children: [
                                                            RichText(
                                                                text: TextSpan(
                                                              children: [
                                                                TextSpan(
                                                                    text: "Tax Total : ",
                                                                    style: new TextStyle(
                                                                        fontSize: 16,
                                                                        color: text_split,
                                                                        fontFamily: 'Poppins',
                                                                        fontWeight: FontWeight.w500))
                                                              ],
                                                            )),
                                                            RichText(
                                                                text: TextSpan(children: [
                                                              TextSpan(
                                                                  text: new String.fromCharCodes(new Runes('\u0024')) +
                                                                      __cart_items_list[pindex].taxAmount,
                                                                  style: new TextStyle(
                                                                      fontSize: 16,
                                                                      color: text_split,
                                                                      fontFamily: 'Poppins',
                                                                      fontWeight: FontWeight.w500))
                                                            ]))
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      color: dashboard_bg,
                                                      margin: EdgeInsets.fromLTRB(5.0, 0.0, 15.0, 0.0),
                                                      alignment: Alignment.topRight,
                                                      child: Padding(
                                                        padding: EdgeInsets.only(left: 15, bottom: 0),
                                                        child: Row(
                                                          mainAxisAlignment: MainAxisAlignment.end,
                                                          children: [
                                                            RichText(
                                                                text: TextSpan(
                                                              children: [
                                                                TextSpan(
                                                                    text: "Tip Amount : ",
                                                                    style: new TextStyle(
                                                                        fontSize: 16,
                                                                        color: text_split,
                                                                        fontFamily: 'Poppins',
                                                                        fontWeight: FontWeight.w500))
                                                              ],
                                                            )),
                                                            RichText(
                                                                text: TextSpan(children: [
                                                              TextSpan(
                                                                  text: new String.fromCharCodes(new Runes('\u0024')) +
                                                                      __cart_items_list[pindex].tipAmount,
                                                                  style: new TextStyle(
                                                                      fontSize: 16,
                                                                      color: text_split,
                                                                      fontFamily: 'Poppins',
                                                                      fontWeight: FontWeight.w500))
                                                            ]))
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      color: dashboard_bg,
                                                      margin: EdgeInsets.fromLTRB(0.0, 0, 0.0, 0.0),
                                                      alignment: Alignment.centerRight,
                                                      child: Padding(
                                                        padding: EdgeInsets.only(left: 15, top: 5, right: 15),
                                                        child: Row(
                                                          mainAxisAlignment: MainAxisAlignment.end,
                                                          children: [
                                                            RichText(
                                                                text: TextSpan(
                                                              children: [
                                                                TextSpan(
                                                                    text: "Discount : ",
                                                                    style: new TextStyle(
                                                                        fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                                        color: text_split,
                                                                        fontFamily: 'Poppins',
                                                                        fontWeight: FontWeight.w500))
                                                              ],
                                                            )),
                                                            RichText(
                                                                text: TextSpan(children: [
                                                              TextSpan(
                                                                  text: new String.fromCharCodes(new Runes('\u0024')) +
                                                                      __cart_items_list[pindex].discountAmount.toStringAsFixed(2),
                                                                  style: new TextStyle(
                                                                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                                      color: text_split,
                                                                      fontFamily: 'Poppins',
                                                                      fontWeight: FontWeight.w500))
                                                            ]))
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      color: dashboard_bg,
                                                      margin: EdgeInsets.fromLTRB(0.0, 0, 0.0, 0.0),
                                                      alignment: Alignment.centerRight,
                                                      child: Padding(
                                                        padding: EdgeInsets.only(left: 15, top: 5, bottom: 30, right: 15),
                                                        child: Row(
                                                          mainAxisAlignment: MainAxisAlignment.end,
                                                          children: [
                                                            RichText(
                                                                text: TextSpan(
                                                              children: [
                                                                TextSpan(
                                                                    text: "Grand Total : ",
                                                                    style: new TextStyle(
                                                                        fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                                        color: login_passcode_text,
                                                                        fontFamily: 'Poppins',
                                                                        fontWeight: FontWeight.w500))
                                                              ],
                                                            )),
                                                            RichText(
                                                                text: TextSpan(children: [
                                                              TextSpan(
                                                                  text: new String.fromCharCodes(new Runes('\u0024')) +
                                                                      __cart_items_list[pindex].totalAmount,
                                                                  style: new TextStyle(
                                                                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                                      color: login_passcode_text,
                                                                      fontFamily: 'Poppins',
                                                                      fontWeight: FontWeight.w700))
                                                            ])),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              )
                                            ],
                                          ),
                                        )),
                                  ); // you forgot this
                                },
                                controller: controller,
                                reverse: false,
                              ),
                              align: IndicatorAlign.bottom,
                              indicatorColor: Colors.black12,
                              indicatorSelectorColor: Colors.lightBlueAccent,
                              length: split_length,
                              indicatorSpace: 10.0,
                            ),
                          )),
                        ),
                      ],
                    ),
                  ),
                ),
        ));
  }

  Future<bool> _onBackPressed() async {
    print("orderId--- ${widget.isFromCart}");
    if (widget.isFromCart) {
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => HomePage()), (route) => false);
    } else {
      CartsRepository.removesaved_in_cart();
      getCartItemModelapi.clear();
      UserRepository.save_OrderId("");
      print("length--- ${getCartItemModelapi.length}");
      Navigator.of(context).pop(true);
    }
    return true;
  }

  Future gewinner(int index) async {
    returnVal = await showDialog(
        context: context,
        builder: (context) {
          return _MyDialog(
            user_id: employee_id,
            order_id: widget.orderId,
            dialog_list: __cart_items_list[index].itemsList,
          );
        });
    print("RETURNVALUEINSIDE" + returnVal);
    if (returnVal == "Success") {
      Future.delayed(Duration(seconds: 2), () async {
        setState(() {
          UserRepository().getuserdetails().then((userdetails) {
            print("DIALOGuserdata" + userdetails.length.toString());
            setState(() {
              employee_id = userdetails[0];
              //user_id = userdetails[1];
              UserRepository().getRestaurantId().then((restaurantdetails) {
                setState(() {
                  restaurant_id = restaurantdetails;
                  print("TABLEINFORMATIONDIALOG" +
                      "TABLENUMBERDIALOG===" +
                      widget.orderId.toString() +
                      " DIALOGEMPLOYEEID===" +
                      employee_id +
                      " DIALOGRESTAURANTID====" +
                      restaurant_id);
                  Future.delayed(Duration(seconds: 2), () async {
                    setState(() {
                      GetTableInformationApiRepository().checktableinformation(widget.orderId, employee_id, restaurant_id).then((value) {
                        print("TABLEINFORMATION DIALOG  " + value.responseStatus.toString());
                        debugPrint(base_url + "" + value.result.toString());
                        if (value.responseStatus == 1) {
                          setState(() {
                            _loading = false;
                            //split_length = value.tableInfo.length;
                            table_number = value.orderInfo[0].tableNumber.toString();
                            order_id = value.orderInfo[0].orderId.toString();
                            no_of_guest = value.orderInfo[0].noOfGuest.toString();
                            //tablenumber_Controller.text = table_number;

                            print(table_number +
                                "======" +
                                no_of_guest +
                                "====" +
                                tablenumber_Controller.text +
                                "_____" +
                                no_of_guest_Controller.text.toString());
                            if (no_of_guest == null || no_of_guest == "null") {
                              no_of_guest = "0";
                              no_of_guest_Controller.text = no_of_guest;
                            } else {
                              no_of_guest_Controller.text = no_of_guest;
                            }
                            //_itemsubTotal = double.parse(value.tableInfo.subTotal);
                            //_itemGrandTotalPrice = value.tableInfo.totalAmount.toString() ;
                            //_itemsTotal = int.parse(value.tableInfo.totalAmount);
                            __cart_items_list.clear();
                            __dialog_items_list.clear();
                            split_length = 0;
                            for (int t = 0; t < value.orderInfo.length; t++) {
                              __cart_items_list.add(value.orderInfo[t]);
                              print("itemslength" + value.orderInfo[t].itemsList.length.toString() + "" + payment_status);
                              __dialog_items_list = __cart_items_list[t].itemsList;

                              if (value.orderInfo[t].itemsList.length > 0) {
                                split_length = split_length + 1;
                              }
                              check_payment_status = __cart_items_list[t].paymentStatus.toString();
                              receipt_no = __cart_items_list[t].receiptNumber.toString();
                            }
                          });
                        } else if (value.responseStatus == 3) {
                          setState(() {
                            _loading = false;
                            Toast.show("NoneType' object has no attribute 'id'", context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                          });
                        } else if (value.responseStatus == 0) {
                          setState(() {
                            _loading = false;
                            Toast.show("Invalid Credentials", context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                          });
                        }
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    }
  }
}

class CancelDialogs {
  static Future<void> showLoadingDialog(BuildContext context, GlobalKey key) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new WillPopScope(
              onWillPop: () async => false,
              child: SimpleDialog(key: key, backgroundColor: Colors.black54, children: <Widget>[
                Center(
                  child: Column(children: [
                    CircularProgressIndicator(),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Cancelling....",
                      style: TextStyle(color: Colors.lightBlueAccent),
                    )
                  ]),
                )
              ]));
        });
  }
}

Route _createRoute(String table_no) {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => SplitScreen(table_no),
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      var begin = Offset(0.0, 1.0);
      var end = Offset.zero;
      var curve = Curves.ease;

      var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

      return SlideTransition(
        position: animation.drive(tween),
        child: child,
      );
    },
  );
}

class _MyDialog extends StatefulWidget {
  _MyDialog({
    this.user_id,
    this.order_id,
    this.dialog_list,
  });

  final String user_id;
  final String order_id;
  List<ItemsList> dialog_list = [];

  @override
  _MyDialogState createState() => _MyDialogState();
}

class _MyDialogState extends State<_MyDialog> {
  var _isChecked = false;
  int percent = 0;
  bool _loading = true;
  PageController _controller = new PageController();
  String userid = "";
  String employee_id = "";
  String first_name = "";
  String last_name = "";
  String restaurant_id = "";
  String servicearea_id = "";
  String user_id = "";
  double screenheight = 0.0;
  int split_length = 0;
  List<ItemsList> items_dialog_list = [];
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
  bool selectingmode = false;
  var counter_value = 1;
  List<VoidReasonsList> _voidreasons = [];
  String _selectedHall;
  String _selected_voidReason;
  List<ItemsList> __void_items_list = [];

  List<String> _texts = ["InduceSmile.com", "Flutter.io", "google.com", "youtube.com", "yahoo.com", "gmail.com"];
  List<bool> _group_checkslist = [];
  List<VoidItemsList> itemlistApi = [];

  Widget _decrementButton(int index) {
    return Container(
        margin: EdgeInsets.all(3),
        child: ClipOval(
          child: Material(
            color: cart_minus_bg,
            // button color
            child: InkWell(
              splashColor: login_passcode_bg2,
              // inkwell color
              child: SizedBox(
                  width: 28,
                  height: 28,
                  child: Icon(
                    Icons.remove,
                    size: 17,
                  )),
              onTap: () {
                setState(() {
                  if (items_dialog_list[index].quantity > 1) {
                    items_dialog_list[index].quantity = (items_dialog_list[index].quantity - 1);
                  } else {
                    items_dialog_list.removeAt(index);
                    getmtableenuitemsapi.removeAt(index);
                    print("remove item");
                    //__cart_items_list.removeAt(index);

                  }
                });
              },
            ),
          ),
        ));
  }

  Widget _incrementButton(int index) {
    return Container(
        margin: EdgeInsets.all(3),
        child: ClipOval(
          child: Material(
            color: cart_minus_bg,
            // button color
            child: InkWell(
              splashColor: login_passcode_bg2,
              // inkwell color
              child: SizedBox(
                  width: 28,
                  height: 28,
                  child: Icon(
                    Icons.add,
                    size: 17,
                  )),
              onTap: () {
                setState(() {
                  items_dialog_list[index].quantity = (items_dialog_list[index].quantity + 1);
                  print("INCREMENTQUANTITY" + items_dialog_list[index].quantity.toString());
                  //_cart_itemscalculation();
                });
              },
            ),
          ),
        ));
  }

  @override
  void initState() {
    super.initState();

    print("DIALOGLENGTH=====" + widget.dialog_list.length.toString());
    UserRepository().getuserdetails().then((userdetails) {
      setState(() {
        employee_id = userdetails[0];
        //user_id = userdetails[1];
        print("SESSIONDATAFROMLOGIN" + employee_id);
        UserRepository().getRestaurantId().then((restaurantdetails) {
          setState(() {
            restaurant_id = restaurantdetails;
            print("restaurant_id" + restaurant_id);

            Future.delayed(Duration(seconds: 2), () async {
              setState(() {
                print("DIALOG_ORDERID===" + widget.order_id + "========" + employee_id + "=========" + restaurant_id);

                for (int t = 0; t < widget.dialog_list.length; t++) {
                  for (int l = 0; l < widget.dialog_list.length; l++) {
                    items_dialog_list.add(widget.dialog_list[t]);
                  }
                }
                _group_checkslist = List<bool>.filled(widget.dialog_list.length, false);

                /* GetTableInformationApiRepository()
                    .checktableinformation(
                        widget.order_id, employee_id, restaurant_id)
                    .then((value) {
                  print("TABLEINFORMATION RESPSTATUS  " +
                      value.responseStatus.toString());
                  debugPrint(base_url + "" + value.result.toString());
                  if (value.responseStatus == 1) {
                    setState(() {
                      _loading = false;

                      for (int t = 0; t < value.orderInfo.length; t++) {
                        for (int l = 0;
                            l < value.orderInfo[t].itemsList.length;
                            l++) {
                          items_dialog_list
                              .add(value.orderInfo[t].itemsList[l]);
                        }

                        print("itemslength" +
                            value.orderInfo[t].itemsList.length.toString());

                        if (value.orderInfo[t].itemsList.length > 0) {
                          split_length = split_length + 1;
                        }
                      }
                      _group_checkslist =
                          List<bool>.filled(items_dialog_list.length, false);
                    });
                  } else if (value.responseStatus == 3) {
                    setState(() {
                      _loading = false;
                      Toast.show(
                          "NoneType' object has no attribute 'id'", context,
                          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                    });
                  } else if (value.responseStatus == 0) {
                    setState(() {
                      _loading = false;
                      Toast.show("Invalid Credentials", context,
                          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                    });
                  }
                });*/
              });
            });

            GetAllVoidReasonsApiRepository().getallvoidreasons(employee_id, restaurant_id).then((voidreasonlist) {
              setState(() {
                _voidreasons = voidreasonlist;
                setState(() {
                  _loading = false;
                });
              });
            });

            GetAllVoidReasonsApiRepository().getallvoidreasons(employee_id, restaurant_id).then((voidreasonslist) {
              setState(() {
                _voidreasons = voidreasonslist;
                setState(() {
                  Future.delayed(Duration(seconds: 2), () async {
                    setState(() {
                      _loading = false;
                    });
                  });
                });
              });
            });
          });
        });
      });
    });

    print("CHECKBOXITEM" + items_dialog_list.length.toString());

    /*for (int g = 0; g < items_dialog_list.length; g++) {
      List<bool> _items_checked = [];
      _items_checked.add(false);
      _group_checkslist.add(_items_checked);
    }*/
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
        backgroundColor: Colors.transparent,
        insetPadding: EdgeInsets.all(15),
        child: (Column(
          children: [
            Expanded(
                child: SingleChildScrollView(
                    child: Column(
              children: [
                Card(
                    margin: EdgeInsets.only(top: 50, bottom: 5, left: 15, right: 15),
                    elevation: 4,
                    child: Container(
                        color: Colors.white,
                        child: Container(
                          child: Column(
                            children: [
                              Container(
                                color: login_passcode_box,
                                height: 60,
                                alignment: Alignment.centerLeft,
                                child: Padding(
                                  padding: EdgeInsets.only(top: 0, left: 15, bottom: 0),
                                  child: Text(
                                    "Void Reason",
                                    style: TextStyle(color: login_passcode_text, fontSize: 20, fontFamily: 'Poppins', fontWeight: FontWeight.w800),
                                  ),
                                ),
                              ),
                              widget.dialog_list.length > 0
                                  ? _voidreasons.length > 0
                                      ? Container(
                                          margin: EdgeInsets.fromLTRB(15, 10, 0, 10),
                                          child: SafeArea(
                                              child: ListView.builder(
                                            scrollDirection: Axis.vertical,
                                            shrinkWrap: true,
                                            itemCount: widget.dialog_list.length,
                                            itemBuilder: (BuildContext ctx, index) {
                                              return Container(
                                                  child: Column(
                                                children: [
                                                  Row(
                                                    children: [
                                                      Expanded(
                                                          flex: 1,
                                                          child: Theme(
                                                            data: ThemeData(unselectedWidgetColor: Colors.grey),
                                                            child: new CheckboxListTile(
                                                              controlAffinity: ListTileControlAffinity.leading,
                                                              title: Text(
                                                                widget.dialog_list[index].itemName.toString(),
                                                                style: TextStyle(
                                                                    color: login_passcode_text,
                                                                    fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                                    fontFamily: 'Poppins',
                                                                    fontWeight: FontWeight.w600),
                                                                textAlign: TextAlign.start,
                                                              ),
                                                              value: _group_checkslist[index],
                                                              onChanged: (val) {
                                                                setState(() {
                                                                  _group_checkslist[index] = val;
                                                                  print("CHECKED VALUE ITEMNAME" + widget.dialog_list[index].itemId);

                                                                  itemlistApi.add(VoidItemsList(
                                                                      widget.dialog_list[index].itemId, widget.dialog_list[index].quantity, "", ""));
                                                                });
                                                              },
                                                            ),
                                                          )),
                                                      Card(
                                                        color: Colors.white,
                                                        margin: EdgeInsets.fromLTRB(15, 15, 15, 15),
                                                        elevation: 4,
                                                        child: Row(
                                                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                          children: [
                                                            _decrementButton(index),
                                                            Container(
                                                                color: Colors.white,
                                                                width: 40,
                                                                child: Padding(
                                                                  padding: EdgeInsets.all(3),
                                                                  child: Text(
                                                                    '${widget.dialog_list[index].quantity}',
                                                                    style: TextStyle(
                                                                        color: login_passcode_text,
                                                                        fontSize: 16,
                                                                        fontFamily: 'Poppins',
                                                                        fontWeight: FontWeight.w600),
                                                                    textAlign: TextAlign.center,
                                                                  ),
                                                                )),
                                                            _incrementButton(index),
                                                          ],
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ));
                                            },
                                          )))
                                      : SizedBox()
                                  : SizedBox(),
                              _voidreasons.length > 0
                                  ? Padding(
                                      padding: EdgeInsets.only(left: 0, right: 15),
                                      child: Text(
                                        'Please select a reason',
                                        style: TextStyle(
                                            color: login_passcode_text,
                                            fontSize: SizeConfig.safeBlockHorizontal * 4,
                                            fontFamily: 'Poppins',
                                            fontWeight: FontWeight.w600),
                                        textAlign: TextAlign.start,
                                      ),
                                    )
                                  : SizedBox(),
                              _voidreasons.length > 0
                                  ? Container(
                                      margin: EdgeInsets.fromLTRB(15, 15, 15, 15),
                                      alignment: Alignment.center,
                                      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 2),
                                      child: DropdownButton(
                                        isExpanded: true,
                                        underline: SizedBox(),
                                        elevation: 8,
                                        hint: Text('Please select Reason'),
                                        // Not necessary for Option 1
                                        value: _selectedHall,
                                        onChanged: (newValue) {
                                          setState(() {
                                            _selectedHall = newValue;
                                            print("SELECTEDHALL====" + _selectedHall);
                                            if (_selectedHall != null) {}
                                          });
                                        },
                                        items: _voidreasons.map((location) {
                                          return DropdownMenuItem(
                                            child: new Text(location.name.toString()),
                                            value: location.id.toString(),
                                          );
                                        }).toList(),
                                      ),
                                      decoration: ShapeDecoration(
                                        shape: RoundedRectangleBorder(
                                          side: BorderSide(width: 1, style: BorderStyle.solid, color: login_passcode_text),
                                          borderRadius: BorderRadius.all(Radius.circular(0.0)),
                                        ),
                                      ),
                                    )
                                  : Container(
                                      margin: EdgeInsets.fromLTRB(0, 20, 0, 20),
                                      alignment: Alignment.center,
                                      padding: EdgeInsets.only(left: 15, top: 10),
                                      child: Text("No Reasons Found",
                                          style: TextStyle(
                                              color: login_passcode_text, fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w600)),
                                    ),
                            ],
                          ),
                        ))),
                SizedBox(height: 15),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                        // alignment: Alignment.center,
                        decoration: BoxDecoration(
                            gradient:
                                LinearGradient(begin: Alignment.topCenter, end: Alignment.bottomCenter, colors: [cancelgradient1, cancelgradient2]),
                            borderRadius: BorderRadius.circular(0)),
                        child: TextButton(
                            child: Text("CANCEL",
                                style: TextStyle(
                                    fontSize: SizeConfig.safeBlockHorizontal * 4,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.w800,
                                    color: Colors.white)),
                            onPressed: () {
                              Navigator.pop(context, "Cancelled");
                            })),
                    Container(
                        // alignment: Alignment.center,
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                                begin: Alignment.topCenter, end: Alignment.bottomCenter, colors: [login_passcode_bg1, login_passcode_bg2]),
                            borderRadius: BorderRadius.circular(0)),
                        child: TextButton(
                            child: Text("OK",
                                style: TextStyle(
                                    fontSize: SizeConfig.safeBlockHorizontal * 4,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.w800,
                                    color: Colors.white)),
                            onPressed: () {
                              if (_selectedHall != null) {
                                for (int g = 0; g < itemlistApi.length; g++) {
                                  print(itemlistApi[g].id +
                                      "--" +
                                      itemlistApi[g].voidId +
                                      "--" +
                                      itemlistApi[g].voidReason +
                                      "--" +
                                      itemlistApi[g].voidQuantity.toString());
                                  itemlistApi[g].voidId = _selectedHall;
                                  itemlistApi[g].voidReason = "Not Liked";
                                }
                                for (int g = 0; g < itemlistApi.length; g++) {
                                  print(itemlistApi[g].id);
                                  print("AFTERUPDATED====" +
                                      itemlistApi[g].id +
                                      "--" +
                                      itemlistApi[g].voidId +
                                      "--" +
                                      itemlistApi[g].voidReason +
                                      "--" +
                                      itemlistApi[g].voidQuantity.toString());
                                  /*  itemlistApi.add(VoidItemsList(
                                          items_dialog_list[g].itemId,
                                          items_dialog_list[g].quantity,
                                          _selectedHall,
                                          ""
                                      ));*/
                                }
                                VoidItemApiRepository()
                                    .voidMultipleItems(
                                  itemlistApi,
                                  employee_id,
                                  restaurant_id,
                                )
                                    .then((result) {
                                  print(result);
                                  Navigator.pop(context, "Success");
                                  //Navigator.push( context, MaterialPageRoute( builder: (context) => GetTableInformation()), ).then((value) => setState(() {}));
                                  /*Navigator.pushReplacement(
                                          context,
                                          MaterialPageRoute(
                                              builder: (BuildContext context) => super.widget));*/
                                  if (result.responseStatus == 0) {
                                    setState(() {
                                      Toast.show(result.result, context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                                    });
                                  } else {}
                                });
                              } else {
                                Toast.show("Please Select Reason", context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                              }
                            })),
                  ],
                )
              ],
            )))
          ],
        )));
  }
}

void addCartItems(TableInfo __cart_items_list) {
  List<MenuCartItemapi> cart_list = [];
  List<SendTaxRatesModel> itemTaxesList = [];
  List<TaxTable> tax_table_arraylist = [];
  int discountValue = 0, taxTypeId = 0;
  double discountAmount = 0.00, tax = 0.0, total_price = 0.0;
  String taxType = "";
  __cart_items_list.itemsList.forEach((items) {
    List<ModifierListApi> modifiersList = [];
    List<SpecialRequestListApi> specialRequestList = [];
    total_price = 0.0;
    items.modifiersList.forEach((modifiers) {
      ModifierListApi modifier = ModifierListApi(
        modifiers.modifierId,
        modifiers.modifierName,
        modifiers.modifierTotalPrice,
        "",
        modifiers.modifierUnitPrice,
        modifiers.modifierQty,
      );
      modifiersList.add(modifier);
    });
    items.specialRequestList.forEach((specialRequests) {
      SpecialRequestListApi specialRequest = SpecialRequestListApi(specialRequests.name, specialRequests.requestPrice);
      specialRequestList.add(specialRequest);
    });
    if (items.discountValue != "") {
      if (int.tryParse(items.discountValue) != null) {
        discountValue = int.parse(items.discountValue);
      } else if (double.tryParse(items.discountValue) != null) {
        discountValue = double.tryParse(items.discountValue).toInt();
      } else {
        discountValue = 0;
      }
    } else {
      discountValue = 0;
    }
    if (items.discountAmount != "") {
      discountAmount = double.parse(items.discountAmount);
    } else {
      discountAmount = 0;
    }
    if (items.itemTaxesList.length > 0) {
      print("TAXRATELENGTH" + items.itemTaxesList.length.toString());
      for (int g = 0; g < items.itemTaxesList.length; g++) {
        taxType = items.itemTaxesList[g].taxType.toString();
        if (taxType == "0") {
          taxType = "disable";
          taxTypeId = 0;
          tax = 0.00;
          taxType = taxTypeId.toString();
        } else if (taxType == "1") {
          print("TAXTYPE" + taxType);
          var taxRate = total_price * (items.itemTaxesList[g].taxRate.toDouble() / 100);
          print("ROUNDING" + items.itemTaxesList[g].roundingOptions.toString());
          if (items.itemTaxesList[g].roundingOptions == 1) {
            print("NUMBERROUNDUP" + taxRate.toString());
            print("ROUNDINGhalfEVEN" + halfEven(taxRate, 2).toString());
            taxRate = halfEven(taxRate, 2);
          } else if (items.itemTaxesList[g].roundingOptions == 2) {
            print("ROUNDINGhalfup" + halfUp(taxRate, 2).toString());
            taxRate = halfUp(taxRate, 2);
          } else if (items.itemTaxesList[g].roundingOptions == 3) {
            print("ROUNDINGdown" + alwaysDown(taxRate, 2).toString());
            taxRate = alwaysDown(taxRate, 2);
          } else if (items.itemTaxesList[g].roundingOptions == 4) {
            print("ROUNDINGALWAYSUP" + getNumber(alwaysUp(taxRate), precision: 2).toString());
            taxRate = getNumber(alwaysUp(taxRate), precision: 2);
          }
          taxType = "percent";
          taxTypeId = 1;
          taxType = taxTypeId.toString();
          tax = taxRate;
          print("TAXTYPE" + taxType + "=========" + tax.toStringAsFixed(2) + "========" + items.itemTaxesList[g].taxid);
        } else if (taxType == "2") {
          taxType = "fixed";
          taxTypeId = 2;
          tax = total_price * items.itemTaxesList[g].taxRate.toDouble();
          taxType = taxTypeId.toString();
        } else if (taxType == "3") {
          taxType = "taxTable";
          taxTypeId = 3;
          if (items.itemTaxesList[g].taxTable.length > 0) {
            for (int h = 0; h < items.itemTaxesList[h].taxTable.length; h++) {
              if (total_price >= double.parse(items.itemTaxesList[g].taxTable[h].from) &&
                  total_price <= double.parse(items.itemTaxesList[g].taxTable[h].to)) {
                tax += double.parse(items.itemTaxesList[g].taxTable[h].taxApplied);
              }
            }
          } else {
            tax = 0.00;
          }
          taxType = taxTypeId.toString();
        }
        var sendTaxRates = SendTaxRatesModel(
            items.itemTaxesList[g].enableTakeOutRate,
            items.itemTaxesList[g].importId,
            items.itemTaxesList[g].orderValue,
            items.itemTaxesList[g].roundingOptions,
            items.itemTaxesList[g].status,
            items.itemTaxesList[g].taxName,
            items.itemTaxesList[g].taxRate,
            int.parse(taxType),
/* taxTypeId,*/
/*tax_table_arraylist*/
            [],
            items.itemTaxesList[g].taxid,
            items.itemTaxesList[g].uniqueNumber,
            tax);
        itemTaxesList.add(sendTaxRates);
      }
    } else {
      itemTaxesList = [];
    }
    MenuCartItemapi _menuCartItem = MenuCartItemapi(
      items.itemId,
      items.menuId,
      items.menuGroupId,
      items.itemName,
      items.unitPrice,
      items.quantity.toString(),
      items.totalPrice,
      "",
      modifiersList,
      specialRequestList,
      items.itemType,
      items.discountId,
      items.discountName,
      items.discountLevel,
      items.discountType,
      discountValue,
      discountAmount,
      items.comboId,
      items.comboUniqueId,
      items.isCombo,
      items.bogoId,
      items.bogoUniqueId,
      items.isBogo,
      items.discountApplicable,
      items.discountApplied,
      items.itemStatus,
      itemTaxesList,
    );
    cart_list.add(_menuCartItem);
  });
  print("list--- ${cart_list}");
  CartsRepository().saveCartList(cart_list);
}

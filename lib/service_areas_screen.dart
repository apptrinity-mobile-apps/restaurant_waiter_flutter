import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:group_button/group_button.dart';
import 'package:waiter/apis/register_device.dart';
import 'package:waiter/login.dart';
import 'package:waiter/model/service_areas_response.dart';
import 'package:waiter/session/userRepository.dart';
import 'package:waiter/utils/DialogClass.dart';
import 'package:waiter/utils/all_constans.dart';
import 'package:waiter/utils/sizeconfig.dart';
import 'package:async/async.dart';
import 'package:waiter/utils/snackbar.dart';

class ServiceAreasScreen extends StatefulWidget {
  const ServiceAreasScreen({Key key}) : super(key: key);

  @override
  _ServiceAreasScreenState createState() => _ServiceAreasScreenState();
}

class _ServiceAreasScreenState extends State<ServiceAreasScreen> {
  CancelableOperation cancelableOperation;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  double logoMarginHeight;
  String restaurantId = "", restaurantCode = "", serviceAreaId = "", revenueCenterId = "", deviceName = "", deviceToken = "";
  bool loading = true;
  List<ServiceAreasList> serviceAreasList;
  List<String> optionNameList = [];

  @override
  void initState() {
    UserRepository().getRestaurantId().then((value) {
      setState(() {
        restaurantId = value;
      });
    });
    UserRepository().getDeviceName().then((value) {
      setState(() {
        deviceName = value;
      });
    });
    UserRepository().getDeviceToken().then((value) {
      setState(() {
        deviceToken = value;
      });
    });
    UserRepository().getRestaurantCode().then((value) {
      setState(() {
        Future.delayed(Duration(seconds: 2), () async {
          restaurantCode = value;
          print(restaurantCode);
          await RegisterDeviceRepository().getWaiterServiceAreasApi(restaurantCode).then((value) {
            if (value.responseStatus == 1) {
              if (value.serviceAreasList.isNotEmpty) {
                setState(() {
                  serviceAreasList = value.serviceAreasList;
                  serviceAreasList.forEach((element) {
                    optionNameList.add(element.serviceName);
                  });
                  loading = false;
                });
              } else {
                showSnackBar(context, noDataAvailable);
              }
            } else {
              showSnackBar(context, value.result);
            }
          });
        });
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    print("Height" + SizeConfig.screenHeight.toString());
    if (SizeConfig.screenHeight >= 580 && SizeConfig.screenHeight < 800) {
      logoMarginHeight = 120;
    } else if (SizeConfig.screenHeight >= 800) {
      logoMarginHeight = 220;
    } else {
      logoMarginHeight = 100;
    }
    return Scaffold(
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(image: DecorationImage(image: ExactAssetImage("images/splash_bg.png"), fit: BoxFit.cover)),
        child: SingleChildScrollView(
            child: Column(
          children: [
            Container(
                margin: EdgeInsets.fromLTRB(40, logoMarginHeight, 0, 0),
                alignment: Alignment.centerLeft,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image(
                      image: AssetImage('images/logo_dashboard.png'),
                      width: 150,
                      height: 110,
                    ),
                    Padding(
                        padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                        child: Text(
                          'The Waiter App',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: SizeConfig.blockSizeHorizontal * 4,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w400,
                          ),
                        ))
                  ],
                )),
            Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                      margin: EdgeInsets.fromLTRB(0, 50, 0, 0),
                      alignment: Alignment.center,
                      child: Column(
                        children: [
                          Container(
                              margin: EdgeInsets.fromLTRB(35, 0, 35, 0),
                              alignment: Alignment.topLeft,
                              child: Text(
                                selectServiceArea,
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: SizeConfig.blockSizeHorizontal * 3.2,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w400,
                                ),
                              )),
                          Container(
                            margin: EdgeInsets.fromLTRB(35, 5, 35, 0),
                            width: double.infinity,
                            height: 200,
                            child: Card(
                              color: Colors.white,
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                              child: loading
                                  ? Center(
                                      child: CircularProgressIndicator(),
                                    )
                                  : Padding(
                                      padding: EdgeInsets.all(5),
                                      child: MediaQuery.removePadding(
                                          context: context,
                                          removeTop: true,
                                          child: ListView(
                                            children: [
                                              Padding(
                                                padding: EdgeInsets.all(5),
                                                child: GroupButton(
                                                  buttonWidth: double.infinity,
                                                  mainGroupAlignment: MainGroupAlignment.start,
                                                  crossGroupAlignment: CrossGroupAlignment.start,
                                                  elevation: 1,
                                                  groupingType: GroupingType.column,
                                                  spacing: 2,
                                                  runSpacing: 10,
                                                  alignment: Alignment.centerLeft,
                                                  selectedTextStyle: TextStyle(
                                                      fontSize: 21, fontFamily: "Swis721", fontWeight: FontWeight.w500, letterSpacing: 0.63),
                                                  unselectedTextStyle: TextStyle(
                                                      fontSize: 21,
                                                      fontFamily: "Swis721",
                                                      color: Colors.black,
                                                      fontWeight: FontWeight.w500,
                                                      letterSpacing: 0.63),
                                                  selectedColor: login_passcode_bg1,
                                                  unselectedColor: Colors.white,
                                                  textPadding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                                                  borderRadius: BorderRadius.circular(5.0),
                                                  buttons: optionNameList,
                                                  onSelected: (index, isSelected) {
                                                    print('$index button is selected -- ${serviceAreasList[index].serviceName}');
                                                    setState(() {
                                                      serviceAreaId = serviceAreasList[index].id;
                                                      revenueCenterId = serviceAreasList[index].revenueCenter;
                                                      UserRepository.save_servicearea_id(serviceAreaId, revenueCenterId);
                                                    });
                                                  },
                                                ),
                                              )
                                            ],
                                            shrinkWrap: true,
                                          )),
                                    ),
                            ),
                          )
                        ],
                      )),
                  Container(
                      margin: EdgeInsets.fromLTRB(35, 20, 35, 0),
                      width: double.infinity,
                      decoration: BoxDecoration(color: login_passcode_bg1, borderRadius: BorderRadius.circular(0)),
                      child: TextButton(
                          child: Text(submit.toUpperCase(),
                              style: TextStyle(fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w800, color: Colors.white)),
                          onPressed: () {
                            if (serviceAreaId == "") {
                              showSnackBar(context, pleaseSelectServiceArea);
                            } else {
                              SignDialogs.showLoadingDialog(context, "Please wait", _keyLoader);
                              cancelableOperation?.cancel();
                              CancelableOperation.fromFuture(Future.delayed(Duration(seconds: 1), () {
                                print("$restaurantId, $deviceName, $deviceToken, $serviceAreaId");
                                RegisterDeviceRepository().addDeviceApi(restaurantId, deviceName, deviceToken, serviceAreaId).then((value) {
                                  Navigator.of(context).pop();
                                  if (value.responseStatus == 1) {
                                    UserRepository.isRestaurantLogin("true");
                                    UserRepository.saveDeviceId(value.deviceId);
                                    Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => Login()), (route) => false);
                                  } else {
                                    showSnackBar(context, value.result);
                                  }
                                });
                              }));
                            }
                          }))
                ],
              ),
          ],
        )),
      ),
    );
  }
}
